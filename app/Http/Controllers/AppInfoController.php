<?php

namespace App\Http\Controllers;

use App\Services\AppinfoService;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;


class AppInfoController extends Controller
{
    use ApiResponser;

    /**
     * Servicio de App Info
     * @var AppinfoService $appinfoService
     */
    public $appinfoService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AppinfoService $appinfoService)
    {
       $this->appinfoService = $appinfoService;
    }

    /**
     * Return AppInfo List.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = $this->appinfoService->listAppinfos();
        return $this->SuccessResponse($response);
    }


    /**
     * Store AppInfo item.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->appinfoService->createAppinfos($request->all());
        return $this->SuccessResponse($response, Response::HTTP_CREATED);
    }


    /**
     * Show AppInfo item.
     *
     * @param  \App\Models\AppInfo $appinfo
     * @return \Illuminate\Http\Response
     */
    public function show($appinfo)
    {
        $response = $this->appinfoService->showAppinfos($appinfo);
        return $this->SuccessResponse($response);
    }



    /**
     * Update AppInfo item.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\AppInfo $appinfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $appinfo)
    {
        $response = $this->appinfoService->updateAppinfos($appinfo, $request->all());
        return $this->SuccessResponse($response);
    }



    /**
     * SoftDelte AppInfo item.
     *
     * @param  \App\Models\AppInfo $appinfo
     * @return \Illuminate\Http\Response
     */
    public function destroy($appinfo)
    {
        $response = $this->appinfoService->deleteAppinfos($appinfo);
        return $this->SuccessResponse($response);
    }

    //
}
