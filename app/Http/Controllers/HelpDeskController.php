<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use SendGrid\Mail\Mail as Mail;
use SendGrid as SendGrid;




class HelpDeskController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
    }

    /**
     * @return JsonResponse
     */
    public function send_email(Request $request)
    {
        $rules = [
        "title" => 'required',
        'description' => 'required',
        'image' => 'nullable',
        'platform' => 'required',
        'version' => 'required',
        'app_version' => 'required'
        ];

        $this->validate($request, $rules);
        $attributes = $request->all();


        $user = auth()->user()->getDataDescrypt()->getAttributes();
        $mutable = Carbon::now('America/Bogota');

        if(empty($user)){

            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'error' => 'Unauthorized.'
                    ]
                ],
                401
            );

        }else{
            $path="";
            if(!empty($attributes["image"])){
                $new_name = time() . '.' . $request->image->extension();
                $path = $request->image->storeAs('helpdesk', $new_name, 'public');
            }

            $data_replace = [
                "title" => $attributes["title"],
                "image" => $path,
                "user" => $user['name'].' '.$user["last_name"],
                "email" => $user['email'],
                "date" => $mutable->format('l jS \\of F Y h:i:s A'),
                "description" => $attributes["description"],
                "details" => 'Platform: '.$attributes["platform"].' - OS Version: '.$attributes["version"].' - APP Version: '.$attributes["app_version"].'.',
            ];

            $email = new Mail();
            $email->setFrom('mmiranda@inmov.com', "Acceligo Help Desk");
            $email->setSubject("Acceligo Help Desk -  New Issue");
            $email->addTo('issue-TDS-BU3CdLSB3psaWbHKSc0nTT0WK@i6.backlog.com', 'TDS New Report');
            //$email->addTo('cbeltran@inmov.com', 'TDS New Report');
            $email->setTemplateId("d-2de1005e0b5f407599da78ce24b9fbf7");
            $email->addDynamicTemplateDatas($data_replace);

            if(!empty($attributes["image"])){
                $file_encoded = base64_encode(file_get_contents($path));
                $email->addAttachment(
                    $file_encoded,
                    "application/text",
                    "File.jpg",
                    "attachment"
                );
            }


            $sendgrid = new SendGrid(env("MAIL_API"));
          // dd($sendgrid, env("MAIL_API"));

            $response = $sendgrid->send($email);
            /*try {
                $response = $sendgrid->send($email);
                print $response->statusCode() . "\n";
                print_r($response->headers());
                print $response->body() . "\n";
            } catch (Exception $e) {
                echo 'Caught exception: '.  $e->getMessage(). "\n";
            }*/

            return response()->json(
                [
                    'data' => [
                        'status' => 200,
                        'messages' => 'Sent'
                    ]
                ],
                200
            );

        }

    }

}
