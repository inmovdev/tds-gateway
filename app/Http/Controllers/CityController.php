<?php

namespace App\Http\Controllers;

use App\Services\CityService;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;


class CityController extends Controller
{

    use ApiResponser;

    /**
     * @var $service
     */
    public $service;

    /**
     * Constructor for CityController
     *
     * @param CityService $service
     */
    public function __construct(CityService $service)
    {
        $this->service = $service;
    }

    /**
     * Return resource List.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = $this->service->listCities();
        return $this->SuccessResponse($response);
    }

    /**
     * Store resource item.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->service->createCity($request->all());
        return $this->SuccessResponse($response, Response::HTTP_CREATED);
    }

    /**
     * Show resource item.
     *
     * @param int $city
     * @return void
     */
    public function show($city)
    {
        $response = $this->service->showCity($city);
        return $this->SuccessResponse($response);
    }

    /**
     * Update resource item
     *
     * @param Request $request
     * @param int $city
     * @return void
     */
    public function update(Request $request, $city)
    {
        $response = $this->service->updateCity($city, $request->all());
        return $this->SuccessResponse($response);
    }

    /**
     * SoftDelte resource item
     *
     * @param int $city
     * @return void
     */
    public function destroy($city)
    {
        $response = $this->service->deleteCity($city);
        return $this->SuccessResponse($response);
    }
}
