<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Planning;
use App\Models\Task;
use App\Models\TimeSheetsLine;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\GlobalCollection;
use App\Models\Job;
use Carbon\Carbon;
use SendGrid\Mail\Mail as Mail;
use SendGrid as SendGrid;

#use Illuminate\Http\RedirectResponse;

class UserController extends Controller
{
    use ApiResponser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Return User List.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Return User List.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filterId = $request->input("filterId");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";
        $page = ($request->input("page")) ? (int) $request->input("page") : 1;

        if (empty($pageSize)) {
            $pageSize = 10;
        }

        $im = auth()->user();
        $user = User::where("status", "=", "A");

        /*if(!empty($filterValue)){
            $user->orWhere('department', 'like', '%'.$filterValue.'%');
            $user->orWhere('position_title', 'like', '%'.$filterValue.'%');
            $user->orWhere('email', 'like', '%'.$filterValue.'%');
        }*/

        if (!empty($filterId)) {
            $user->Where('id', '=', $filterId);
        }


        $user = $user->where("status", "=", "A")->where("id", "!=", $im->id)->get();
        foreach ($user as $k => $us) {
            $user[$k] = User::findOrFail($us->id)->getDataDescrypt();
        }


        if ($sortOrder == 'asc') {
            $user = $user->sortBy(function ($item) {
                return $item->name;
            });
        } else {
            $user = $user->sortByDesc(function ($item) {
                return $item->name;
            });
        }

        if (!empty($filterValue) && empty($filterId)) {
            $user = $user->filter(function ($item, $key) use ($filterValue) {
                if (stripos($item->name, $filterValue) !== false || stripos($item->last_name, $filterValue) !== false || stripos($item->department, $filterValue) !== false || stripos($item->position_title, $filterValue) !== false) {
                    return $item;
                }
            });
        }
        $user->all();
        $total = count($user);
        $user = $user->splice($pageSize * ($page - 1));
        $user = $user->take($pageSize);
        $response["data"] = $user;
        $response["total"] = $total;
        return $this->SuccessResponse($response);
    }


    /**
     * Store User item.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'document' => 'required|unique:users',
            'name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'gender' => 'required|in:M,F,O',
            'mobile' => 'required|max:30',
            'email' => 'required|unique:users',
            'password' => 'required|min:8|confirmed',
            'update_data' => 'required',

        ];


        $this->validate($request, $rules);
        $fields = $request->all();
        $user = new User();
        $data = $user->setDataCreate($fields);
        $user = User::create($data)->getDataDescrypt()->getAttributes();
        $data = $this->getTaskJobs();
        $user = array_merge($user, $data);
        return response()->json([
            "data" => $user,
            "code" => Response::HTTP_CREATED,
        ]);
    }


    /**
     * Show User item.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {
        $user = User::findOrFail($user)->getDataDescrypt();
        return $this->ValidResponse($user);
    }


    /**
     * @param array $args
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function update($args)
    {
        //        $rules = [
        //            'document' => 'unique:users,document,'.$user,
        //            'name' => 'max:255',
        //            'last_name' => 'max:255',
        //            'gender' => 'in:M,F,O',
        //            'mobile' => 'max:30',
        //            'email' => 'unique:users,email,'.$user, // Excepto el usuario que esta actualizando
        //            'password' => 'confirmed|min:8',
        //
        //        ];
        //        $this->validate($request, $rules);
        $user = User::findOrFail($args["id"]);
        $user->fill($args);
        if ($user->isClean()) {
            return $this->ErrorResponse('Al menos un valor debe cambiar', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $user->setDataUpdate($args);
        $user->save();
        $user = $user->getDataDescrypt()->getAttributes();
        $data = $this->getTaskJobs();
        $user = array_merge($user, $data);
        return response()->json([
            "data" => $user,
            "code" => Response::HTTP_OK,
        ]);
    }

    /**
     * @param array $args
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function update_user($user, Request $request)
    {
        $rules = [
            'password' => 'required|confirmed|min:6',
            'token' => 'required',

        ];
        $this->validate($request, $rules);
        $args = $request->all();

        $token_valid = User::where('token', '=', $args["token"])->where('id', '=', $user)->get();

        if (empty($token_valid[0]->id)) {
            return $this->ErrorResponse("The token entered is incorrect.", Response::HTTP_UNAUTHORIZED);
        }

        $args["token"] = '';
        $args["password"] = Hash::make($args["password"]);


        $user = User::findOrFail($user);
        $user->fill($args);
        if ($user->isClean()) {
            return $this->ErrorResponse('Al menos un valor debe cambiar', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        //$user->setDataUpdate($args);
        $user->save();
        $user = $user->getDataDescrypt()->getAttributes();
        $data = $this->getTaskJobs();
        if (!empty($data)) {
            $user = array_merge($user, $data);
        }
        return response()->json([
            "data" => $user,
            "code" => 200,
        ]);
    }


    /**
     * SoftDelte User item.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {
        $user = User::findOrFail($user);
        $user->delete();
        $user->getDataDescrypt();
        return $this->ValidResponse($user);
    }


    public function test(Request $request)
    {
        $header = $request->header('custom-header');
        $request = $request->all();

        if ($header) {
            return $this->ValidResponse($request);
        } else {
            return $this->ErrorResponse('No se ha detectado el header', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }


    /**
     * Return data user auth
     *
     * @return void
     */
    public function me()
    {
        //$this->getOdoo();
        $user = auth()->user()->getDataDescrypt()->getAttributes();
        $data = $this->getTaskJobs();
        $user = array_merge($user, $data);
        return response()->json([
            "data" => $user,
            "code" => 200,
        ]);
    }

    /**
     * Return data user auth
     *
     * @return void
     */
    public function get_user_info($user_id)
    {
        $user = User::findOrFail($user_id);
        $user = $user->getDataDescrypt()->getAttributes();
        return response()->json([
            "data" => $user,
            "code" => 200,
        ]);
    }


    public function getTaskJobs()
    {
        if (auth()->check()) {
            $employee = Employee::where('work_email', auth()->user()->email)->where('active', true)->first();
            $countTasks = [];
            $countProjects = [];
            $inDraft = 0;
            $inWaiting = 0;
            $inApproved = 0;
            $inReject = 0;
            if ($employee) {
                $startDate = Carbon::now('America/Bogota')->startOfWeek()->format('Y-m-d');
                $endDate = Carbon::now('America/Bogota')->addMonth()->format('Y-m-d');
                // $projects = Planning::where('employee_id', $employee->id)
                //     ->where('is_published', true)
                //     ->whereBetween('start_datetime', [$startDate, $endDate])
                //     ->pluck('project_id')
                //     ->toArray();
                $projects = Job::where('users_id', auth()->user()->id)
                    ->where('status', '!=', 'FI')
                    ->whereBetween('visit_date', [$startDate, $endDate])
                    ->whereNotNull('planning_odoo_id')
                    ->pluck('project_odoo_id')
                    ->toArray();
                // $tasks = Planning::where('employee_id', $employee->id)
                //     ->where('is_published', true)
                //     ->whereBetween('start_datetime', [$startDate, $endDate])
                //     ->pluck('task_id')
                //     ->toArray();

                $tasks = Job::where('users_id', auth()->user()->id)
                    ->where('status', '!=', 'FI')
                    ->whereBetween('visit_date', [$startDate, $endDate])
                    ->whereNotNull('planning_odoo_id')
                    ->pluck('task_odoo_id')
                    ->toArray();
                $countProjects = array_unique($projects);
                $countTasks = array_unique($tasks);
                $inDraft = TimeSheetsLine::where('employee_id', $employee->id)
                    ->where('unit_amount', '>', 0)
                    ->where('custom_state', TimeSheetsLine::DRAFT)
                    ->whereNotNull('project_id')
                    ->sum('unit_amount');
                $inWaiting = TimeSheetsLine::where('employee_id', $employee->id)
                    ->where('unit_amount', '>', 0)
                    ->where('custom_state', TimeSheetsLine::WAITING)
                    ->whereNotNull('project_id')
                    ->sum('unit_amount');
                $inApproved = TimeSheetsLine::where('employee_id', $employee->id)
                    ->where('unit_amount', '>', 0)
                    ->where('custom_state', TimeSheetsLine::APPROVED)
                    ->whereNotNull('project_id')
                    ->sum('unit_amount');
                $inReject = TimeSheetsLine::where('employee_id', $employee->id)
                    ->where('unit_amount', '>', 0)
                    ->where('custom_state', TimeSheetsLine::REJECT)
                    ->whereNotNull('project_id')
                    ->sum('unit_amount');
            }

            return [
                "jobs" => $countProjects == [null] ? 0 : count($countProjects),
                "tasks" => $countTasks == [null] ? 0 : count($countTasks),
                'inDraft' => $inDraft,
                'inWaiting' => $inWaiting,
                'inApproved' => $inApproved,
                'inReject' => $inReject,
                'NoAuth' => false,
            ];
        } else {
            return [
                "jobs" => 0,
                "tasks" => 0,
                'inDraft' => 0,
                'inWaiting' => 0,
                'inApproved' => 0,
                'inReject' => 0,
                'NoAuth' => true,
            ];
        }
    }

    //}

    public function profile_image(Request $request, $user)
    {

        $rules = [
            "file" => "required",
        ];

        $this->validate($request, $rules);
        $user = User::findOrFail($user);
        $path = $new_name = time() . '.' . $request->file->extension();
        $path = $request->file->storeAs('profile_images', $new_name, 'public');
        // Storage::disk('public')->put( $new_name, 'profile_images');
        $current_avatar = $user->avatar;
        $user->avatar = $path;
        $user->save();
        if (empty($current_avatar) || is_null($current_avatar)) {
            DB::select('CALL prd_register_score(?,?,?,?)', array((int) $user->id, (int) 1, (int) 0, '')); //1 Change profile photo
            //dd($response);
        }
        $return["user"] = $user;
        $user = $user->getDataDescrypt()->getAttributes();
        $data = $this->getTaskJobs();
        $user = array_merge($user, $data);
        return response()->json([
            "data" => $user,
            "code" => 200,
        ]);
    }


    public function verifyAuthHasura()
    {
        /* if (auth()->check() && auth()->user()->status == "A") {
            $header = [
                "X-Hasura-User-Id" => (string) auth()->user()->id,
                "X-Hasura-Role" => "user",
                "X-Hasura-Admin-Secret" => "123456",
            ];
            return $this->SuccessResponse($header, Response::HTTP_OK);
        } else {
            if (auth()->check())
                auth()->logout();
            return $this->ErrorResponse("", Response::HTTP_UNAUTHORIZED);
        } */

        return [
            "X-Hasura-User-Id" => auth()->check() ? (string) auth()->user()->id : "1",
            "X-Hasura-Role" => "user",
            "X-Hasura-Admin-Secret" => "123456",
        ];
    }


    public function recovery_password(Request $request)
    { //Cel y Email
        $rules = [
            "item" => "required|email"

        ];
        $this->validate($request, $rules);
        $item = request(['item']);
        $item['item'] = strtolower(trim($item['item']));

        $email = User::whereRaw("LOWER(email) = '".$item['item']."'")->count();

        $recovery_token = random_int(1111111, 9999999);
        if ($email > 0) {

            $user = User::where("email", "=", $item['item'])->first();
            $user->token = $recovery_token;
            $user->save();
            $user = $user->getDataDescrypt();

            if($user->status != 'A'){
                $message = "It seems that your user is inactive, please contact the App Administrator";

                return response()->json(
                    [
                        'errors' => [
                            'status' => 401,
                            'messages' => [$message]
                        ]
                    ],
                    401
                );
            }

            $data_replace = [
                "name" => $user->name . ' ' . $user->last_name,
                "code" => $recovery_token,
            ];


            $email = new Mail();
            $email->setFrom("acceligo_support@acceligosoftware.com", "Acceligo Software");
            $email->setSubject("Acceligo Software - Recovery Password");
            $email->addTo($item['item'], $user->name . ' ' . $user->last_name);
            $email->setTemplateId("d-85beb5a357934f0b9f7a8fc819b42ad1");
            $email->addDynamicTemplateDatas($data_replace);
            // $email->addContent("text/plain", "and easy to do anywhere, even with PHP");
            //$email->addContent(
            //    "text/html", "<strong>and easy to do anywhere, even with PHP</strong>"
            //);
            //sleep(20);
            $error = '';
            $response = '';
            $message = '';
            try {
            $sendgrid = new SendGrid(env("MAIL_API"));
            $response = $sendgrid->send($email);
            } catch (Exception $e) {
                $error =  $e->getMessage();
                $message = "We had a complication when trying to send the message. Reason: Email rejected by SendGrid";
            }

            if(!empty($error)){
                return response()->json(
                    [
                        'errors' => [
                            'status' => 401,
                            'messages' => [$message],
                            'error' => $error
                        ]
                    ],
                    401
                );
            }else{
            return response()->json(
                [
                    'errors' => [
                        'status' => 200,
                        'messages' => ["Email enviado con éxito"],
                        'response' => $response,
                        'error' => $error
                    ]
                ],
                200
            );
            }



        } else {

            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => ["We had a complication when trying to send the message. Reason: Email ".$item['item']." Not Found"]
                    ]
                ],
                401
            );
        }
    }


    public function recovery_password_cancel(Request $request, $code)
    {

        $cnt = User::where("token", "=", $code)->count();
        if ($cnt > 0) {
            $us = User::where("token", "=", $code)->first();

            $user = User::find($us->id);
            $user->token = NULL;
            $user->save();
            $value = 1;
        } else {
            $value = 0;
        }

        $url = 'https://prosource.tdssolutions.com/?recovery_cancel=' . $value;
        return redirect()->to($url);
    }


    public function validate_token(Request $request)
    {

        $rules = [
            "item" => "required",
            "token" => "required"
        ];


        $this->validate($request, $rules);


        $item = request(['item', 'token']);
        $phone = User::where("mobile", "=", $item['item'])->where("token", "=", $item['token'])->count();
        $email = User::where("email", "=", $item['item'])->where("token", "=", $item['token'])->count();

        $temp_pass = random_int(111111, 999999);

        if ($phone > 0 || $email > 0) {

            if ($phone > 0) {
                $user = User::where("phone", "=", $item['item'])->first();
            } else if ($email > 0) {
                $user = User::where("email", "=", $item['item'])->first();
            }

            $user->password = Hash::make($temp_pass);
            $user->save();

            return response()->json(
                [
                    'response' => [
                        'status' => 200,
                        'messages' => [$temp_pass],
                        'email' => $user->email,
                        'id' => $user->id
                    ]
                ],
                200
            );
        } else {

            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => ['The information entered is wrong']
                    ]
                ],
                401
            );
        }
    }


    /**
     * Mass User Store.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function mass(Request $request)
    {
        $rules = [
            'users' => 'required',
        ];

        $this->validate($request, $rules);
        $fields = $request->all();


        foreach ($fields["users"] as $k => $v) {
            $result_data = array();

            $rules = [
                'document' => 'nullable|unique:users',
                'name' => 'required|max:255',
                'last_name' => 'required|max:255',
                'email' => 'required|unique:users',
                'gender' => 'required|in:M,F,O',
                "employee_paycom_code" => "required",
                'state_code' => 'required',
                'city' => 'required',
                'department' => 'required',
                'position_title' => 'required',
                'marital_status' => 'required',
                'work_phone' => 'required',
                'dol_status' => 'required',
                'street' => 'required',
                'position_family' => 'required',
                'employee_status' => 'required',
                'age' => 'required',
                "mobile" => "nullable|unique:users",
                "password" => "required",
                "uuid" => "nullable",
                "status" => "required"
            ];

            $req = new \Illuminate\Http\Request();
            $req->replace($v);

            $this->validate($req, $rules);

            $user = new User();
            $data = $user->setDataCreate($v);
            $user = User::create($data)->getDataDescrypt()->getAttributes();
            $data = $this->getTaskJobs();
            $user = array_merge($user, $data);
        }

        $result_data[] = $user;
        return response()->json([
            "data" => $user,
            "code" => Response::HTTP_CREATED,
        ]);
    }

    public function resetPassword()
    {
        $user = User::where('email', "jwayne@tdssolutions.com");
        $user->update([
            "password" => bcrypt('jwayne@tdssolutions.com')
        ]);
        return $this->SuccessResponse("OK", Response::HTTP_OK);
    }

    public function uuid_update(Request $request, $id)
    {

        $rules = [
            "uuid" => "required"
        ];


        $this->validate($request, $rules);
        $user = User::findOrFail($id);
        $item = request(['uuid']);
        $user->fill($item);
        if ($user->isClean()) {
            return $this->ErrorResponse('Al menos un valor debe cambiar', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $user->setDataUuid($item);
        $user->save();
        $user = $user->getDataDescrypt()->getAttributes();
        $data = $this->getTaskJobs();
        $user = array_merge($user, $data);
        return response()->json([
            "data" => $user,
            "code" => Response::HTTP_OK,
        ]);
    }


    public function createUsers()
    {
        $user = User::where('email', "ameadows@tdssolutions.com")->first();
        $user->update([
            'password' => bcrypt('Demo1234'),
        ]);
        $user->save();
        return "OK";
    }


    public function getOdoo()
    {
        $controller = new JobController();
        $controller->index([]);
    }
}
