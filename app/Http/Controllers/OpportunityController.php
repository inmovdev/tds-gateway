<?php


namespace App\Http\Controllers;

use App\GraphQL\Queries\Job\Jobs;
use App\Models\Agreement;
use App\Models\Customer;
use App\Models\Employee;
use App\Models\Job;
use App\Models\Opportunity;
use App\Models\OpportunityTagUser;
use App\Models\Partner;
use App\Models\Planning;
use App\Models\Project;
use App\Models\ProjectsManager;
use App\Models\Stage;
use App\Models\StateOdoo;
use App\Models\Task;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class OpportunityController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
    }

    /**
     * @param $opportunity
     * @return array
     */
    private function getProject($opportunity)
    {
        if ($opportunity->id_odoo) {
            $project = Project::find($opportunity->id_odoo);
            if ($project) {
                $tasks = $this->getTasks($project->id);
                if (count($tasks) > 0) {
                    $project = array_merge($tasks);
                }
                return $project;
            }
        } else {
            return [];
        }
    }

    /**
     * @param $job
     * @return mixed
     */
    private function getProjectJob($job)
    {
        if ($job->project_odoo_id) {
            $project = Project::find($job->project_odoo_id);
            return $project;
        }
        return null;
    }

    /**
     * @param $projectId
     * @return array
     */
    public function getTasks($projectId)
    {
        $employee = $this->getEmployee();
        $array = [];
        if ($employee) {
            $tasksArray = Planning::where('employee_id', $employee->id)
                ->where('is_published', true)
                ->where('project_id', $projectId)
                ->pluck('task_id')
                ->toArray();
            $tasks = Task::whereIn('id', $tasksArray)->where('active', true)->get();
            foreach ($tasks as $task) {
                $planning = Planning::where('employee_id', $employee->id)
                    ->where('is_published', true)
                    ->where('project_id', $projectId)
                    ->where('task_id', $task->id)
                    ->first();
                $stage = Stage::find($task->stage_id);
                array_push($array, [
                    'id' => $task->id,
                    'name' => $task->name,
                    'description' => $task->description == "<p><br></p>" ? "Without information" : $task->description,
                    'stage' => [
                        'id' => $stage->id,
                        'name' => $stage->name,
                    ],
                    'planning' => [
                        'id' => $planning->id,
                        'start_datetime' => $planning->start_datetime,
                        'end_datetime' => $planning->end_datetime,
                        'working_days_count' => $planning->working_days_count,
                    ],
                ]);
            }
        }
        return $array;
    }

    public function getCustomer($opportunity)
    {
        return Customer::where('account_id', $opportunity->account_id)->first();
    }

    private function getStatus($opportunity)
    {
        if ($opportunity->status == "A" && !$opportunity->id_odoo)
            return "Assigned";
        if ($opportunity->status == "T" && !$opportunity->id_odoo)
            return "Targeted";
        if ($opportunity->status == "A" && $opportunity->id_odoo)
            return "M";
    }

    private function getEmployee()
    {
        return Employee::where('work_email', auth()->user()->email)->where('active', true)->first();
    }

    /**
     * @param $job
     * @return array
     */
    private function getContact($job)
    {
        $table = DB::table('customers');
        $contact = $table->where('id', $job->contact_id)->first();
        $partner = Partner::find($contact->id_odoo);
        return [
            'id' => $contact ? $contact->id : "Without information",
            'name' => $contact ? $contact->name : "Without information",
            'phone' => $contact ? !ctype_space($contact->phone) ? !empty($contact->phone) ?: "Without information" : "Without information" : "Without information",
            'assistant' => null,
            'assistant_phone' => null,
            'other_phone' => null,
            'home_phone' => $partner ? !ctype_space($partner->phone) ? !empty($partner->phone) ? $partner->phone : null : null : null,
            'mailing_address' => $partner ? $partner->email : null,
            'mobile' => $partner ? $partner->mobile : null,
            'created_at' => $contact ? $contact->created_at : null,
        ];
    }

    /**
     * @param $job
     * @return array
     */
    private function getJobsWorkdays($job)
    {
        $table = DB::table('jobs_workday');
        $workDay = $table->where('job_id', $job->id)->latest('id')->first();
        if ($workDay) {
            $tracker = DB::table('jobs_workday_time_tracker')
                ->where('jobs_workday_id', $workDay->id)
                ->latest('id')
                ->first();
            return [
                'id' => $workDay->id,
                'user_id' => $workDay->user_id,
                'status' => $workDay->status,
                'total_seconds_duration' => $workDay->total_seconds_duration,
                'date' => $workDay->date,
                'created_at' => $workDay->created_at,
                'updated_at' => $workDay->updated_at,
                'jobs_workday_time_trackers' => [$tracker]
            ];
        } else {
            return null;
        }
    }

    private function getProjectManager($project)
    {
        if ($project) {
            $projectManager = ProjectsManager::find($project->user_id);
            if(!isset($projectManager->partner_id)){
                return null;
            }else{
                return Partner::find($projectManager->partner_id);
            }
        }
        return null;
    }

    public function getByDistance($lat, $lng)
    {
        //Fórmula de Haversine
        $haversine = "(6371 * acos(cos(radians(" . $lat . "))
                    * cos(radians(`latitude`))
                    * cos(radians(`longitude`)
                    - radians(" . $lng . "))
                    + sin(radians(" . $lat . "))
                    * sin(radians(`latitude`))))";

        return Job::where('users_id', auth()->user()->id)
            ->whereDate('created_at', '>=', Carbon::now())
            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [10])
            ->orderBy('distance', 'ASC');
    }

    private function getAgreement($project)
    {
        if ($project->agreement_id)
            return Agreement::find($project->agreement_id);
        return null;
    }

    private function getPlanningJob($job)
    {
        $planning = null;
        if ($job->planning_odoo_id) {
            $planning = Planning::where('id', $job->planning_odoo_id)->first();
        } else {
            $planning = Planning::where('project_id', $job->project_odoo_id)
                ->where('task_id', $job->task_odoo_id)
                ->first();
        }
        return $planning;
    }

    /**
     * @param $request
     * @return array
     */
    private function getJobs($request)
    {
        $employee = $this->getEmployee();
        $now = Carbon::now('America/Bogota')->format('Y-m-d');
        $jobs = Job::where('users_id', auth()->user()->id)
            ->whereDate('visit_date', '>=', "$now 00:00:00")
            ->oldest('id')
            ->get();
        $arrayJobs = [];
        $arrayIdsJobs = [];
        $taskIds = [];
        $sss = [];

        foreach ($jobs as $job) {
            $count_p = Planning::where('id', $job->planning_odoo_id)
                    ->count();
            if (!in_array($job->task_odoo_id, $taskIds) && $count_p > 0) {
                $taskIds[] = $job->task_odoo_id;
                $planning = Planning::where('id', $job->planning_odoo_id)
                    ->where('task_id', $job->task_odoo_id)
                    ->where('employee_id', $employee->id)
                    ->first();

                if ($planning) {
                    array_push($arrayJobs, $job);
                    array_push($arrayIdsJobs, $job->id);
                }
            }
        }

        /*DB::table('debug')->insert(
            ['text' =>  json_encode($sss) ]
        );*/

        $jobs2 = Job::whereIn('id', $arrayIdsJobs);
        if ($request->longitude && $request->latitude) {
            $fields = "id, contact_id, title, purpose, date_create, parameter, users_id, address, contract, latitude, longitude, places_id, extra_info, checkin, date_checkin, visit_date, status, created_at, updated_at, deleted_at, project_odoo_id, task_odoo_id, status_opportunities";
            $fields_avi = ", '-' AS  availability ";
            $select_raw = "( 3959 * acos( cos( radians('" . $request->latitude . "') ) *
                            cos( radians( latitude ) ) *
                            cos( radians( longitude ) -
                            radians('" . $request->longitude . "') ) +
                            sin( radians('" . $request->latitude . "') ) *
                            sin( radians( latitude ) ) ) )
                            AS distance, $fields $fields_avi";
            $jobs2->select(DB::raw($select_raw))
                ->havingRaw("( 3959 * acos( cos( radians('" . $request->latitude . "') ) *
                                        cos( radians( latitude ) ) *
                                        cos( radians( longitude ) -
                                        radians('" . $request->longitude . "') ) +
                                        sin( radians('" . $request->latitude . "') ) *
                                        sin( radians( latitude ) ) ) )
                                        < '5000000000000000000000000000000000000'")
                ->orderByRaw('distance ASC')
                ->groupBy(DB::raw($fields . ', availability'));
        }
        $jobs = $jobs2->get();

        $jobsArray = [];
        foreach ($jobs as $job) {
            $project = Project::find($job->project_odoo_id);
            //$planning = Planning::find($job->planning_odoo_id);
            //$task = Task::find($job->task_odoo_id);
            if ($project) {
                $contact = $this->getContact($job);
                $workDays = $this->getJobsWorkdays($job);
                $project = $this->getProjectJob($job);
                $task = $this->getTask($job);
                $planning = $this->getPlanningJob($job);
                $projectManager = $this->getProjectManager($project);
                $agreement = $project ? $this->getAgreement($project) : null;
                $extraInfo = $this->getExtraInfo($task, $job);
                $checking = $this->getLastestChecking($job);
                $address = "Without information job";
                if ($project && isset($project->delivery_street) &&  $project->delivery_street) {
                    $state = $project->delivery_state_id ? StateOdoo::where('id', $project->delivery_state_id)->first() : null;
                    $codeZip = (int) $project->delivery_zip;
                    $address = "$project->delivery_street, $project->delivery_city, $state->name, $codeZip";
                }
                $jobO = [
                    'type' => "Job",
                    'id' => $job->id,
                    'purchase_order' => $project && $project->cm_po ? $project->cm_po : "Without information",
                    'work_order' => $project && $project->cm_wo ? $project->cm_wo : "Without information",
                    'contract' => $agreement ? "#$agreement->code" : "Without information",
                    'ubication' => $job->latitude ? true : false,
                    'latitude' => $job->latitude,
                    'longitude' => $job->longitude,
                    'address' => $address,
                    'extra_info' => $extraInfo ? $extraInfo : "Without information",
                    'parameter' => $job->parameter,
                    'places_id' => $job->places_id,
                    'purpose' => $task ? $task->name : $job->purpose,
                    'status' => $job->status,
                    'title' => $project ? $project->name : $job->title,
                    'users_id' => $job->users_id,
                    'visit_date' => $planning ? Carbon::parse($planning->start_datetime)->timezone($task ? $task->tz : 'America/New_York') : Carbon::parse($job->visit_date)->timezone($task ? $task->tz : 'America/New_York'),
                    'end_date' => $planning ? Carbon::parse($planning->end_datetime)->timezone($task ? $task->tz : 'America/New_York') : null,
                    'end_data' => $planning ? Carbon::parse($planning->end_datetime)->timezone($task ? $task->tz : 'America/New_York') : null,
                    'project_odoo_id' => $job->project_odoo_id,
                    'task_odoo_id' => $job->task_odoo_id,
                    'checkin' => (bool) $job->checkin,
                    'date_checkin' => $job->date_checkin,
                    'date_create' => $job->date_create,
                    'created_at' => $job->created_at,
                    'deleted_at' => $job->deleted_at,
                    'contact' => $contact,
                    'jobsworkdays' => $workDays ? [$workDays] : [],
                    'projectOdoo' => $project,
                    'taskOdoo' => [$task],
                    'projectManager' => $projectManager,
                ];
                array_push($jobsArray, $jobO);
            }
        }
        return $jobsArray;
    }

    public function index(Request $request)
    {
        $userId = auth()->user()->id;
        $opportunitiesArray = $this->getTargets($userId);
        $opportunities = $this->getOpportunities($opportunitiesArray);
        $arrayOp = [];
        foreach ($opportunities as $opportunity) {
            $project = $opportunity->id_odoo ? $this->getProject($opportunity) : [];
            $customer = $this->getCustomer($opportunity);
            $address = "Without information";
            if ($project && isset($project->delivery_street) &&  $project->delivery_street) {
                $state = $project->delivery_state_id ? StateOdoo::where('id', $project->delivery_state_id)->first() : null;
                $codeZip = (int) $project->delivery_zip;
                $address = "$project->delivery_street, $project->delivery_city, $state->name, $codeZip";
            }

            array_push($arrayOp, [
                'type' => "Opportunity",
                'id' => $opportunity->id,
                'name' => $opportunity->opportunity_name,
                'start_date' => $opportunity->estimated_start_date,
                'end_date' => $opportunity->estimated_end_date,
                'city' => $customer ? $customer->city : "Without information",
                'state' => $customer ? $customer->state : "Without information",
                'address' => $address,
                'created_at' => $opportunity->created_at,
                'status' => $opportunity->probability == 100 && $opportunity->stage == "Closed Won" ? "A" : "T",
                'project' => $project,
            ]);
        }
        $jobs = $this->getJobs($request);

        $arrayOp = array_merge($jobs, $arrayOp);
        return $this->SuccessResponse(["data" => $arrayOp, "code" => Response::HTTP_OK], Response::HTTP_OK);
    }

    /**
     * @param $userId
     * @return mixed
     */
    private function getTargets($userId)
    {
        return OpportunityTagUser::where('user_id', $userId)
            ->pluck('opportunity_id')
            ->toArray();
    }

    /**
     * @param $array
     * @return mixed
     */
    private function getOpportunities($array)
    {
        $arrayIds = Job::where('users_id', auth()->user()->id)
            ->pluck('opportunity_id')
            ->toArray();
        return Opportunity::whereIn('id', $array)->whereNotIn('id', $arrayIds)->get();
    }

    private function getLocation($address)
    {
        $address = urlencode($address);
        //dd($address);
        $key = "AIzaSyB8ZwgpE1YjoabdGss6ZZ1EeZPIPmSryzU";
        //$key = "AIzaSyAdrbNIzRfxKjVT3Bz1YaQkmKI6Tlu_Jc4";
        $url = "https://maps.google.com/maps/api/geocode/json?address=" . $address . "&key=$key";
        $client = new Client();
        $response = $client->request('GET', $url);
        $response = json_decode($response->getBody());
        //dd($response);
        return [
            'latitude' => $response->results[0]->geometry->location->lat,
            'longitude' => $response->results[0]->geometry->location->lng
        ];
    }

    /**
     * @param $job
     * @return mixed
     */
    private function getTask($job)
    {
        if ($job->task_odoo_id) {
            $task = Task::findOrFail($job->task_odoo_id);
            return $task;
        }
        return null;
    }

    private function getPlanning($job)
    {
        $planning = null;
        if ($job->planning_odoo_id) {
            $planning = Planning::where('id', $job->planning_odoo_id)->first();
        } else {
            $planning = Planning::where('project_id', $job->project_odoo_id)
                ->where('task_id', $job->task_odoo_id)
                ->first();
        }
        return $planning;
    }

    private function getExtraInfo($task, $job)
    {
        if ($task && $task->description != "<p><br></p>") {
            return $task->description;
        }
        if ($job && $job->extra_info) {
            return $job->extra_info;
        }
        return "Without information";
    }

    private function getLastestChecking($job)
    {
        $table = DB::table('jobs_checkings');
        $checking = $table->where('job_id', $job->id)->whereDate('created_at', Carbon::now('America/Bogota'))->first();
        if ($checking) {
            return $checking;
        }
    }
}
