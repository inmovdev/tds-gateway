<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Task;
use App\Models\Planning;
use App\Models\Project;
use App\Models\TimeSheetsLine;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Carbon\Carbon;
use DB;

use App\Models\Score;
use App\Models\UserLevel;
use App\Models\Ratings;


class UserLevelController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {

        $user = auth()->user();
        $my_points = DB::table("vs_view_users_ranking")->where("id","=",$user->id)->get();

        $my_current_level=[];
        if(!empty($my_points[0])){
        $my_current_level = UserLevel::where("level", "=", $my_points[0]->level)->get();
        }


        $interactions = DB::select(
                DB::raw("select count(g.*) as cant from scores_users s
                        inner join scores_guide g
                        on s.score_guide_id = g.id
                        where g.source = 'INTERACTIONS' and s.user_id = '".$user->id."'")
                );


        $item = UserLevel::orderBy("level")->get();
        foreach($item as $K => $it){
            if(!empty($my_points[0]) && $it->level == $my_points[0]->level){
                $item[$K]->current = true;
            }else{
                $item[$K]->current = false;
            }
        }

        $employeeController = new EmployeeController();
        $employee = $employeeController->show(auth()->user());


        $userController = new UserController();
        $user_projects = $userController->getTaskJobs();
        $projects_cant = 0;
        if(!empty($user_projects["jobs"])){
            $projects_cant = $user_projects["jobs"];
        }


        $timeSheetsLines = 0;
        if(!empty($employee->id)){
            $timeSheetsLines = TimeSheetsLine::where('employee_id', '=', $employee->id)->count();
        }


        $my_points[0]->interactions = (!empty($interactions[0]))?$interactions[0]->cant:0;
        $my_points[0]->projects = $projects_cant;
        $my_points[0]->timesheets = $timeSheetsLines;
        $my_points[0]->my_current_level = $my_current_level;
        $my_points = array("my_score"=> $my_points, "level" => $item);
        return $this->SuccessResponse($my_points);
    }


    /**
     * @return JsonResponse
     */
    public function ranking(Request $request)
    {
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $page = ($request->input("page")) ? (int)$request->input("page") : 1;

        if (empty($pageSize)) {
            $pageSize = 10;
        }

        $user = auth()->user();
        $my_points = DB::table("vs_view_users_ranking")->orderBy("score", "desc")->get();
        foreach($my_points as $K => $it){
            if($user->id == $it->id){
                $my_points[$K]->me = true;
            }else{
                $my_points[$K]->me = false;
            }
        }

        if(!empty( $filterValue)){
            $my_points = $my_points->filter(function ($item, $key) use ($filterValue){
                if(stripos($item->name, $filterValue) !== false || stripos($item->last_name, $filterValue) !== false || stripos($item->position_title, $filterValue) !== false){
                    return $item;
                }
            });
        }
        $my_points->all();
        $total = count($my_points);
        $my_points = $my_points->splice($pageSize*($page-1));
        $my_points = $my_points->take($pageSize);
        $response["data"] =  $my_points;
        $response["total"] =  $total;
        return $this->SuccessResponse($response);
    }

}
