<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;

class ProjectController extends Controller
{
    use ApiResponser;

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $projects = Project::where('active', true)->latest('name')->get();
        return $this->SuccessResponse($projects, Response::HTTP_OK);
    }

    /**
     * @param $projectId
     * @return JsonResponse
     */
    public function show($projectId)
    {
        $project = Project::findOrFail($projectId);
        return $project;
    }
}
