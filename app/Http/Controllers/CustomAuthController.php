<?php

namespace App\Http\Controllers;


use App\Models\User;
use Dusterio\LumenPassport\LumenPassport;
use Illuminate\Http\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;
use \Laravel\Passport\Http\Controllers\AccessTokenController;
use Zend\Diactoros\Response as Psr7Response;
use Laravel\Passport\Passport;
use Laravel\Passport\Token;

class CustomAuthController extends AccessTokenController
{

    public function auth(ServerRequestInterface $request)
    {
        //sleep(20);
        $info = $request->getParsedBody();
        $username = strtolower(trim($info["username"]));
        $user_exist = User::where("email", "=", $username)->count();
        $user = User::where("email", "=", $username)->first();

        if ($user_exist < 1) {
            return  response()->json(
                [
                    'errors' => [
                        'status' => 402,
                        'messages' => ["We had a complication when trying to Login. Reason: Email " . $username . " Not Found"]
                    ]
                ],
                402
            );
        }

        if (!empty($user) && $user->status != 'A') {
            $message = "It seems that your user is inactive, please contact the App Administrator";
            return  response()->json(
                [
                    'errors' => [
                        'status' => 402,
                        'messages' => [$message]
                    ]
                ],
                402
            );
        }



        $tokenResponse = parent::issueToken($request);
        $token = $tokenResponse->getContent();


        // $tokenInfo will contain the usual Laravel Passort token response.
        $tokenInfo = json_decode($token, true);

        // Then we just add the user to the response before returning it.
        $username = strtolower(trim($request->getParsedBody()['username']));
        //$user = User::whereEmail($username)->first();
        $user = User::whereRaw("LOWER(email) = '" . $username . "'")->first();
        if ($user) {
            $user =  $user->getDataDescrypt();
        }
        $tokenInfo = collect($tokenInfo);
        $tokenInfo->put('user', $user);

        return $tokenInfo;
    }

    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return response()->json(
            [
                'errors' => [
                    'status' => 200,
                    'error' => 'Logout Success.'
                ]
            ],
            200
        );
    }
}
