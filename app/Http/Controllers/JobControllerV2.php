<?php


namespace App\Http\Controllers;


use App\Models\Agreement;
use App\Models\Employee;
use App\Models\Job;
use App\Models\Partner;
use App\Models\Planning;
use App\Models\Project;
use App\Models\ProjectsManager;
use App\Models\StateOdoo;
use App\Models\Task;
use App\Models\TimeSheetsLine;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class JobControllerV2 extends Controller
{
    use ApiResponser;

    public function jobsTaskLines(Request $request)
    {
        $employee = Employee::where('work_email', auth()->user()->email)->where('active', true)->first();
        if ($employee) {
            $jobs = Job::where('users_id', auth()->user()->id);


            $data = [];
            if ($request->state) {
                if ($request->state == 'today') {
                    $jobs = $jobs->whereBetween('visit_date', [
                        Carbon::now('America/Bogota')->subDays(1)->format('Y-m-d'),
                        Carbon::now('America/Bogota')->format('Y-m-d')
                    ])
                        ->where('status', '!=', 'FI')
                        ->get();
                    $data = $this->getJobs($jobs, null);
                }
                if ($request->state != "notReported" && $request->state != 'today') {
                    $taskIds = TimeSheetsLine::where('employee_id', $employee->id)
                        ->where('unit_amount', '>', 0)
                        ->where('custom_state', $request->state)
                        ->pluck('task_id')
                        ->toArray();
                    $jobs = $jobs->whereIn('task_odoo_id', array_unique($taskIds))
                        ->pluck('id')
                        ->toArray();
                    $jobs = Job::whereIn('id', array_unique($jobs))->get();
                    $data = $this->filterJobs($jobs, $request->state);
                } else {
                    $taskIds = TimeSheetsLine::where('employee_id', $employee->id)
                        ->where('unit_amount', 0)
                        ->where('custom_state', TimeSheetsLine::DRAFT)
                        ->whereDate('date', '<', Carbon::now('America/Bogota')->format('Y-m-d'))
                        ->pluck('task_id')
                        ->toArray();
                    $jobs = $jobs->whereIn('task_odoo_id', array_unique($taskIds))
                        ->pluck('id')
                        ->toArray();
                    $jobs = Job::whereIn('id', array_unique($jobs))->get();
                    $data = $this->filterJobs($jobs, $request->state);
                }
            } else {
                $jobs = $jobs->pluck('id')
                    ->toArray();
                $jobs = Job::whereIn('id', array_unique($jobs))->orderBy("id", "asc")->get();
                $data = $this->filterJobsMultiple($jobs);

            }
            return response()->json([
                'code' => Response::HTTP_OK,
                "data" => $data
            ], Response::HTTP_OK);
        } else {
            return response()->json([
                'code' => Response::HTTP_NOT_FOUND,
                'data' => 'Employee not found',
            ], Response::HTTP_NOT_FOUND);
        }
    }

    public function jobsTaskLinesGrouped(Request $request)
    {
        $employee = Employee::where('work_email', auth()->user()->email)->where('active', true)->first();
        if ($employee) {
            $jobs = Job::where('users_id', auth()->user()->id);
            $data = [];
            if ($request->state) {
                if ($request->state == 'today') {
                    $jobs = $jobs->whereBetween('visit_date', [
                        Carbon::now('America/Bogota')->subDays(1)->format('Y-m-d'),
                        Carbon::now('America/Bogota')->format('Y-m-d')
                    ])
                        ->where('status', '!=', 'FI')
                        ->get();
                    $data = $this->getJobs($jobs, null);
                }
                if ($request->state != "notReported" && $request->state != 'today') {
                    $taskIds = TimeSheetsLine::where('employee_id', $employee->id)
                        ->where('unit_amount', '>', 0)
                        ->where('custom_state', $request->state)
                        ->pluck('task_id')
                        ->toArray();
                    $jobs = $jobs->whereIn('task_odoo_id', array_unique($taskIds))
                        ->pluck('id')
                        ->toArray();
                    $jobs = Job::whereIn('id', array_unique($jobs))->get();
                    $data = $this->filterJobs($jobs, $request->state);
                } else {
                    $taskIds = TimeSheetsLine::where('employee_id', $employee->id)
                        ->where('unit_amount', 0)
                        ->where('custom_state', TimeSheetsLine::DRAFT)
                        ->whereDate('date', '<', Carbon::now('America/Bogota')->format('Y-m-d'))
                        ->pluck('task_id')
                        ->toArray();
                    $jobs = $jobs->whereIn('task_odoo_id', array_unique($taskIds))
                        ->pluck('id')
                        ->toArray();
                    $jobs = Job::whereIn('id', array_unique($jobs))->get();
                    $data = $this->filterJobs($jobs, $request->state);
                }
            } else {
                $jobs = $jobs->pluck('id')
                    ->toArray();
                $jobs = Job::whereIn('id', array_unique($jobs))->get();
                $data = $this->filterJobsMultiple($jobs);
            }
            return response()->json([
                'code' => Response::HTTP_OK,
                "data" => $data
            ], Response::HTTP_OK);
        } else {
            return response()->json([
                'code' => Response::HTTP_NOT_FOUND,
                'data' => 'Employee not found',
            ], Response::HTTP_NOT_FOUND);
        }
    }



    private function filterJobsMultiple($jobs, $state = null)
    {
        $employee = Employee::where('work_email', auth()->user()->email)->where('active', true)->first();
        $data = [];
        // if (count($jobs) > 0) {
        //     $data = [$jobs[0]];
        // }
        $ids = [];
        $project = [];
        //        foreach ($jobs as $job) {
        //            foreach ($data as $element) {
        //                if ($element->task_odoo_id != $job->task_odoo_id) {
        //                    $planning = Planning::where('task_id', $job->task_odoo_id)
        //                        ->where('employee_id', $employee->id)
        //                        ->first();
        //                    if ($planning) {
        //                        array_push($data, $job);
        //                    }
        //                }
        //            }
        //        }
        //dd($jobs->toArray());
        foreach ($jobs as $job) {
            if (!in_array($job->task_odoo_id, $ids)) {
                $ids[] = $job->task_odoo_id;
                $planning = Planning::where('task_id', $job->task_odoo_id)
                    ->where('employee_id', $employee->id)
                    ->first();
                if ($planning) {
                    array_push($data, $job);
                }
            }
        }

        $data2 = [];
        foreach ($data as $dt) {
            if (!in_array($dt->project_odoo_id, $project)) {
                $project[] = $dt->project_odoo_id;
                array_push($data2, $dt);
            }
        }
        return $this->getJobsMultiple($data2, $state);
    }

    private function getJobsMultiple($jobs, $stateRequest)
    {
        $data = [];
        $proyect = [];
        foreach ($jobs as $job) {
            $contact = $this->getContact($job);
            $workDays = $this->getJobsWorkdays($job);
            $project = $this->getProject($job);
            $task = $this->getTaskMultiple($job->project_odoo_id);
            $planning = $this->getPlanning($job);
            $projectManager = $this->getProjectManager($project);
            $agreement = $this->getAgreement($project);
            $extraInfo = $this->getExtraInfo($task[0], $job);
            //$checking = $this->getLastestChecking($job);
            $address = "Without information job";
            if ($project && isset($project->delivery_street) &&  $project->delivery_street) {
                $state = $project->delivery_state_id ? StateOdoo::where('id', $project->delivery_state_id)->first() : null;
                $codeZip = (int) $project->delivery_zip;
                $address = "$project->delivery_street, $project->delivery_city, $state->name, $codeZip";
            }
            array_push($data, [
                'id' => $job->id,
                'purchase_order' => $project && $project->cm_po ? $project->cm_po : "Without information",
                'work_order' => $project && $project->cm_wo ? $project->cm_wo : "Without information",
                'contract' => $agreement ? "#$agreement->code" : "Without information",
                'ubication' => $job->latitude ? true : false,
                'latitude' => $job->latitude,
                'longitude' => $job->longitude,
                'address' => $address,
                'extra_info' => $extraInfo ? $extraInfo : "Without information",
                'parameter' => $job->parameter,
                'places_id' => $job->places_id,
                'purpose' => $task ? $task[0]->name : $job->purpose,
                'status' => $job->status,
                'title' => $project ? $project->name : $job->title,
                'users_id' => $job->users_id,
                'visit_date' => $job->planning_odoo_id ? Carbon::parse($planning->start_datetime)->timezone($task ? $task[0]->tz : 'America/New_York') : Carbon::parse($job->visit_date)->timezone($task ? $task[0]->tz : 'America/New_York'),
                'end_date' => $planning ? Carbon::parse($planning->end_datetime)->timezone($task ? $task[0]->tz : 'America/New_York') : null,
                'end_data' => $planning ? Carbon::parse($planning->end_datetime)->timezone($task ? $task[0]->tz : 'America/New_York') : null,
                'project_odoo_id' => $job->project_odoo_id,
                'task_odoo_id' => $job->task_odoo_id,
                'checkin' => (bool) $job->checkin,
                'date_checkin' => $job->date_checkin,
                'date_create' => $job->date_create,
                'created_at' => $job->created_at,
                'deleted_at' => $job->deleted_at,
                'contact' => $contact,
                'jobsworkdays' => $workDays ? [$workDays] : [],
                'projectOdoo' => $project,
                'taskOdoo' => $task,
                'projectManager' => $projectManager,
            ]);
        }
        return $data;
    }


    private function filterJobs($jobs, $state = null)
    {
        $employee = Employee::where('work_email', auth()->user()->email)->where('active', true)->first();
        $data = [];
        // if (count($jobs) > 0) {
        //     $data = [$jobs[0]];
        // }
        $ids = [];
        //        foreach ($jobs as $job) {
        //            foreach ($data as $element) {
        //                if ($element->task_odoo_id != $job->task_odoo_id) {
        //                    $planning = Planning::where('task_id', $job->task_odoo_id)
        //                        ->where('employee_id', $employee->id)
        //                        ->first();
        //                    if ($planning) {
        //                        array_push($data, $job);
        //                    }
        //                }
        //            }
        //        }

        foreach ($jobs as $job) {
            if (!in_array($job->task_odoo_id, $ids)) {
                $ids[] = $job->task_odoo_id;
                $planning = Planning::where('task_id', $job->task_odoo_id)
                    ->where('employee_id', $employee->id)
                    ->first();
                if ($planning) {
                    array_push($data, $job);
                }
            }
        }
        return $this->getJobs($data, $state);
    }

    private function getJobs($jobs, $stateRequest)
    {
        $data = [];
        $proyect = [];
        foreach ($jobs as $job) {
            $contact = $this->getContact($job);
            $workDays = $this->getJobsWorkdays($job);
            $project = $this->getProject($job);
            $task = $this->getTask($job);
            $planning = $this->getPlanning($job);
            $projectManager = $this->getProjectManager($project);
            $agreement = $this->getAgreement($project);
            $extraInfo = $this->getExtraInfo($task, $job);
            //$checking = $this->getLastestChecking($job);
            $address = "Without information job";
            if ($project && isset($project->delivery_street) &&  $project->delivery_street){
                $state = $project->delivery_state_id ? StateOdoo::where('id', $project->delivery_state_id)->first() : null;
                $codeZip = (int) $project->delivery_zip;
                $address = "$project->delivery_street, $project->delivery_city, $state->name, $codeZip";
            }
            array_push($data, [
                'id' => $job->id,
                'purchase_order' => $project && $project->cm_po ? $project->cm_po : "Without information",
                'work_order' => $project && $project->cm_wo ? $project->cm_wo : "Without information",
                'contract' => $agreement ? "#$agreement->code" : "Without information",
                'ubication' => $job->latitude ? true : false,
                'latitude' => $job->latitude,
                'longitude' => $job->longitude,
                'address' => $address,
                'extra_info' => $extraInfo ? $extraInfo : "Without information",
                'parameter' => $job->parameter,
                'places_id' => $job->places_id,
                'purpose' => $task ? $task->name : $job->purpose,
                'status' => $job->status,
                'title' => $project ? $project->name : $job->title,
                'users_id' => $job->users_id,
                'visit_date' => $job->planning_odoo_id ? Carbon::parse($planning->start_datetime)->timezone($task ? $task->tz : 'America/New_York') : Carbon::parse($job->visit_date)->timezone($task ? $task->tz : 'America/New_York'),
                'end_date' => $planning ? Carbon::parse($planning->end_datetime)->timezone($task ? $task->tz : 'America/New_York') : null,
                'end_data' => $planning ? Carbon::parse($planning->end_datetime)->timezone($task ? $task->tz : 'America/New_York') : null,
                'project_odoo_id' => $job->project_odoo_id,
                'task_odoo_id' => $job->task_odoo_id,
                'checkin' => (bool) $job->checkin,
                'date_checkin' => $job->date_checkin,
                'date_create' => $job->date_create,
                'created_at' => $job->created_at,
                'deleted_at' => $job->deleted_at,
                'contact' => $contact,
                'jobsworkdays' => $workDays ? [$workDays] : [],
                'projectOdoo' => $project,
                'taskOdoo' => [$task],
                'projectManager' => $projectManager,
            ]);
        }
        return $data;
    }

    /**
     * @param $job
     * @return array
     */
    private function getContact($job)
    {
        $table = DB::table('customers');
        $contact = $table->where('id', $job->contact_id)->first();
        $partner = Partner::find($contact->id_odoo);
        return [
            'id' => $contact ? $contact->id : "Without information",
            'name' => $contact ? $contact->name : "Without information",
            'phone' => $contact ? $contact->phone : "Without information",
            'assistant' => null,
            'assistant_phone' => null,
            'other_phone' => null,
            'home_phone' => $partner ? $partner->phone : null,
            'mailing_address' => $partner ? $partner->email : null,
            'mobile' => $partner ? $partner->mobile : null,
            'created_at' => $contact ? $contact->created_at : null,
        ];
    }

    /**
     * @param $job
     * @return array
     */
    private function getJobsWorkdays($job)
    {
        $table = DB::table('jobs_workday');
        $workDay = $table->where('job_id', $job->id)->latest('id')->first();
        if ($workDay) {
            $tracker = DB::table('jobs_workday_time_tracker')
                ->where('jobs_workday_id', $workDay->id)
                ->latest('id')
                ->first();
            if (!$tracker) {
                DB::table('jobs_workday_time_tracker')->insert([
                    'jobs_workday_id' => $workDay->id,
                    'start' => null,
                    'end' => null,
                    'duration_seconds' => 0,
                    'created_at' => Carbon::now('America/Bogota'),
                    'upadted_at' => Carbon::now('America/Bogota'),
                    'status' => "A",
                ]);
            }
            $tracker = DB::table('jobs_workday_time_tracker')
                ->where('jobs_workday_id', $workDay->id)
                ->latest('id')
                ->first();
            return [
                'id' => $workDay->id,
                'user_id' => $workDay->user_id,
                'status' => $workDay->status,
                'total_seconds_duration' => $workDay->total_seconds_duration,
                'date' => $workDay->date,
                'created_at' => $workDay->created_at,
                'updated_at' => $workDay->updated_at,
                'jobs_workday_time_trackers' => [$tracker]
            ];
        } else {
            return null;
        }
    }

    /**
     * @param $job
     * @return mixed
     */
    private function getProject($job)
    {
        if ($job->project_odoo_id) {
            $project = Project::find($job->project_odoo_id);
            return $project;
        }
        return null;
    }

    private function getProjectManager($project)
    {
        if ($project) {
            $projectManager = ProjectsManager::find($project->user_id);
            if(!isset($projectManager->partner_id)){
                return null;
            }else{
                return Partner::find($projectManager->partner_id);
            }
        }
        return null;
    }

        /**
     * @param $job
     * @return mixed
     */
    private function getTaskMultiple($project_odoo_id)
    {
        $employee = Employee::where('work_email', auth()->user()->email)->where('active', true)->first();
        $task_result = [];
        $jobs = Job::where('project_odoo_id', $project_odoo_id)->get();
        $ids=[];

        foreach($jobs as $job){
            if (!in_array($job->task_odoo_id, $ids)) {
                $ids[] = $job->task_odoo_id;
                $planning = Planning::where('task_id', $job->task_odoo_id)
                    ->where('employee_id', $employee->id)
                    ->first();
                if ($planning) {
                    if ($job->task_odoo_id) {
                        $task_result[]= Task::find($job->task_odoo_id);
                    }
                }
            }


        }
        return (empty($task_result))?null:$task_result;
    }

    /**
     * @param $job
     * @return mixed
     */
    private function getTask($job)
    {
        if ($job->task_odoo_id) {
            $task = Task::find($job->task_odoo_id);
            return $task;
        }
        return null;
    }

    private function getJobsWorkdayLatest($worDay)
    {
        $table = DB::table('jobs_workday_time_tracker');
        $tracker = $table->where('jobs_workday_id', $worDay->id)
            ->latest('id')
            ->first();
        return $tracker;
    }

    private function getPlanning($job)
    {
        $planning = Planning::where('project_id', $job->project_odoo_id)
            ->where('task_id', $job->task_odoo_id)
            ->first();
        return $planning;
    }

    private function getAgreement($project)
    {
        if ($project && $project->agreement_id)
            return Agreement::find($project->agreement_id);
        return null;
    }

    private function getExtraInfo($task, $job)
    {
        if ($task && !empty($task) && $task->description != "<p><br></p>") {
            return $task->description;
        }
        if ($job && $job->extra_info) {
            return $job->extra_info;
        }
        return "Without information";
    }

    private function getLastestChecking($job)
    {
        $table = DB::table('jobs_checkings');
        $checking = $table->where('job_id', $job->id)->whereDate('created_at', Carbon::now('America/Bogota'))->first();
        if ($checking) {
            return $checking;
        }
    }

    private function getLines($taskId, $state)
    {
        $data = [];
        $lines = TimeSheetsLine::where('task_id', $taskId);
        if ($state) {
            $lines = $lines->where('unit_amount', '>', 0)
                ->where('custom_state', $state);
        }
        $lines = $lines->get();
        foreach ($lines as $line) {
            if ($line->custom_state == "waiting") {
                $line->custom_state = "submitted";
            }
            array_push($data, $line);
        }
        return $data;
    }
}
