<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Task;
use App\Models\Planning;
use App\Models\Project;
use App\Models\TimeSheetsLine;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Carbon\Carbon;
use DB;

use App\Models\Score;
use App\Models\UserLevel;
use App\Models\Ratings;


class NewsController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
    }


    /**
     * @return JsonResponse
     */
    public function show($news)
    {
        $im = auth()->user();
        $rating = new Ratings();
        $array['item_type'] = "N";
        $array['item_id'] = $news;
        $array['user_id'] = $im->id;
        $array['type'] = "R";
        $array['date'] = Carbon::now('America/Bogota');
        $Read = $rating->create($array);
        DB::select('CALL prd_register_score(?,?,?,?)',array((int)$im->id, (int)4,(int)0,'')); //3 Manual phone log
        return  $Read;

    }

    public function like($news)
    {

        $im = auth()->user();
       $rating = Ratings::where('item_type', "N")
        ->where('item_id',$news)
        ->where('type', "L")
        ->where('user_id',  $im->id)
        ->first();
    if ($rating) {
        $rating->setDataDeleteSoft();
        $rating->save();
        $data = $rating;
        return response()->json([
            "code" => Response::HTTP_OK,
            "data" => $data,
        ], Response::HTTP_OK);
    } else {
        DB::select('CALL prd_register_score(?,?,?,?)',array((int)$im->id, (int)6,(int)0,'')); //3 Manual phone log

        $rating = new Ratings();
        $dataCreate = $rating->setDataLike("N", $news);
        $rating = $rating->create($dataCreate);
        $data = $rating;
        return response()->json([
            "code" => Response::HTTP_OK,
            "data" => $data,
        ], Response::HTTP_OK);
    }


    }

}
