<?php


namespace App\Http\Controllers;


use App\Models\Employee;
use App\Models\Planning;
use App\Traits\ApiResponser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class PlanningController extends Controller
{
    use ApiResponser;

    public function index($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);
        $now = Carbon::now('America/Bogota')->format('Y-m-d');
        $pl =  Planning::where('employee_id', $employee->id)
            ->whereDate('start_datetime', $now)
            ->oldest('start_datetime')
            ->get();
        if (count($pl) == 0) {
            $pl = Planning::where('employee_id', $employee->id)
                ->whereDate('end_datetime', '>=', $now)
                ->oldest('start_datetime')
                ->get();
        }
        return $pl;
    }

    public function indexV2($employeeId)
    {
        $employee = Employee::findOrFail($employeeId);
        $startDate = Carbon::now('America/Bogota')->startOfWeek()->format('Y-m-d');
        $endDate = Carbon::now('America/Bogota')->addMonth()->format('Y-m-d');
        $pl =  Planning::where('employee_id', $employee->id)
            ->whereBetween('start_datetime', [$startDate, $endDate])
            ->oldest('start_datetime')
            ->get();
        if (count($pl) == 0) {
            $pl = Planning::where('employee_id', $employee->id)
                ->whereBetween('end_datetime', [$startDate, $endDate])
                ->oldest('start_datetime')
                ->get();
        }
        return $pl;
    }
}
