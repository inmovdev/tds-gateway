<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\TermsConditionService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TermsConditionController extends Controller {

    use ApiResponser;

    /**
     * @var $service
     */
    protected $service;

    /**
     * Constructor TermsConditionController
     *
     * @param TermsConditionService $service
     */
    public function __construct(TermsConditionService $service)
    {
        $this->service = $service;
    }

    /**
     * Return list resource
     *
     * @return void
     */
    public function index()
    {
        $response = $this->service->listTermsConditions();
        return $this->SuccessResponse($response);
    }

    /**
     * Store storage and return resource
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $response = $this->service->createTermsCondition($request->all());
        return $this->SuccessResponse($response, Response::HTTP_CREATED);
    }

    /**
     * Return a resource
     *
     * @param int $termsCondition
     * @return void
     */
    public function show($termsCondition)
    {
        $response = $this->service->showTermsCondition($termsCondition);
        return $this->SuccessResponse($response);
    }

    /**
     * Update storage and return resource
     *
     * @param Request $request
     * @param int $termsCondition
     * @return void
     */
    public function update(Request $request, $termsCondition)
    {
        $response = $this->service->updateTermsCondition($termsCondition, $request->all());
        return $this->SuccessResponse($response);
    }

    /**
     * Delete storage and return resource
     *
     * @param int $termsCondition
     * @return void
     */
    public function destroy($termsCondition)
    {
        $response = $this->service->deleteTermsCondition($termsCondition);
        return $this->SuccessResponse($response);
    }
}
