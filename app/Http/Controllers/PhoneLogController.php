<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Task;
use App\Models\Job;
use App\Models\PhoneLog;
use App\Models\TimeSheetsLine;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Carbon\Carbon;
use DB;

use App\Models\Score;
use App\Models\UserLevel;
use App\Models\Ratings;


class PhoneLogController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {

        $user = auth()->user();
        $items = PhoneLog::where("user_id", "=", $user->id)->get();
        foreach($items as $k => $v){
            $job = Job::where('id', $v->job_id)->get();
            if(!empty($job[0]->id)){

                $items[$k]->job_id = $job[0]->title ;
            }
        }
        return $this->SuccessResponse(array("data"=>$items));
    }


    /**
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'date' => 'required',
            'job_id' => 'required',
            'note' => 'nullable',
            'phone' => 'required',
            'manual' => 'nullable',
        ];

        $user = auth()->user();
        if(!empty($user->id)){
        $this->validate($request, $rules);
        $fields = $request->all();

        if($fields["manual"] == 1){
            DB::select('CALL prd_register_score(?,?,?,?)',array((int)$user->id, (int)3,(int)0,'')); //3 Manual phone log
        }else{
            DB::select('CALL prd_register_score(?,?,?,?)',array((int)$user->id, (int)2,(int)0,'')); //2 Auto phone log
        }

        $item = new PhoneLog();
        $item->name = $fields["name"];
        $item->date = $fields["date"];
        $item->job_id = $fields["job_id"];
        $item->note = $fields["note"];
        $item->phone = $fields["phone"];
        $item->manual = (!empty($fields["manual"]) && $fields["manual"] == 1)?1:0;
        $item->user_id = $user->id;
        $item->save();
        $items = PhoneLog::where("user_id", "=", $user->id)->get();
        return $this->SuccessResponse($items);
        }else{
            return response()->json(
                [
                    'errors' => [
                        'messages' => 'Unauthorized'
                    ]
                ],
                401
            );
        }
    }

}
