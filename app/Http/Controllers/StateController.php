<?php

namespace App\Http\Controllers;

use App\Services\CityService;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;


class StateController extends Controller
{

    use ApiResponser;

    /**
     * @var $service
     */
    public $service;

    /**
     * Constructor for StateController
     *
     * @param CityService $service
     */
    public function __construct(CityService $service)
    {
        $this->service = $service;
    }

    /**
     * Return resource List.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = $this->service->listStates();
        return $this->SuccessResponse($response);
    }

    /**
     * Store resource item.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->service->createState($request->all());
        return $this->SuccessResponse($response, Response::HTTP_CREATED);
    }

    /**
     * Show resource item.
     *
     * @param int $state
     * @return void
     */
    public function show($state)
    {
        $response = $this->service->showState($state);
        return $this->SuccessResponse($response);
    }

    /**
     * Update resource item
     *
     * @param Request $request
     * @param int $state
     * @return void
     */
    public function update(Request $request, $state)
    {
        $response = $this->service->updateState($state, $request->all());
        return $this->SuccessResponse($response);
    }

    /**
     * SoftDelte resource item
     *
     * @param int $state
     * @return void
     */
    public function destroy($state)
    {
        $response = $this->service->deleteState($state);
        return $this->SuccessResponse($response);
    }

    /**
     * Returns a list of states that is related to the country
     *
     * @param int $country
     * @return \Illuminate\Http\Response
     */
    public function indexCountry($country)
    {
        $response = $this->service->listStatesCountry($country);
        return $this->SuccessResponse($response);
    }

    /**
     * Returns a list of cities that is related to the state
     *
     * @param int $state
     * @return \Illuminate\Http\Response
     */
    public function indexState($state)
    {
        $response = $this->service->listCitiesState($state);
        return $this->SuccessResponse($response);
    }
}
