<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Task;
use App\Models\Planning;
use App\Traits\ApiResponser;
use App\Models\TimeSheetsLine;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class TaskController extends Controller
{
    use ApiResponser;

    /**
     * @param $projectId
     * @return JsonResponse
     */
    public function index($projectId)
    {

        $employeeController = new EmployeeController();
        $employee = $employeeController->show(auth()->user());

        $my_tasks = array();
        $planning = Planning::where('project_id', '=', $projectId)->where('is_published', '=', true)->where("employee_id", "=", $employee->id)->get();

        if (isset($planning) && !empty($planning)) {
            foreach ($planning as $plan) {
                $my_tasks[] = $plan->task_id;
            }
        }


        // List id, name and description add relation tags = table project.tags
        $project = Project::findOrFail($projectId);
        $tasks = Task::where('active', true)
            ->where('project_id', $project->id)
            ->whereIn('id', $my_tasks)
            ->latest('id')
            ->get();

        foreach ($tasks as $k => $t) {

            $sum = 0;
            $time = TimeSheetsLine::where('task_id', '=', $t->id)->where('employee_id', '=', $employee->id)->get();
            foreach ($time as $tssh) {
                $sum += $tssh->unit_amount;
            }
            $allocatedHours = Planning::where('project_id', '=', $projectId)->where('task_id', '=', $t->id)->where('is_published', '=', true)->where("employee_id", "=", $employee->id)->sum('allocated_hours');
            $planning = Planning::where('project_id', '=', $projectId)->where('task_id', '=', $t->id)->where('is_published', '=', true)->where("employee_id", "=", $employee->id)->get();
            $tasks[$k]->planned_hours = $allocatedHours;
            $tasks[$k]->effective_hours =  $sum;
        }
        //return $this->showAll($tasks, Response::HTTP_OK);
        return $this->SuccessResponse($tasks, Response::HTTP_OK);
    }

    public function show($taskId)
    {
        $task = Task::findOrFail($taskId);
        return $this->SuccessResponse($task, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function filterStates(Request $request)
    {
        $rules = [
            'stateOdooId' => 'required|exists:project_task_type,id',
            'project_id' => 'required|exists:project_project,id',
        ];
        $this->validate($request, $rules);
        $tasks = Task::where('project_id', $request->project_id)
            ->where('stage_id', $request->stateOdooId)
            ->where('active', true)
            ->get();
        return $this->SuccessResponse($tasks, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param $taskId
     * @return JsonResponse
     * @throws ValidationException
     */
    public function changeState(Request $request, $taskId)
    {
        $rules = [
            'stateOdooId' => 'required|exists:project_task_type,id'
        ];
        $this->validate($request, $rules);
        $task = Task::findOrFail($taskId);
        $task->changeState($request->stateOdooId);
        $task->save();
        return $this->SuccessResponse($task, Response::HTTP_OK);
    }
}
