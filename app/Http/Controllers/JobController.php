<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Employee;
use App\Models\Opportunity;
use App\Models\OpportunityTagUser;
use App\Models\Planning;
use App\Models\TimeSheetsLine;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Job;
use App\Models\JobsWorkday;
use App\Models\Project;
use App\Models\Task;
use App\Services\TimeSheetsLineService;
use App\Traits\ApiResponser;
use App\Traits\JobFill;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB as FacadesDB;
use DB;
use function Symfony\Component\VarDumper\Dumper\esc;

class JobController extends Controller
{
    use ApiResponser, JobFill;

    public function __construct()
    {
    }

    public function index($item)
    {
        //        if (auth()->check()) {
        //            //$service = new TimeSheetsLineService();
        //            $employeeController = new EmployeeController();
        //            $projectController = new ProjectController();
        //            $planningController = new PlanningController();
        //            //
        //            $employee = $employeeController->show(auth()->user());
        //            //$this->fill_job(auth()->user()->id);
        //
        //            if ($employee) {
        //
        //                $planning = $planningController->index($employee->id);
        //                $em = FacadesDB::table('vs_employees_with_availability')->where('email', '=', $employee->work_email)->select('availability')->get();
        //
        //                if (isset($em[0])) {
        //                    $availability = $em[0]->availability;
        //                } else {
        //                    $availability = '-';
        //                }
        //                if (isset($planning) && !empty($planning) && count($planning) > 0) {
        //                    $project = $projectController->show($planning[0]->project_id);
        //                    if ($project) {
        //                        $jobs = Job::where('project_odoo_id', $project->id)
        //                            ->where('users_id', auth()->user()->id)->get();
        //                        if (count($jobs) > 0) {
        //                            foreach ($jobs as $job) {
        //                                $job->update([
        //                                    //'project_odoo_id' => $planning[0]->project_id,
        //                                    //'task_odoo_id' => $planning[0]->task_id,
        //                                    'visit_date' => Carbon::now('America/Bogota'),
        //                                ]);
        //                                $workDay = FacadesDB::table('jobs_workday')
        //                                    ->where('job_id', $job->id)
        //                                    ->whereIn('status', ["S", "A", "I", "F"])
        //                                    ->whereDate('date', Carbon::now('America/Bogota')->format('Y-m-d'))
        //                                    ->latest('id')
        //                                    ->first();
        //                                if (!$workDay) {
        //                                    $table = FacadesDB::table('jobs_workday');
        //                                    $table->insert([
        //                                        'job_id' => $job->id,
        //                                        'user_id' => auth()->user()->id,
        //                                        'status' => 'S',
        //                                        'date' => Carbon::now('America/Bogota'),
        //                                        'opportunity_id' => null,
        //                                    ]);
        //                                }
        //                            }
        //                        }
        //                        $jobs = Job::where('project_odoo_id', $project->id)
        //                            ->where('users_id', auth()->user()->id);
        //                        if (isset($item['status']) && !empty($item['status'])) {
        //                            $jobs->where('status', $item['status']);
        //                        }
        //
        //                        if (isset($item['contract']) && !empty($item['contract'])) {
        //                            $jobs->where('contract', "like", '%' . $item['contract'] . '%');
        //                        }
        //
        //                        if (isset($item['latitude']) && !empty($item['latitude']) && isset($item['longitude']) && !empty($item['longitude'])) {
        //
        //                            $fields = "id, contact_id, title, purpose, date_create, parameter, users_id, address, contract, latitude, longitude, places_id, extra_info, checkin, date_checkin, visit_date, status, created_at, updated_at, deleted_at, project_odoo_id, task_odoo_id, status_opportunities";
        //                            $fields_avi = ", '-' AS  availability ";
        //                            $select_raw = "( 3959 * acos( cos( radians('" . $item["latitude"] . "') ) *
        //                            cos( radians( latitude ) ) *
        //                            cos( radians( longitude ) -
        //                            radians('" . $item["longitude"] . "') ) +
        //                            sin( radians('" . $item["latitude"] . "') ) *
        //                            sin( radians( latitude ) ) ) )
        //                            AS distance, $fields $fields_avi";
        //                            $jobs->select(FacadesDB::raw($select_raw))
        //                                ->havingRaw("( 3959 * acos( cos( radians('" . $item["latitude"] . "') ) *
        //                                        cos( radians( latitude ) ) *
        //                                        cos( radians( longitude ) -
        //                                        radians('" . $item["longitude"] . "') ) +
        //                                        sin( radians('" . $item["latitude"] . "') ) *
        //                                        sin( radians( latitude ) ) ) )
        //                                        < '5000000000000000000000000000000000000'")
        //                                ->orderByRaw('distance ASC')
        //                                ->groupBy(FacadesDB::raw($fields . ', availability'));
        //                        }
        //
        //                        if (array_key_exists('min_date', $item) && $item["min_date"] != "") {
        //                            $jobs->whereDate('visit_date', '>', Carbon::parse($item["min_date"])->toString())
        //                                ->latest('visit_date');
        //                        } else {
        //                            $jobs->whereDate('visit_date', '=', Carbon::now('America/Bogota')->format('Y-m-d'))
        //                                ->latest('visit_date');
        //                        }
        //                        $jobs = $jobs->get();
        //                        //dd($jobs);
        //
        //                        return $this->showAll($jobs, Response::HTTP_OK);
        //                    } else {
        //                        return $this->ErrorResponse("Project no found", Response::HTTP_NOT_FOUND);
        //                    }
        //                } else {
        //                    return $this->ErrorResponse("Planning no found", Response::HTTP_NOT_FOUND);
        //                }
        //            } else {
        //                return $this->ErrorResponse("Employee no found", Response::HTTP_NOT_FOUND);
        //            }
        //        }
        return [];
    }

    /**
     * @param array $item
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function indexV2($item = [])
    {


        if (auth()->check()) {
            $employee = Employee::where('work_email', auth()->user()->email)->where('active', true)->first();
            if ($employee) {

                $return_debbug = $this->createJobs($employee);

                 /*FacadesDB::table('debug')->insert(
                    ['text' => json_encode($return_debbug)]
                );*/
                $startDate = Carbon::now('America/Bogota')->startOfWeek()->format('Y-m-d');
                $endDate = Carbon::now('America/Bogota')->addMonth()->format('Y-m-d');
                $plannings = Planning::where('employee_id', $employee->id)
                    ->where('is_published', true)
                    ->pluck('id')
                    ->toArray();
                $jobs = Job::where('users_id', auth()->user()->id)
                    ->where('status', '!=', "FI")
                    ->whereIn('planning_odoo_id', array_unique($plannings));
                if (isset($item['status']) && !empty($item['status'])) {
                    $jobs = $this->filterStatus($jobs, $item["status"]);
                }
                if (isset($item['contract']) && !empty($item['contract'])) {
                    $jobs = $this->filterContract($jobs, $item["contract"]);
                }
                if (isset($item['latitude']) && !empty($item['latitude']) && isset($item['longitude']) && !empty($item['longitude'])) {
                    $jobs = $this->filterLocation($jobs, $item["longitude"], $item["latitude"]);
                }
                if (isset($item["min_date"]) && !empty($item["min_date"])) {
                    $jobs = $this->filterMinDate($jobs, $item["min_date"]);
                } else {
                    $jobs = $jobs->whereBetween('visit_date', [$startDate, $endDate]);
                }
                $jobs = $jobs->latest('visit_date')->get();
                return $this->showAll($jobs, Response::HTTP_OK);
            } else {
                return $this->ErrorResponse("Employee no found", Response::HTTP_NOT_FOUND);
            }
        } else {
            return $this->ErrorResponse("Unauthorized", Response::HTTP_UNAUTHORIZED);
        }
    }

    /**
     * @param $jobs
     * @param $status
     * @return mixed
     */
    private function filterStatus($jobs, $status)
    {
        return $jobs->where('status', $status);
    }

    /**
     * @param $jobs
     * @param $contract
     * @return mixed
     */
    private function filterContract($jobs, $contract)
    {
        return $jobs->where('contract', "like", '%' . $contract . '%');
    }

    /**
     * @param $jobs
     * @param $longitude
     * @param $latitude
     */
    private function filterLocation($jobs, $longitude, $latitude)
    {
        $fields = "id, contact_id, title, purpose, date_create, parameter, users_id, address, contract, latitude, longitude, places_id, extra_info, checkin, date_checkin, visit_date, status, created_at, updated_at, deleted_at, project_odoo_id, task_odoo_id, status_opportunities";
        $fields_avi = ", '-' AS  availability ";
        $select_raw = "( 3959 * acos( cos( radians('" . $latitude . "') ) *
                            cos( radians( latitude ) ) *
                            cos( radians( longitude ) -
                            radians('" . $longitude . "') ) +
                            sin( radians('" . $latitude . "') ) *
                            sin( radians( latitude ) ) ) )
                            AS distance, $fields $fields_avi";
        $jobs->select(FacadesDB::raw($select_raw))
            ->havingRaw("( 3959 * acos( cos( radians('" . $latitude . "') ) *
                                        cos( radians( latitude ) ) *
                                        cos( radians( longitude ) -
                                        radians('" . $longitude . "') ) +
                                        sin( radians('" . $latitude . "') ) *
                                        sin( radians( latitude ) ) ) )
                                        < '5000000000000000000000000000000000000'")
            ->orderByRaw('distance ASC')
            ->groupBy(FacadesDB::raw($fields . ', availability'));
    }

    /**
     * @param $jobs
     * @param $minDate
     * @return mixed
     */
    private function filterMinDate($jobs, $minDate)
    {
        return $jobs->whereDate('visit_date', '>', Carbon::parse($minDate)->toString());
    }

    public function show($jobId)
    {
        $employeeController = new EmployeeController();
        $projectController = new ProjectController();
        $planningController = new PlanningController();
        //
        $employee = $employeeController->show(auth()->user());
        if ($employee) {

            $em = FacadesDB::table('vs_employees_with_availability')->where('id', '=', $employee->id)->select('availability')->get();

            if (isset($em[0])) {
                $availability = $em[0]["availability"];
            } else {
                $availability = '-';
            }


            $planning = $planningController->index($employee->id);
            if ($planning) {
                //$project = $projectController->show($planning->project_id);
                //if ($project) {

                $fields = "id, contact_id, title, purpose, date_create, parameter, users_id, address, contract, latitude, longitude, places_id, extra_info, checkin, date_checkin, visit_date, status, created_at, updated_at, deleted_at, project_odoo_id, task_odoo_id, status_opportunities,  '" . $availability . "' availability";
                $fields_avi = ", '-' AS  availability ";
                $jobs = Job::where('id', $jobId)
                    ->get();
                return $this->showAll($jobs, Response::HTTP_OK);
                //} else {
                //    return $this->ErrorResponse("Project no found", Response::HTTP_NOT_FOUND);
                //}
            } else {
                return $this->ErrorResponse("Planning no found", Response::HTTP_NOT_FOUND);
            }
        } else {
            return $this->ErrorResponse("Employee no found", Response::HTTP_NOT_FOUND);
        }
    }

    public
    function get_nearly_jobs(Request $request)
    {

        $rules = [
            'latitude' => 'required',
            'longitude' => 'required',
        ];
        $this->validate($request, $rules);

        $item = request(['latitude', 'longitude']);

        $user = auth()->user()->getDataDescrypt()->getAttributes();

        $fields = "id, contact_id, title, purpose, date_create, parameter, users_id, address, contract, latitude, longitude, places_id, extra_info, checkin, date_checkin, visit_date, status, created_at, updated_at, deleted_at, project_odoo_id, task_odoo_id, status_opportunities ";

        $item = FacadesDB::select("SELECT
                            ( 3959 * acos( cos( radians('" . $item["latitude"] . "') ) *
                            cos( radians( latitude ) ) *
                            cos( radians( longitude ) -
                            radians('" . $item["longitude"] . "') ) +
                            sin( radians('" . $item["latitude"] . "') ) *
                            sin( radians( latitude ) ) ) )
                            AS distance,
                            $fields
                            FROM jobs
                            where users_id = '" . $user["id"] . "'
                            group by  $fields
                            HAVING
                            ( 3959 * acos( cos( radians('" . $item["latitude"] . "') ) *
                            cos( radians( latitude ) ) *
                            cos( radians( longitude ) -
                            radians('" . $item["longitude"] . "') ) +
                            sin( radians('" . $item["latitude"] . "') ) *
                            sin( radians( latitude ) ) ) )
                            < '5000000000000000000'
                            ORDER BY distance ASC ");


        $output["jobs"] = $item;

        return response()->json($output);
    }


    public
    function jobsTarget()
    {
        $userId = auth()->user()->id;
        $model = new Job();
        $results = $model->jobsTarget();
        $array = [];
        foreach ($results as $result) {
            if ($result->id == $userId) {
                array_push($array, $result);
            }
        }
        return \response()->json([
            'data' => $array,
            'code' => Response::HTTP_OK
        ]);
    }


    public
    function job_fill_test($iduser)
    {
        //$this->fill_job($iduser);
    }

    public
    function jobChecking(Request $request, $jobId)
    {
        $this->validate($request, [
            'longitude' => 'required',
            'latitude' => 'required',
        ]);
        $job = Job::findOrFail($jobId);
        $checking = FacadesDB::table('jobs_checkings')->where('job_id', $job->id)->whereDate('created_at', Carbon::now('America/Bogota'))->first();
        $job->setDataUpdateChecking($request->all());
        $job->save();
        /*$workDay = FacadesDB::table('jobs_workday')->where('job_id', $job->id)->where('date', Carbon::now('America/Bogota')->format('Y-m-d'))->first();
        if ($workDay) {
            FacadesDB::table('jobs_workday')->where('job_id', $job->id)->where('date', Carbon::now('America/Bogota')->format('Y-m-d'))->update([
                "status" => "A",
            ]);
        }*/
        if (!$checking) {
            $dataCreate = [
                'checking' => true,
                'longitude' => $request->longitude,
                'latitude' => $request->latitude,
                'job_id' => $job->id,
                'created_at' => Carbon::now('America/Bogota'),
                'updated_at' => Carbon::now('America/Bogota'),
            ];
            FacadesDB::table('jobs_checkings')->insert($dataCreate);
        }
        $im = auth()->user();
        DB::select('CALL prd_register_score(?,?,?,?)', array((int) $im->id, 11, 0, '')); //11
        return response()->json([
            "data" => $job,
        ], 200);
    }

    /**
     * Create jobs
     * @param Employee $employee
     */
    private function createJobs(Employee $employee)
    {
        $opportunitiesIds = OpportunityTagUser::where('user_id', auth()->user()->id)->pluck('opportunity_id')->toArray();
        $projectsIds = Opportunity::whereIn('id', $opportunitiesIds)->pluck('id_odoo')->toArray();
        $projects = Project::whereIn('id', array_unique($projectsIds))->get();
        $task_return = [];

        foreach ($projects as $project) {
            $tasks = Task::where('project_id', $project->id)
                ->where('employee_id', $employee->id)->get();
                $task_return[$project->id]["project"] = $project;    //dd();
                $task_return[$project->id]["task"] = $tasks;    //dd();

            foreach ($tasks as $task) {

                $plannings = Planning::where('project_id', $project->id)
                    ->where('task_id', $task->id)
                    ->where('employee_id', $employee->id)
                    ->where('is_published', true)
                    ->get();

                   /* FacadesDB::table('debug')->insert(
                        ['text' =>  $task->id.', '.$project->id. ', '. $employee->id ]
                    );

                    FacadesDB::table('debug')->insert(
                        ['text' =>  json_encode($plannings) ]
                    );*/

                $opportunity = Opportunity::where('id_odoo', $project->id)->first();
                $customer = Customer::where('salesforce_account_id', $opportunity->account_id)->first();
                if (count($plannings) > 0) {
                    $this->createJobsSinglePlannings($plannings, $customer, $opportunity);
                }
            }
        }
        return $task_return;
    }

    /**
     * @param $plannings
     * @param $customer
     * @param $opportunity
     */
    private function createJobsMultiPlannings($plannings, $customer, $opportunity)
    {
        $now = Carbon::now('America/Bogota');
        for ($i = 0; $i < count($plannings); $i++) {
            $startDate = Carbon::parse($plannings[$i]->start_datetime)->subHours(5);
            $endDay = Carbon::parse($plannings[$i]->end_datetime)->subHours(5);
            $visitDate = $startDate;
            $visitDateFormat = $visitDate->format('Y-m-d');
            $planningId = $plannings[$i]->id;
            $job = Job::where('users_id', auth()->user()->id)
                ->where('visit_date', $visitDateFormat)
                ->where('planning_odoo_id', $planningId)
                ->first();
            /* if (!$job) {
                $job = new Job();
                $dataCreate = $job->setDataCreate($plannings[$i], $visitDate, $opportunity, $customer, $now);
                $job = $job->create($dataCreate);
                $jobsWorkday = new JobsWorkday();
                $dataCreate = $jobsWorkday->setDataCreate($job, $opportunity, $visitDate);
                $jobsWorkday = $jobsWorkday->create($dataCreate);
            } else {
                $planning = $plannings[$i];
                $job->setDataUpdateMulti($planning);
                $job->save();
            } */
        }
    }

    /**
     * @param $plannings
     * @param $customer
     * @param $opportunity
     */
    private function createJobsSinglePlannings($plannings, $customer, $opportunity)
    {
        $now = Carbon::now('America/Bogota');
        /*FacadesDB::table('debug')->insert(
            ['text' =>  json_encode($plannings) ]
        );*/
        foreach ($plannings as $planning) {
            Job::where('planning_odoo_id', $planning->id)->update([
                'status' => "FI",
            ]);
            $startDay = Carbon::parse($planning->start_datetime)->subHours(5);
            $endDay = Carbon::parse($planning->end_datetime)->subHours(5);
            $startDayFormat = Carbon::parse($startDay->format('Y-m-d'));
            $endDayFormat = Carbon::parse($endDay->format('Y-m-d'))->endOfDay();
            $daysApart = $startDayFormat->diffInDays($endDayFormat);
            for ($i = 0; $i <= $daysApart; $i++) {
                $job = null;
                if ($i == 0) {
                    $visitDate = $startDay;
                } else {
                    $visitDate = $startDay->addDays(1);
                }
                $start = $visitDate->format('Y-m-d');
                $job = Job::where('users_id', auth()->user()->id)
                    ->where('planning_odoo_id', $planning->id)
                    ->whereDate('visit_date', $start)
                    ->first();
                /*FacadesDB::table('debug')->insert(
                        ['text' =>  json_encode($job) ]
                    );*/
                if ($job) {
                    $job->setDataUpdate($planning, $i);
                    $job->save();
                } else {
                    //dd("Here");
                    $job = new Job();
                    $dataCreate = $job->setDataCreate($planning, $startDay, $opportunity, $customer, $now, $i);
                    $job = $job->create($dataCreate);
                    $jobsWorkday = new JobsWorkday();
                    $dataCreate = $jobsWorkday->setDataCreate($job, $opportunity, $startDay);
                    $jobsWorkday = $jobsWorkday->create($dataCreate);
                }
            }
        }
    }
}
