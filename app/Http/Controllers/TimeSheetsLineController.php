<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\State;
use App\Models\Task;
use App\Models\Planning;
use App\Models\Calendar;
use App\Models\TimeSheetsLine;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB as FacadesDB;
use DB;
use function Symfony\Component\VarDumper\Dumper\esc;

class TimeSheetsLineController extends Controller
{
    use ApiResponser;

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $timeSheetsLines = TimeSheetsLine::latest('id')->get();
        return $this->SuccessResponse($timeSheetsLines);
    }

    public function indexTask($taskId)
    {
        $return = array();

        $employeeController = new EmployeeController();
        $planningController = new PlanningController();
        //

        $task = Task::findOrFail($taskId);

        $employee = $employeeController->show(auth()->user());

        if ($employee) {
            $plnings = array();
            $planning = Planning::where('task_id', '=', $taskId)
                ->where('is_published', '=', true)
                ->where("employee_id", "=", $employee->id)
                ->get();

            $sum_ts = 0;
            if (isset($planning) && !empty($planning)) {

                foreach ($planning as $plan) {
                    $sum_ts = 0;

                    $start = explode(' ', $plan->start_datetime);

                    $end = explode(' ', $plan->end_datetime);
                    $just_one_day = false;

                    $dif_start = Carbon::parse($plan->start_datetime)->subHours(5);
                    $dif_end = Carbon::parse($plan->end_datetime)->subHours(5);
                    $startDate = $dif_start->format("Y-m-d");
                    $endDate = $dif_end->format("Y-m-d");
                    $days_diference = Carbon::parse($startDate)->diffInDays(Carbon::parse($endDate));
                    if ($start[0] == $end[0]) {
                        $just_one_day = true;
                    }

                    //dd($dif_start, $dif_end);
                    if ($dif_start <= $dif_end) {
                        $hours = $plan->allocated_hours;
                        $days = $days_diference + 1; //$plan->working_days_count;

                        if ($hours > 0 && $days > 0) {

                            $hours = $hours / $days;
                        } else {
                            //no days, no hours

                            $hours = 0;
                        }

                        $return['From ' . $start[0] . " to " . $end[0]]["Year " . $dif_start->format('Y') . " - Week: " . $dif_start->weekOfYear][$dif_start->format('Y-m-d')]["hours_planning"] = $hours;

                        $timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                            ->where('employee_id', $employee->id)
                            ->whereDate('date', $dif_start)
                            ->get();
                        if (count($timeSheetLines) == 0) {
                            /*$attributes = array();
                            $attributes["description"] = '/';
                            $attributes["date"] = $dif_start->format('Y-m-d');
                            $attributes["email_employee"] = $employee->work_email;
                            $attributes["unit_amount"] = 0;
                            $attributes["task_id"] = $taskId;
                            $attributes["employee_id"] = $employee->id;
                            $attributes["custom_state"] = "draft";
                            $task = Task::findOrFail($taskId);

                            $timeSheetsLine = new TimeSheetsLine();
                            $data = $timeSheetsLine->setDataCreate($attributes, $task);
                            $timeSheetsLine = $timeSheetsLine->create($data);

                            $timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                                ->where('employee_id', $employee->id)
                                ->whereDate('date', $dif_start)
                                ->get();*/
                        }

                        foreach ($timeSheetLines as $line) {
                            if ($line->custom_state == "waiting") {
                                $line->custom_state = "submitted";
                            }
                            $return['From ' . $start[0] . " to " . $end[0]]["Year " . $dif_start->format('Y') . " - Week: " . $dif_start->weekOfYear][$dif_start->format('Y-m-d')]["timesheet_hours"] = $line;
                            $sum_ts += $line->unit_amount;
                        }
                        $dif_start = $dif_start;
                    }
                }
                $task->effective_hours = $sum_ts;
                $task->planned_hours = (!empty($planning[0])) ? $planning[0]->allocated_hours : 0;
                $return["task"] = $task;
                $return["planning"] = $planning;
                return $this->SuccessResponse($return, Response::HTTP_OK);
            } else {
                return $this->ErrorResponse([], Response::HTTP_OK);
            }
        } else {
            return $this->ErrorResponse([], Response::HTTP_OK);
        }
    }

    public function indexTaskV2(Request $request, $taskId)
    {
        $state = $request->state;
        $task = Task::findOrFail($taskId);
        $employee = Employee::where('work_email', '=', auth()->user()->email)
            ->where('active', true)
            ->first();
        if ($employee) {
            $plannings = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->oldest('id')
                ->get();
            // dd(count($plannings));
            if (count($plannings) > 1) {
                $array = $this->showMultiPlanning($plannings, $taskId, $employee, $state);
                return response()->json($array, Response::HTTP_OK);
            }
            if (count($plannings) == 1) {
                //dd("chupelo");
                $array = $this->showSinglePlanningTryingToFix($plannings, $taskId, $employee, $state);
                return response()->json($array, Response::HTTP_OK);
            }
            if (count($plannings) == 0) {
                return response()->json([], Response::HTTP_OK);
            }
        } else {
            return response()->json([], Response::HTTP_OK);
        }
    }

    public function indexTaskV3(Request $request, $taskId)
    {
        $state = $request->state;
        $task = Task::findOrFail($taskId);
        $employee = Employee::where('work_email', '=', auth()->user()->email)
            ->where('active', true)
            ->first();
        if ($employee) {
            $plannings = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->oldest('id')
                ->get();
            if (count($plannings) > 1) {
                return $this->showMultiPlanning($plannings, $taskId, $employee, $state);
            }
            if (count($plannings) == 1) {
                return $this->showSinglePlanning($plannings, $taskId, $employee, $state);
            }
            if (count($plannings) == 0) {
                return response()->json([], Response::HTTP_OK);
            }
        } else {
            return response()->json([], Response::HTTP_OK);
        }
    }


    public function indexTaskV4(Request $request, $taskId)
    {
        $grouped = $request->grouped;
        $state = $request->state;
        $task = Task::findOrFail($taskId);

        $employee = Employee::where('work_email', '=', auth()->user()->email)
            ->where('active', true)
            ->first();
        if ($employee) {
            $plannings = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->oldest('id')
                ->get();
            //dd(count($plannings));
            if (count($plannings) > 1) {
                //dd("t1");
                $array = $this->showMultiPlanningV4($plannings, $taskId, $employee, $state, $grouped);
                return response()->json($array, Response::HTTP_OK);
            }
            if (count($plannings) == 1) {
                 //dd("t2");
                $array = $this->showSinglePlanningTryingToFixV4($plannings, $taskId, $employee, $state, $grouped);
                return response()->json($array, Response::HTTP_OK);
            }
            if (count($plannings) == 0) {
                //dd("t3");
                return response()->json([], Response::HTTP_OK);
            }
        } else {
            return response()->json([], Response::HTTP_OK);
        }
    }

    private function showMultiPlanning($plannings, $taskId, $employee, $state = null, $grouped = null)
    {
        $array = [];
        $dates = [];
        $task = Task::findOrFail($taskId);
        foreach ($plannings as $planning) {
            $startDatePlanning = Carbon::parse($planning->start_datetime)->subHours(5);
            $endDatePlanning = Carbon::parse($planning->end_datetime)->subHours(5);
            $dates = array_merge($dates, [
                $startDatePlanning->startOfWeek()->format('Y-m-d'),
                $endDatePlanning->endOfWeek()->format('Y-m-d'),
            ]);
        }
        $datesNew = [];
        foreach (array_unique($dates) as $date) {
            array_push($datesNew, $date);
        }
        $maxCount = count($datesNew) / 2;
        for ($i = 0; $i < $maxCount; $i++) {
            $initPosition = $i;
            $endPosition = $i + 1;
            if ($i > 0) {
                if ($i == 1) {
                    $initPosition = $i + 1;
                    $endPosition = $initPosition + 1;
                } else {
                    if ($i % 2 === 0) {
                        $initPosition = $i + 2;
                        $endPosition = $initPosition + 1;
                    } else {
                        $initPosition = $i + 3;
                        $endPosition = $initPosition + 1;
                    }
                }
            }
            $dateStart = $datesNew[$initPosition];
            $dateEnd = $datesNew[$endPosition];
            $year = explode('-', $dateEnd)[0];
            $numberWeek = Carbon::parse($dateStart)->weekOfYear;
            $plannings = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->where(function ($query) use ($dateStart, $dateEnd) {
                    $query->where('start_datetime', '>=', "$dateStart 00:00:00");
                    $query->where('end_datetime', '<=', "$dateEnd 23:59:59");
                })
                ->oldest('id')
                ->get();
            $labelFrom = "From $dateStart to $dateEnd";
            $labelWeek = "Year $year - Week: $numberWeek";
            array_push($array, [
                "$labelFrom" => [
                    "$labelWeek" => [],
                ]
            ]);
            $positionArray = count($array) - 1;
            foreach ($plannings as $planning) {
                $startDatePlanning = Carbon::parse($planning->start_datetime)->subHours(5);
                $timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                    ->where('employee_id', $employee->id)
                    ->whereDate('date', $startDatePlanning->format('Y-m-d'))
                    ->get();
                if (count($timeSheetLines) == 0) {
                    /*$attributes = [
                        "description" => '',
                        "date" => $startDatePlanning->format('Y-m-d'),
                        "email_employee" => $employee->work_email,
                        "unit_amount" => 0,
                        "task_id" => $taskId,
                        "employee_id" => $employee->id,
                        "custom_state" => "draft",
                    ];
                    $timeSheetsLines = new TimeSheetsLine();
                    $data = $timeSheetsLines->setDataCreate($attributes, $task);
                    $timeSheetsLines->create($data);*/
                }
            }
            $initDate = explode(' ', $labelFrom)[1];
            $finalDate = explode(' ', $labelFrom)[3];
            $timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                ->where('employee_id', $employee->id)
                ->whereBetween('date', [
                    $initDate,
                    $finalDate
                ])
                ->oldest('date');
            if ($state) {
                if ($state != "notReported" && $state != 'today') {
                    $timeSheetLines = $this->filterState($timeSheetLines, $state);
                } else {
                    if ($state == 'today') {
                        $timeSheetLines = $timeSheetLines->where('employee_id', $employee->id)
                            ->where('unit_amount', 0)
                            ->where('date', Carbon::now('America/Bogota')->format('Y-m-d'))
                            ->where('custom_state', TimeSheetsLine::DRAFT)
                            ->oldest('date');
                    } else {
                        $timeSheetLines = $timeSheetLines->where('employee_id', $employee->id)
                            ->where('unit_amount', 0)
                            ->where('date', '<', Carbon::now('America/Bogota')->format('Y-m-d'))
                            ->where('custom_state', TimeSheetsLine::DRAFT)
                            ->oldest('date');
                    }
                }
            }
            $timeSheetLines = $timeSheetLines->get();
            $timeSheets = [];
            if (count($timeSheetLines) > 0) {
                foreach ($timeSheetLines as $timeSheetLine) {
                    if ($timeSheetLine->custom_state == "waiting") {
                        $timeSheetLine->custom_state = "submitted";
                    }
                    $allocatedHours = Planning::where('task_id', $timeSheetLine->task_id)
                        ->where('is_published', true)
                        ->where("employee_id", $employee->id)
                        ->whereBetween('start_datetime', [
                            "$timeSheetLine->date 00:00:00",
                            "$timeSheetLine->date 23:59:59"
                        ])
                        ->sum('allocated_hours');
                    $timeSheetLine->name = ($timeSheetLine->name == '/') ? '' : $timeSheetLine->name;
                    $object = [
                        "$timeSheetLine->date" => [
                            "hours_planning" => (float) $allocatedHours,
                            "timesheet_hours" => $timeSheetLine,
                        ]
                    ];
                    array_push($timeSheets, $object);
                }
                if (count($timeSheets) > 0) {
                    $array[$positionArray][$labelFrom][$labelWeek] = [
                        "timeSheets" => $timeSheets
                    ];
                }
            } else {
                if (count($array[$positionArray][$labelFrom][$labelWeek]) == 0) {
                    unset($array[$positionArray][$labelFrom]);
                }
            }
        }
        foreach ($array as $item) {
            array_filter($item);
        }
        if (count($array) > 0) {
            $allocatedHours = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->oldest('id')
                ->sum('allocated_hours');
            $startDates = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->oldest('id')
                ->pluck('start_datetime')
                ->toArray();
            $dates = TimeSheetsLine::where('task_id', $task->id)->where('employee_id', $employee->id)->oldest('date')->pluck('date')->toArray();
            $sumTs = TimeSheetsLine::where('task_id', $task->id)->where('employee_id', $employee->id)->sum('unit_amount');
            $task->effective_hours = $sumTs;
            $task->planned_hours = (int) $allocatedHours;
            $array[0]["task"] = $task;
            $array[0]["planning"] = count($plannings) > 0 ? [$plannings[0]] : null;
            //$array[0]["startDates"] = $startDates;
            //$array[0]["dates"] = $dates;
        }
        $finalArray = [];
        if (count($array) > 0) {
            foreach ($array as $item) {
                if (!empty($item) || count($item) > 0) {
                    array_push($finalArray, $item);
                }
            }
        }
        return $finalArray;
    }

    private function showMultiPlanningV4($plannings, $taskId, $employee, $state = null, $grouped = null)
    {

        ///resource_calendar_attendance
        $calendar =  Calendar::where("resource_calendar.id", '=',  $employee->resource_calendar_id)
            ->join("resource_calendar_attendance", 'resource_calendar.id', '=', 'resource_calendar_attendance.calendar_id')
            ->selectRAW("DISTINCT(resource_calendar_attendance.dayofweek) as dayofweek")->get();
        $attendance_days = [];
        foreach ($calendar as $cday) {
            if ($cday->dayofweek == 6) { // Domingo
                $attendance_days[0] = 0;
            } else {
                $attendance_days[$cday->dayofweek + 1] = $cday->dayofweek + 1;
            }
        }

        $array = [];
        $dates = [];
        $diference_dates = [];
        $task = Task::findOrFail($taskId);

        $dd = [];
        $sdsd = 0;
        foreach ($plannings as $planning) {
            $diference_dates[$planning->id] = 0;
            $startDatePlanning = Carbon::parse($planning->start_datetime)->subHours(5);
            $endDatePlanning = Carbon::parse($planning->end_datetime)->subHours(5);

            $start = $startDatePlanning->format('Y-m-d');
            $end = $endDatePlanning->format('Y-m-d');

            $dd[$sdsd]['start'] = $start;
            $dd[$sdsd]['end'] = $end;
            $sdsd++;

            $daysDifference = $startDatePlanning->diffInDays($endDatePlanning) == 0 ? 1 : $startDatePlanning->diffInDays($endDatePlanning) + 1;

            //dd($daysDifference);

            for ($abc = 0; $abc < $daysDifference; $abc++) {
                $day = Carbon::parse($planning->start_datetime)->addDays($abc)->format('Y-m-d');
                $day_of_the_week = Carbon::parse($planning->start_datetime)->addDays($abc)->dayOfWeek;

                if (!empty($planning->planning_mass_id) || in_array($day_of_the_week, $attendance_days)) {
                    $diference_dates[$planning->id] = $diference_dates[$planning->id] +  1;
                }
            }

            for ($i = 0; $i < $daysDifference; $i++) {
                $dates = array_merge($dates, [
                    Carbon::parse($start)->addDays($i)->startOfWeek()->format('Y-m-d'),
                    Carbon::parse($end)->addDays($i)->startOfWeek()->format('Y-m-d'),
                ]);
            }
        }

        $datesNew = [];
        sort($dates);
        //dd($dates, $dd);
        foreach (array_unique($dates) as $date) {
            array_push($datesNew, $date);
            $newWeek = Carbon::parse($date)->endOfWeek()->format('Y-m-d');
            array_push($datesNew, $newWeek);
        }



        $maxCount = count($datesNew);// / 2;

        for ($i = 0; $i < $maxCount; $i= $i+2 ) {
            $initPosition = $i;
            $endPosition = $i + 1;
            /*if ($i > 0) {
                if ($i == 1) {
                    $initPosition = $i + 1;
                    $endPosition = $initPosition + 1;
                } else {
                    if ($i % 2 === 0) {
                        $initPosition = $i + 2;
                        $endPosition = $initPosition + 1;
                    } else {
                        $initPosition = $i + 3;
                        $endPosition = $initPosition + 1;
                    }
                }
            }*/

            $dateStart = $datesNew[$initPosition];
            if (!isset($datesNew[$endPosition])) {
                $endPosition = $endPosition - 1;
            }
            $dateEnd = $datesNew[$endPosition];

            $year = explode('-', $dateEnd)[0];
            $numberWeek = Carbon::parse($dateStart)->weekOfYear;




            $plannings = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->where(function ($query) use ($dateStart, $dateEnd) {
                    $query->whereRAW("(
                        ('$dateStart 00:00:00' >= start_datetime and '$dateStart 00:00:00' <= end_datetime )
                        or
                        ('$dateEnd 23:59:59' >= start_datetime and '$dateEnd 23:59:59' <= end_datetime )
                        or
                        ('$dateStart 00:00:00' <= start_datetime and '$dateEnd 23:59:59' >= start_datetime )
                    )");
                })
                ->oldest('id')
                ->get();
            $labelFrom = "From $dateStart to $dateEnd";
            $labelWeek = "Year $year - Week: $numberWeek";
            array_push($array, [
                "$labelFrom" => [
                    "$labelWeek" => [],
                ]
            ]);
            $positionArray = count($array) - 1;
            $cunt = 0;
            $timeSheetLinesTmp = [];



            foreach ($plannings as $planning) {
                $startDatePlanning = Carbon::parse($planning->start_datetime)->subHours(5);
                $endDatePlanning = Carbon::parse($planning->end_datetime)->subHours(5);


                $initDateVerif = explode(' ', $labelFrom)[1];
                if (strtotime($initDateVerif) < strtotime($startDatePlanning->format('Y-m-d'))) {
                    $initDateVerif = $startDatePlanning->format('Y-m-d');
                }
                $initDateVerifCarbon = Carbon::parse($initDateVerif);

                $finalDateVerif = explode(' ', $labelFrom)[3];
                if (strtotime($finalDateVerif) > strtotime($endDatePlanning->format('Y-m-d'))) {
                    $finalDateVerif = $endDatePlanning->format('Y-m-d');
                }
                $finalDateVerifCarbon = Carbon::parse($finalDateVerif);


                /*$timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                    ->where('employee_id', $employee->id)
                    ->whereDate('date', $startDatePlanning->format('Y-m-d'))
                    ->get();*/

                $days2Difference2 = $initDateVerifCarbon->diffInDays($finalDateVerifCarbon) == 0 ? 1 : $initDateVerifCarbon->diffInDays($finalDateVerifCarbon) + 1;

                $timeSheetLines = TimeSheetsLine::where('task_id', '=', $taskId)
                    ->where('employee_id', '=', $employee->id)
                    ->whereBetween('date', [
                        $initDateVerif,
                        $finalDateVerif
                    ])
                    ->orderBy('date', "asc")
                    ->groupBy('date')->select("date")->get();


                if (count($timeSheetLines) == 0 || count($timeSheetLines) < $days2Difference2) {
                    for ($abc = 0; $abc < $days2Difference2; $abc++) {
                        $startDateFor = $initDateVerif;
                        $day = Carbon::parse($startDateFor)->addDays($abc)->format('Y-m-d');
                        $day_of_the_week = Carbon::parse($startDateFor)->addDays($abc)->dayOfWeek;

                        //dd($attendance_days, $day_of_the_week, $day);

                        $timeSheetLine = TimeSheetsLine::where('task_id', $taskId)
                            ->where('employee_id', $employee->id)
                            ->whereDate('date', $day)
                            ->first();

                        if (!$timeSheetLine &&  (!empty($planning->planning_mass_id) || in_array($day_of_the_week, $attendance_days))) {
                            /* $attributes = [
                                "description" => '',
                                "date" => $day,
                                "email_employee" => $employee->work_email,
                                "unit_amount" => 0,
                                "task_id" => $taskId,
                                "employee_id" => $employee->id,
                                "custom_state" => "draft",
                            ];
                            $timeSheetsLines = new TimeSheetsLine();
                            $data = $timeSheetsLines->setDataCreate($attributes, $task);
                            $timeSheetsLines->create($data);*/


                            $timeSheetLinesTmp[$cunt] = (object) [
                                "id" => "",
                                "name" => "",
                                "date" =>  $day,
                                "amount" => "0",
                                "unit_amount" => "0",
                                "product_uom_id" => "",
                                "account_id" => "",
                                "partner_id" => null,
                                "user_id" => "",
                                "company_id" => "",
                                "currency_id" => "",
                                "group_id" => null,
                                "create_uid" => "",
                                "create_date" => "",
                                "write_uid" => "",
                                "write_date" => "",
                                "task_id" => $taskId,
                                "project_id" => "",
                                "employee_id" => $employee->id,
                                "department_id" => "",
                                "validated" => "",
                                "custom_state" => "draft",
                                "custom_approved_by" => null,
                                "custom_rejected_by" => null,
                                "custom_approve_date" => null,
                                "custom_rejected_date" => null,
                                "custom_rejected_reason" => null,
                                "custom_is_rejected" => null,
                                "billeable_hours" => null,
                                "non_billeable_hours" => null,
                                "non_charge" => null,
                                "billable_option" => null,
                                "non_billable_option" => null,
                                "earncode" => null,
                                "paycom_status" => null,
                                "paycom_response" => null
                            ];
                            $cunt++;
                        }
                    }
                }
            }

            $initDate = explode(' ', $labelFrom)[1];
            $finalDate = explode(' ', $labelFrom)[3];

            $timeSheetLines = TimeSheetsLine::where('task_id', '=', $taskId)
                ->where('employee_id',  '=', $employee->id)
                ->whereBetween('date', [
                    $initDate,
                    $finalDate
                ])
                ->orderBy('date', "asc")->orderBy('id', "desc");

            if ($state) {
                if ($state != "notReported" && $state != 'today') {
                    $timeSheetLines = $this->filterState($timeSheetLines, $state);
                } else {
                    if ($state == 'today') {
                        $timeSheetLines = $timeSheetLines->where('employee_id', $employee->id)
                            ->where('unit_amount', 0)
                            ->where('date', Carbon::now('America/Bogota')->format('Y-m-d'))
                            ->where('custom_state', TimeSheetsLine::DRAFT)
                            ->orderBy('date', "asc")->orderBy('id', "desc");
                    } else {
                        $timeSheetLines = $timeSheetLines->where('employee_id', $employee->id)
                            ->where('unit_amount', 0)
                            ->where('date', '<', Carbon::now('America/Bogota')->format('Y-m-d'))
                            ->where('custom_state', TimeSheetsLine::DRAFT)
                            ->orderBy('date', "asc")->orderBy('id', "desc");
                    }
                }
            }

            $timeSheetLines = $timeSheetLines->get();


            $timeSheets = [];
            $sum = [];
            $days = [];
            $status = [];
            //  if (count($timeSheetLines) > 0) {

            if ($grouped == 1) {
                foreach ($timeSheetLines as $timeSheetLine) {
                    if ($timeSheetLine->custom_state == "waiting") {
                        $timeSheetLine->custom_state = "submitted";
                    }

                    $allocatedHours = Planning::where('task_id', $timeSheetLine->task_id)
                        ->where('is_published', true)
                        ->where("employee_id", $employee->id)
                        ->whereBetween('start_datetime', [
                            "$timeSheetLine->date 00:00:00",
                            "$timeSheetLine->date 23:59:59"
                        ])
                        ->sum('allocated_hours');

                    $allocatedHours_detail = Planning::where('task_id', $timeSheetLine->task_id)
                        ->where('is_published', true)
                        ->where("employee_id", $employee->id)
                        ->whereRaw( "DATE(start_datetime) <= '$timeSheetLine->date'" )
                        ->whereRaw("DATE(end_datetime) >=  '$timeSheetLine->date'")
                        /*->whereBetween($timeSheetLine->date, [
                            "start_datetime",
                            "end_datetime"
                        ])*/
                        ->select('allocated_hours', 'planning_mass_id', 'id')->get();

                    //dd($allocatedHours_detail,$timeSheetLine->date, $attendance_days );

                    $allocatedHours_real =  0;
                    $day_of_the_week = Carbon::parse($timeSheetLine->date)->dayOfWeek;
                    foreach ($allocatedHours_detail as $ahd) {
                        if (!empty($ahd->planning_mass_id) || in_array($day_of_the_week, $attendance_days)) {
                        $allocatedHours_real += $ahd->allocated_hours / $diference_dates[$ahd->id];
                        }
                    }

                    //dd($allocatedHours_real);

                    $timeSheetLine->name = ($timeSheetLine->name == '/') ? '' : $timeSheetLine->name;

                    if (!isset($sum["$timeSheetLine->date"])) {
                        $sum["$timeSheetLine->date"] = 0;
                    }

                    $sum["$timeSheetLine->date"] += $timeSheetLine->unit_amount;
                    $timeSheetLine->unit_amount = $sum["$timeSheetLine->date"];

                    if (!isset($status["$timeSheetLine->date"])) {
                        $status["$timeSheetLine->date"] = $timeSheetLine->custom_state;
                    } else {

                        if ($status["$timeSheetLine->date"] == "approved") {

                            foreach ($timeSheets as $k2 => $v2) {
                                foreach ($v2 as $k => $v) {
                                    if ($k == $timeSheetLine->date) {
                                        if ($timeSheetLine->custom_state != "approved") {

                                            $timeSheets[$k2][$k]["timesheet_hours"]->custom_state = $timeSheetLine->custom_state;
                                            $status["$timeSheetLine->date"] = $timeSheetLine->custom_state;
                                        }
                                    }
                                }
                            }
                        } else if ($status["$timeSheetLine->date"] == "draft") {
                            foreach ($timeSheets as $k2 => $v2) {
                                foreach ($v2 as $k => $v) {
                                    if ($k == $timeSheetLine->date) {
                                        if ($timeSheetLine->custom_state != "approved" && $timeSheetLine->custom_state != "draft") {

                                            $timeSheets[$k2][$k]["timesheet_hours"]->custom_state = $timeSheetLine->custom_state;
                                            $status["$timeSheetLine->date"] = $timeSheetLine->custom_state;
                                        }
                                    }
                                }
                            }
                        } else if ($status["$timeSheetLine->date"] == "waiting" || $status["$timeSheetLine->date"] == "submitted") {
                            foreach ($timeSheets as $k2 => $v2) {
                                foreach ($v2 as $k => $v) {
                                    if ($k == $timeSheetLine->date) {
                                        if ($timeSheetLine->custom_state != "approved" && $timeSheetLine->custom_state != "draft" && ($timeSheetLine->custom_state != "waiting" || $timeSheetLine->custom_state != "submitted")) {

                                            $timeSheets[$k2][$k]["timesheet_hours"]->custom_state = $timeSheetLine->custom_state;
                                            $status["$timeSheetLine->date"] = $timeSheetLine->custom_state;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (!isset($days["$timeSheetLine->date"])) {
                        $days["$timeSheetLine->date"] = 1;
                        $object = [
                            "$timeSheetLine->date" => [
                                "hours_planning" => (float) $allocatedHours_real, //$allocatedHours,
                                "case" => 1,
                                "timesheet_hours" => $timeSheetLine,
                            ]
                        ];
                        array_push($timeSheets, $object);
                    } else {
                        foreach ($timeSheets as $k2 => $v2) {
                            foreach ($v2 as $k => $v) {
                                if ($k == $timeSheetLine->date) {
                                    $timeSheets[$k2][$k]["timesheet_hours"]->unit_amount = $sum["$timeSheetLine->date"];
                                }
                            }
                        }
                    }
                }

                if ($state == null) {
                    //dd($timeSheetLinesTmp);
                    foreach ($timeSheetLinesTmp as $timeSheetLine) {
                        if ($timeSheetLine->custom_state == "waiting") {
                            $timeSheetLine->custom_state = "submitted";
                        }

                        $planning = Planning::where('task_id', $timeSheetLine->task_id)
                            ->where('is_published', true)
                            ->where("employee_id", $employee->id)
                            ->first();



                        $allocatedHours_detail = Planning::where('task_id', $timeSheetLine->task_id)
                        ->where('is_published', true)
                        ->where("employee_id", $employee->id)
                        ->whereRaw( "DATE(start_datetime) <= '$timeSheetLine->date'" )
                        ->whereRaw("DATE(end_datetime) >=  '$timeSheetLine->date'")
                        ->select('allocated_hours', 'planning_mass_id', 'id')->get();

                        //dd($allocatedHours_detail);

                        $allocatedHours_real =  0;
                        /*if($timeSheetLine->date == '2020-07-19'){

                            dd($allocatedHours_detail, $timeSheetLine->date, $attendance_days);
                        }*/
                        $day_of_the_week = Carbon::parse($timeSheetLine->date)->dayOfWeek;
                        foreach ($allocatedHours_detail as $ahd) {
                            if (!empty($ahd->planning_mass_id) || in_array($day_of_the_week, $attendance_days)) {
                            $allocatedHours_real += $ahd->allocated_hours / $diference_dates[$ahd->id];
                            }
                        }


                        $timeSheetLine->name = ($timeSheetLine->name == '/') ? '' : $timeSheetLine->name;
                        $object = [
                            "$timeSheetLine->date" => [
                                "hours_planning" => (float) $allocatedHours_real,  //$planning->allocated_hours/ $diference_dates[$planning->id],
                                "case" => 2,
                                "timesheet_hours" => $timeSheetLine,
                            ]
                        ];
                        array_push($timeSheets, $object);
                    }
                }
            } else {

                foreach ($timeSheetLines as $timeSheetLine) {
                    if ($timeSheetLine->custom_state == "waiting") {
                        $timeSheetLine->custom_state = "submitted";
                    }
                    switch ($timeSheetLine->earncode) {
                        case  "R":
                            $timeSheetLine->earncode_name = "Regular";
                            break;
                        case  "NBR":
                            $timeSheetLine->earncode_name = "Standby";
                            break;
                        case  "TRA":
                            $timeSheetLine->earncode_name = "Travel";
                            break;
                        case  "HOL":
                            $timeSheetLine->earncode_name = "Holiday";
                            break;
                        case  "FHL":
                            $timeSheetLine->earncode_name = "Floating holiday";
                            break;
                        case  "PTF":
                            $timeSheetLine->earncode_name = "PTO";
                            break;
                    }


                    $allocatedHours = Planning::where('task_id', $timeSheetLine->task_id)
                        ->where('is_published', true)
                        ->where("employee_id", $employee->id)
                        ->whereBetween('start_datetime', [
                            "$timeSheetLine->date 00:00:00",
                            "$timeSheetLine->date 23:59:59"
                        ])
                        ->sum('allocated_hours');


                    $allocatedHours_detail = Planning::where('task_id', $timeSheetLine->task_id)
                        ->where('is_published', true)
                        ->where("employee_id", $employee->id)
                        ->whereBetween('start_datetime', [
                            "$timeSheetLine->date 00:00:00",
                            "$timeSheetLine->date 23:59:59"
                        ])
                        ->select('allocated_hours', 'planning_mass_id', 'id');

                    $allocatedHours_real =  0;
                    $day_of_the_week = Carbon::parse($timeSheetLine->date)->dayOfWeek;
                    foreach ($allocatedHours_detail as $ahd) {
                        if (!empty($ahd->planning_mass_id) || in_array($day_of_the_week, $attendance_days)) {
                        $allocatedHours_real += $ahd->allocated_hours / $diference_dates[$ahd->id];
                        }
                    }



                    $timeSheetLine->name = ($timeSheetLine->name == '/') ? '' : $timeSheetLine->name;
                    $object = [
                        "$timeSheetLine->date" => [
                            "hours_planning" => (float) $allocatedHours_real,
                            "case" => 3,
                            "timesheet_hours" => $timeSheetLine,
                        ]
                    ];
                    array_push($timeSheets, $object);
                }
            }







            if (count($timeSheets) > 0) {

                uasort ( $timeSheets , array($this,'cmp'));
                $timeSheets =  array_values($timeSheets);


                $array[$positionArray][$labelFrom][$labelWeek] = [
                    "timeSheets" => ($timeSheets)

                ];
            } else {
                if (count($array[$positionArray][$labelFrom][$labelWeek]) == 0) {
                    unset($array[$positionArray][$labelFrom]);
                }
            }
            /*} else {
                if (count($array[$positionArray][$labelFrom][$labelWeek]) == 0) {
                    unset($array[$positionArray][$labelFrom]);
                }
            }*/
        }

        //dd($array);
        foreach ($array as $item) {
            array_filter($item);
        }

        if (count($array) > 0) {
            $allocatedHours = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->oldest('id')
                ->sum('allocated_hours');
            $startDates = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->oldest('id')
                ->pluck('start_datetime')
                ->toArray();
            $dates = TimeSheetsLine::where('task_id', $task->id)->where('employee_id', $employee->id)->oldest('date')->pluck('date')->toArray();
            $sumTs = TimeSheetsLine::where('task_id', $task->id)->where('employee_id', $employee->id)->sum('unit_amount');
            $task->effective_hours = $sumTs;
            $task->planned_hours = (int) $allocatedHours;
            $array[0]["task"] = $task;
            $array[0]["planning"] = count($plannings) > 0 ? [$plannings[0]] : null;
            //$array[0]["startDates"] = $startDates;
            //$array[0]["dates"] = $dates;
        }
        $finalArray = [];
        if (count($array) > 0) {
            foreach ($array as $item) {
                if (!empty($item) || count($item) > 0) {
                    array_push($finalArray, $item);
                }
            }
        }
        return $finalArray;
    }







    private function showSinglePlanningTryingToFixV4($plannings, $taskId, $employee, $state = null, $grouped = null)
    {
        ///resource_calendar_attendance
        $calendar =  Calendar::where("resource_calendar.id", '=',  $employee->resource_calendar_id)
            ->join("resource_calendar_attendance", 'resource_calendar.id', '=', 'resource_calendar_attendance.calendar_id')
            ->selectRAW("DISTINCT(resource_calendar_attendance.dayofweek) as dayofweek")->get();
        $attendance_days = [];
        foreach ($calendar as $cday) {
            if ($cday->dayofweek == 6) { // Domingo
                $attendance_days[0] = 0;
            } else {
                $attendance_days[$cday->dayofweek + 1] = $cday->dayofweek + 1;
            }
        }

        //dd($calendar);
        $array = [];
        $dates = [];
        $task = Task::findOrFail($taskId);
        $startDatePlanning = Carbon::parse($plannings[0]->start_datetime)->subHours(5);
        $endDatePlanning = Carbon::parse($plannings[0]->end_datetime)->subHours(5);

        $daysDifference = $startDatePlanning->diffInDays($endDatePlanning) == 0 ? 1 : $startDatePlanning->diffInDays($endDatePlanning) + 1;

        $diferenceRealDays = 0;

        for ($abc = 0; $abc < $daysDifference; $abc++) {
            $day = Carbon::parse($plannings[0]->start_datetime)->addDays($abc)->format('Y-m-d');
            $day_of_the_week = Carbon::parse($plannings[0]->start_datetime)->addDays($abc)->dayOfWeek;

            if (!empty($plannings[0]->planning_mass_id) || in_array($day_of_the_week, $attendance_days)) {
                $diferenceRealDays++;
            }
        }

        foreach ($plannings as $planning) {
            $startDatePlanning = Carbon::parse($planning->start_datetime)->subHours(5);
            $endDatePlanning = Carbon::parse($planning->end_datetime)->subHours(5);
            $start = $startDatePlanning->format('Y-m-d');
            $end = $endDatePlanning->format('Y-m-d');
            //dd($daysDifference);


            for ($i = 0; $i < $daysDifference; $i++) {
                $dates = array_merge($dates, [
                    Carbon::parse($start)->addDays($i)->startOfWeek()->format('Y-m-d'),
                    Carbon::parse($end)->addDays($i)->startOfWeek()->format('Y-m-d'),
                ]);
            }
        }

        $datesNew = [];
        sort($dates);
        foreach (array_unique($dates) as $date) {

            array_push($datesNew, $date);
            $newWeek = Carbon::parse($date)->endOfWeek()->format('Y-m-d');
            array_push($datesNew, $newWeek);
        }
        //if (!count($datesNew) % 2 == 0) {

        //}
        //dd($datesNew);
        $maxCount = count($datesNew);
        for ($i = 0; $i < $maxCount; $i = $i + 2) {
            $initPosition = $i;
            $endPosition = $i + 1;
            /*
                if ($i > 0) {
                    if ($i == 1) {
                        $initPosition = $i + 1;
                        $endPosition = $initPosition + 1;
                    } else {
                        if ($i % 2 === 0) {
                            $initPosition = $i + 2;
                            $endPosition = $initPosition + 1;
                        } else {
                            $initPosition = $i + 3;
                            $endPosition = $initPosition + 1;
                        }
                    }
                }
            */
            $dateStart = $datesNew[$initPosition];
            if (!isset($datesNew[$endPosition])) {
                $endPosition = $endPosition - 1;
            }
            $dateEnd = $datesNew[$endPosition];

            $year = explode('-', $dateEnd)[0];
            $numberWeek = Carbon::parse($dateStart)->weekOfYear;

            $plannings = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->where(function ($query) use ($dateStart, $dateEnd) {
                    $query->whereRAW("(
                        ('$dateStart 00:00:00' >= start_datetime and '$dateStart 00:00:00' <= end_datetime )
                        or
                        ('$dateEnd 23:59:59' >= start_datetime and '$dateEnd 23:59:59' <= end_datetime )
                        or
                        ('$dateStart 00:00:00' <= start_datetime and '$dateEnd 23:59:59' >= start_datetime )
                    )");
                    //$query->where('start_datetime', '>=', "$dateStart 00:00:00");
                    //$query->here('end_datetime', '<=', "$dateEnd 23:59:59");
                    //or  end_datetime  >= '2020-08-22 23:59:59'
                })
                ->oldest('id')
                ->get();
            //dd($plannings);

            $labelFrom = "From $dateStart to $dateEnd";
            $labelWeek = "Year $year - Week: $numberWeek";
            array_push($array, [
                "$labelFrom" => [
                    "$labelWeek" => [],
                ]
            ]);
            $positionArray = count($array) - 1;
            $cunt = 0;
            $timeSheetLinesTmp = [];
            foreach ($plannings as $planning) {

                $startDatePlanning = Carbon::parse($planning->start_datetime)->subHours(5);
                $endDatePlanning = Carbon::parse($planning->end_datetime)->subHours(5);

                $initDateVerif = explode(' ', $labelFrom)[1];
                if (strtotime($initDateVerif) < strtotime($startDatePlanning->format('Y-m-d'))) {
                    $initDateVerif = $startDatePlanning->format('Y-m-d');
                }
                $initDateVerifCarbon = Carbon::parse($initDateVerif);

                $finalDateVerif = explode(' ', $labelFrom)[3];
                if (strtotime($finalDateVerif) > strtotime($endDatePlanning->format('Y-m-d'))) {
                    $finalDateVerif = $endDatePlanning->format('Y-m-d');
                }
                $finalDateVerifCarbon = Carbon::parse($finalDateVerif);

                /*$timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                    ->where('employee_id', $employee->id)
                    ->whereDate('date', $startDatePlanning->format('Y-m-d'))
                    ->get();*/

                $days2Difference2 = $initDateVerifCarbon->diffInDays($finalDateVerifCarbon) == 0 ? 1 : $initDateVerifCarbon->diffInDays($finalDateVerifCarbon) + 1;

                $timeSheetLines = TimeSheetsLine::where('task_id', '=', $taskId)
                    ->where('employee_id', '=', $employee->id)
                    ->whereBetween('date', [
                        $initDateVerif,
                        $finalDateVerif
                    ])
                    ->orderBy('date', "asc")
                    ->groupBy('date')->select("date")->get();

                //dd($initDateVerifCarbon, $finalDateVerifCarbon,  count($timeSheetLines), count($timeSheetLines), $days2Difference2);

                if (count($timeSheetLines) == 0 || count($timeSheetLines) < $days2Difference2) {
                    for ($abc = 0; $abc < $days2Difference2; $abc++) {
                        $startDateFor = $initDateVerif;
                        $day = Carbon::parse($startDateFor)->addDays($abc)->format('Y-m-d');
                        $day_of_the_week = Carbon::parse($startDateFor)->addDays($abc)->dayOfWeek;

                        //dd($attendance_days, $day_of_the_week, $day);

                        $timeSheetLine = TimeSheetsLine::where('task_id',  '=', $taskId)
                            ->where('employee_id',  '=', $employee->id)
                            ->whereDate('date', $day)
                            ->first();

                        if (!$timeSheetLine &&  (!empty($planning->planning_mass_id) || in_array($day_of_the_week, $attendance_days))) {
                            /* $attributes = [
                                "description" => '',
                                "date" => $day,
                                "email_employee" => $employee->work_email,
                                "unit_amount" => 0,
                                "task_id" => $taskId,
                                "employee_id" => $employee->id,
                                "custom_state" => "draft",
                            ];
                            $timeSheetsLines = new TimeSheetsLine();
                            $data = $timeSheetsLines->setDataCreate($attributes, $task);
                            $timeSheetsLines->create($data);*/


                            $timeSheetLinesTmp[$cunt] = (object) [
                                "id" => "",
                                "name" => "",
                                "date" =>  $day,
                                "amount" => "0",
                                "unit_amount" => "0",
                                "product_uom_id" => "",
                                "account_id" => "",
                                "partner_id" => null,
                                "user_id" => "",
                                "company_id" => "",
                                "currency_id" => "",
                                "group_id" => null,
                                "create_uid" => "",
                                "create_date" => "",
                                "write_uid" => "",
                                "write_date" => "",
                                "task_id" => $taskId,
                                "project_id" => "",
                                "employee_id" => $employee->id,
                                "department_id" => "",
                                "validated" => "",
                                "custom_state" => "draft",
                                "custom_approved_by" => null,
                                "custom_rejected_by" => null,
                                "custom_approve_date" => null,
                                "custom_rejected_date" => null,
                                "custom_rejected_reason" => null,
                                "custom_is_rejected" => null,
                                "billeable_hours" => null,
                                "non_billeable_hours" => null,
                                "non_charge" => null,
                                "billable_option" => null,
                                "non_billable_option" => null,
                                "earncode" => null,
                                "paycom_status" => null,
                                "paycom_response" => null
                            ];
                            $cunt++;
                        }
                    }
                }
            }
            $initDate = explode(' ', $labelFrom)[1];
            $finalDate = explode(' ', $labelFrom)[3];

            $timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                ->where('employee_id', $employee->id)
                ->whereBetween('date', [
                    $initDate,
                    $finalDate
                ])
                ->orderBy('date', "asc")->orderBy('id', "desc");
            if ($state) {
                if ($state != "notReported" && $state != 'today') {
                    $timeSheetLines = $this->filterState($timeSheetLines, $state);
                } else {
                    if ($state == 'today') {
                        $timeSheetLines = $timeSheetLines->where('employee_id', $employee->id)
                            ->where('unit_amount', 0)
                            ->where('date', Carbon::now('America/Bogota')->format('Y-m-d'))
                            ->where('custom_state', TimeSheetsLine::DRAFT)
                            ->orderBy('date', "asc")->orderBy('id', "desc");
                    } else {
                        $timeSheetLines = $timeSheetLines->where('employee_id', $employee->id)
                            ->where('unit_amount', 0)
                            ->where('date', '<', Carbon::now('America/Bogota')->format('Y-m-d'))
                            ->where('custom_state', TimeSheetsLine::DRAFT)
                            ->orderBy('date', "asc")->orderBy('id', "desc");
                    }
                }
            }
            $timeSheetLines = $timeSheetLines->get();

            $timeSheets = [];
            //dd(count($timeSheetLines), ($timeSheetLines));
            //if (count($timeSheetLines) > 0) {

            $sum = [];
            $days = [];
            $status = [];

            if ($grouped == 1) {
                foreach ($timeSheetLines as $timeSheetLine) {
                    if ($timeSheetLine->custom_state == "waiting") {
                        $timeSheetLine->custom_state = "submitted";
                    }
                    $planning = Planning::where('task_id', $timeSheetLine->task_id)
                        ->where('is_published', true)
                        ->where("employee_id", $employee->id)
                        ->first();
                    $timeSheetLine->name = ($timeSheetLine->name == '/') ? '' : $timeSheetLine->name;

                    if (!isset($sum["$timeSheetLine->date"])) {
                        $sum["$timeSheetLine->date"] = 0;
                    }

                    $sum["$timeSheetLine->date"] += $timeSheetLine->unit_amount;
                    $timeSheetLine->unit_amount = $sum["$timeSheetLine->date"];

                    if (!isset($status["$timeSheetLine->date"])) {
                        $status["$timeSheetLine->date"] = $timeSheetLine->custom_state;
                    } else {

                        if ($status["$timeSheetLine->date"] == "approved") {

                            foreach ($timeSheets as $k2 => $v2) {
                                foreach ($v2 as $k => $v) {
                                    if ($k == $timeSheetLine->date) {
                                        if ($timeSheetLine->custom_state != "approved") {

                                            $timeSheets[$k2][$k]["timesheet_hours"]->custom_state = $timeSheetLine->custom_state;
                                            $status["$timeSheetLine->date"] = $timeSheetLine->custom_state;
                                        }
                                    }
                                }
                            }
                        } else if ($status["$timeSheetLine->date"] == "draft") {
                            foreach ($timeSheets as $k2 => $v2) {
                                foreach ($v2 as $k => $v) {
                                    if ($k == $timeSheetLine->date) {
                                        if ($timeSheetLine->custom_state != "approved" && $timeSheetLine->custom_state != "draft") {

                                            $timeSheets[$k2][$k]["timesheet_hours"]->custom_state = $timeSheetLine->custom_state;
                                            $status["$timeSheetLine->date"] = $timeSheetLine->custom_state;
                                        }
                                    }
                                }
                            }
                        } else if ($status["$timeSheetLine->date"] == "waiting" || $status["$timeSheetLine->date"] == "submitted") {
                            foreach ($timeSheets as $k2 => $v2) {
                                foreach ($v2 as $k => $v) {
                                    if ($k == $timeSheetLine->date) {
                                        if ($timeSheetLine->custom_state != "approved" && $timeSheetLine->custom_state != "draft" && ($timeSheetLine->custom_state != "waiting" || $timeSheetLine->custom_state != "submitted")) {

                                            $timeSheets[$k2][$k]["timesheet_hours"]->custom_state = $timeSheetLine->custom_state;
                                            $status["$timeSheetLine->date"] = $timeSheetLine->custom_state;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (!isset($days["$timeSheetLine->date"])) {
                        $days["$timeSheetLine->date"] = 1;
                        $object = [
                            "$timeSheetLine->date" => [
                                "hours_planning" => (float) $planning->allocated_hours / $diferenceRealDays,  //$daysDifference,
                                "timesheet_hours" => $timeSheetLine,
                            ]
                        ];
                        array_push($timeSheets, $object);
                    } else {
                        foreach ($timeSheets as $k2 => $v2) {
                            foreach ($v2 as $k => $v) {
                                if ($k == $timeSheetLine->date) {
                                    $timeSheets[$k2][$k]["timesheet_hours"]->unit_amount = $sum["$timeSheetLine->date"];
                                }
                            }
                        }
                    }
                }
                if ($state == null) {
                    foreach ($timeSheetLinesTmp as $timeSheetLine) {
                        if ($timeSheetLine->custom_state == "waiting") {
                            $timeSheetLine->custom_state = "submitted";
                        }
                        $planning = Planning::where('task_id', $timeSheetLine->task_id)
                            ->where('is_published', true)
                            ->where("employee_id", $employee->id)
                            ->first();
                        $timeSheetLine->name = ($timeSheetLine->name == '/') ? '' : $timeSheetLine->name;
                        $object = [
                            "$timeSheetLine->date" => [
                                "hours_planning" => (float) $planning->allocated_hours / $diferenceRealDays,  //$daysDifference,
                                "timesheet_hours" => $timeSheetLine,
                            ]
                        ];
                        array_push($timeSheets, $object);
                    }
                }
            } else {

                foreach ($timeSheetLines as $timeSheetLine) {
                    if ($timeSheetLine->custom_state == "waiting") {
                        $timeSheetLine->custom_state = "submitted";
                    }

                    switch ($timeSheetLine->earncode) {
                        case  "R":
                            $timeSheetLine->earncode_name = "Regular";
                            break;
                        case  "NBR":
                            $timeSheetLine->earncode_name = "Standby";
                            break;
                        case  "TRA":
                            $timeSheetLine->earncode_name = "Travel";
                            break;
                        case  "HOL":
                            $timeSheetLine->earncode_name = "Holiday";
                            break;
                        case  "FHL":
                            $timeSheetLine->earncode_name = "Floating holiday";
                            break;
                        case  "PTF":
                            $timeSheetLine->earncode_name = "PTO";
                            break;
                    }

                    $planning = Planning::where('task_id', $timeSheetLine->task_id)
                        ->where('is_published', true)
                        ->where("employee_id", $employee->id)
                        ->first();
                    $timeSheetLine->name = ($timeSheetLine->name == '/') ? '' : $timeSheetLine->name;
                    $object = [
                        "$timeSheetLine->date" => [
                            "hours_planning" => (float) $planning->allocated_hours / $diferenceRealDays, // $daysDifference,
                            "timesheet_hours" => $timeSheetLine,
                        ]
                    ];
                    array_push($timeSheets, $object);
                }
            }


            if (count($timeSheets) > 0) {


              //  uasort ( $timeSheets , array($this,'cmp'));

              uasort ( $timeSheets , array($this,'cmp'));
              $timeSheets =  array_values($timeSheets);

                $array[$positionArray][$labelFrom][$labelWeek] = [
                    "timeSheets" => $timeSheets,
                ];
            } else {
                if (count($array[$positionArray][$labelFrom][$labelWeek]) == 0) {
                    unset($array[$positionArray][$labelFrom]);
                }
            }
        }

        ///end -------------------------------
        //dd($array);
        foreach ($array as $item) {
            array_filter($item);
        }
        if (count($array) > 0) {
            $allocatedHours = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->oldest('id')
                ->sum('allocated_hours');
            $sumTs = TimeSheetsLine::where('task_id', $task->id)->where('employee_id', $employee->id)->sum('unit_amount');
            $task->effective_hours = $sumTs;
            $task->planned_hours = (int) $allocatedHours;
            $array[0]["task"] = $task;
            $array[0]["planning"] = count($plannings) > 0 ? [$plannings[0]] : null;
        }
        $finalArray = [];
        if (count($array) > 0) {
            foreach ($array as $item) {
                if (!empty($item) || count($item) > 0) {
                    array_push($finalArray, $item);
                }
            }
        }
        return $finalArray;
    }



    private function showSinglePlanningTryingToFix($plannings, $taskId, $employee, $state = null, $grouped = null)
    {
        $array = [];
        $dates = [];
        $task = Task::findOrFail($taskId);
        $startDatePlanning = Carbon::parse($plannings[0]->start_datetime)->subHours(5);
        $endDatePlanning = Carbon::parse($plannings[0]->end_datetime)->subHours(5);
        $daysDifference = $startDatePlanning->diffInDays($endDatePlanning) == 0 ? 1 : $startDatePlanning->diffInDays($endDatePlanning) + 1;
        foreach ($plannings as $planning) {
            $startDatePlanning = Carbon::parse($planning->start_datetime)->subHours(5);
            $endDatePlanning = Carbon::parse($planning->end_datetime)->subHours(5);
            $start = $startDatePlanning->format('Y-m-d');
            $end = $endDatePlanning->format('Y-m-d');
            //dd($daysDifference);
            for ($i = 0; $i < $daysDifference; $i++) {
                $dates = array_merge($dates, [
                    Carbon::parse($start)->addDays($i)->startOfWeek()->format('Y-m-d'),
                    Carbon::parse($end)->addDays($i)->startOfWeek()->format('Y-m-d'),
                ]);
            }
        }
        $datesNew = [];
        sort($dates);
        foreach (array_unique($dates) as $date) {

            array_push($datesNew, $date);
            $newWeek = Carbon::parse($date)->endOfWeek()->format('Y-m-d');
            array_push($datesNew, $newWeek);
        }
        //if (!count($datesNew) % 2 == 0) {

        //}
        //dd($datesNew);
        $maxCount = count($datesNew) / 2;
        for ($i = 0; $i < $maxCount; $i++) {
            $initPosition = $i;
            $endPosition = $i + 1;
            if ($i > 0) {
                if ($i == 1) {
                    $initPosition = $i + 1;
                    $endPosition = $initPosition + 1;
                } else {
                    if ($i % 2 === 0) {
                        $initPosition = $i + 2;
                        $endPosition = $initPosition + 1;
                    } else {
                        $initPosition = $i + 3;
                        $endPosition = $initPosition + 1;
                    }
                }
            }
            $dateStart = $datesNew[$initPosition];
            if (!isset($datesNew[$endPosition])) {
                $endPosition = $endPosition - 1;
            }
            $dateEnd = $datesNew[$endPosition];

            $year = explode('-', $dateEnd)[0];
            $numberWeek = Carbon::parse($dateStart)->weekOfYear;

            $plannings = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                /*->where(function ($query) use ($dateStart, $dateEnd) {
                    $query->where('start_datetime', '>=', "$dateStart 00:00:00");
                    $query->where('end_datetime', '<=', "$dateEnd 23:59:59");
                })*/
                ->oldest('id')
                ->get();

            $labelFrom = "From $dateStart to $dateEnd";
            $labelWeek = "Year $year - Week: $numberWeek";
            array_push($array, [
                "$labelFrom" => [
                    "$labelWeek" => [],
                ]
            ]);
            $positionArray = count($array) - 1;
            foreach ($plannings as $planning) {
                $startDatePlanning = Carbon::parse($planning->start_datetime)->subHours(5);
                $timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                    ->where('employee_id', $employee->id)
                    ->whereDate('date', $startDatePlanning->format('Y-m-d'))
                    ->get();
                if (count($timeSheetLines) == 0 || count($timeSheetLines) < $daysDifference) {
                    for ($abc = 0; $abc < $daysDifference; $abc++) {
                        $startDateFor = $startDatePlanning->format('Y-m-d');
                        $day = Carbon::parse($startDateFor)->addDays($abc)->format('Y-m-d');
                        $timeSheetLine = TimeSheetsLine::where('task_id', $taskId)
                            ->where('employee_id', $employee->id)
                            ->whereDate('date', $day)
                            ->first();
                        if (!$timeSheetLine) {
                            /* $attributes = [
                                "description" => '',
                                "date" => $day,
                                "email_employee" => $employee->work_email,
                                "unit_amount" => 0,
                                "task_id" => $taskId,
                                "employee_id" => $employee->id,
                                "custom_state" => "draft",
                            ];
                            $timeSheetsLines = new TimeSheetsLine();
                            $data = $timeSheetsLines->setDataCreate($attributes, $task);
                            $timeSheetsLines->create($data);*/
                        }
                    }
                }
            }
            $initDate = explode(' ', $labelFrom)[1];
            $finalDate = explode(' ', $labelFrom)[3];
            $timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                ->where('employee_id', $employee->id)
                ->whereBetween('date', [
                    $initDate,
                    $finalDate
                ])
                ->oldest('date');
            if ($state) {
                if ($state != "notReported" && $state != 'today') {
                    $timeSheetLines = $this->filterState($timeSheetLines, $state);
                } else {
                    if ($state == 'today') {
                        $timeSheetLines = $timeSheetLines->where('employee_id', $employee->id)
                            ->where('unit_amount', 0)
                            ->where('date', Carbon::now('America/Bogota')->format('Y-m-d'))
                            ->where('custom_state', TimeSheetsLine::DRAFT)
                            ->oldest('date');
                    } else {
                        $timeSheetLines = $timeSheetLines->where('employee_id', $employee->id)
                            ->where('unit_amount', 0)
                            ->where('date', '<', Carbon::now('America/Bogota')->format('Y-m-d'))
                            ->where('custom_state', TimeSheetsLine::DRAFT)
                            ->oldest('date');
                    }
                }
            }
            $timeSheetLines = $timeSheetLines->get();
            $timeSheets = [];
            if (count($timeSheetLines) > 0) {
                foreach ($timeSheetLines as $timeSheetLine) {
                    if ($timeSheetLine->custom_state == "waiting") {
                        $timeSheetLine->custom_state = "submitted";
                    }
                    $planning = Planning::where('task_id', $timeSheetLine->task_id)
                        ->where('is_published', true)
                        ->where("employee_id", $employee->id)
                        ->first();
                    $timeSheetLine->name = ($timeSheetLine->name == '/') ? '' : $timeSheetLine->name;
                    $object = [
                        "$timeSheetLine->date" => [
                            "hours_planning" => (float) $planning->allocated_hours / $daysDifference,
                            "timesheet_hours" => $timeSheetLine,
                        ]
                    ];
                    array_push($timeSheets, $object);
                }
                if (count($timeSheets) > 0) {
                    $array[$positionArray][$labelFrom][$labelWeek] = [
                        "timeSheets" => $timeSheets,
                    ];
                }
            } else {
                if (count($array[$positionArray][$labelFrom][$labelWeek]) == 0) {
                    unset($array[$positionArray][$labelFrom]);
                }
            }
        }

        ///end -------------------------------
        //dd($array);
        foreach ($array as $item) {
            array_filter($item);
        }
        if (count($array) > 0) {
            $allocatedHours = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->oldest('id')
                ->sum('allocated_hours');
            $sumTs = TimeSheetsLine::where('task_id', $task->id)->where('employee_id', $employee->id)->sum('unit_amount');
            $task->effective_hours = $sumTs;
            $task->planned_hours = (int) $allocatedHours;
            $array[0]["task"] = $task;
            $array[0]["planning"] = count($plannings) > 0 ? [$plannings[0]] : null;
        }
        $finalArray = [];
        if (count($array) > 0) {
            foreach ($array as $item) {
                if (!empty($item) || count($item) > 0) {
                    array_push($finalArray, $item);
                }
            }
        }
        return $finalArray;
    }




    private function showSinglePlanning($plannings, $taskId, $employee, $state = null)
    {
        $array = [];
        $dates = [];
        $task = Task::findOrFail($taskId);
        $startDatePlanning = Carbon::parse($plannings[0]->start_datetime)->subHours(5);
        $endDatePlanning = Carbon::parse($plannings[0]->end_datetime)->subHours(5);
        $daysDifference = $startDatePlanning->diffInDays($endDatePlanning) == 0 ? 1 : $startDatePlanning->diffInDays($endDatePlanning) + 1;
        foreach ($plannings as $planning) {
            $startDatePlanning = Carbon::parse($planning->start_datetime)->subHours(5);
            $endDatePlanning = Carbon::parse($planning->end_datetime)->subHours(5);
            $start = $startDatePlanning->format('Y-m-d');
            $end = $endDatePlanning->format('Y-m-d');
            for ($i = 0; $i < $daysDifference; $i++) {
                $dates = array_merge($dates, [
                    Carbon::parse($start)->addDays($i)->startOfWeek()->format('Y-m-d'),
                    Carbon::parse($end)->addDays($i)->startOfWeek()->format('Y-m-d'),
                ]);
            }
        }
        $datesNew = [];
        sort($dates);
        foreach (array_unique($dates) as $date) {
            array_push($datesNew, $date);
        }
        if (!count($datesNew) % 2 == 0) {
            $newWeek = Carbon::parse(end($datesNew))->endOfWeek()->format('Y-m-d');
            array_push($datesNew, $newWeek);
        }
        $maxCount = count($datesNew) / 2;
        for ($i = 0; $i < $maxCount; $i++) {
            $initPosition = $i;
            $endPosition = $i + 1;
            if ($i > 0) {
                if ($i == 1) {
                    $initPosition = $i + 1;
                    $endPosition = $initPosition + 1;
                } else {
                    if ($i % 2 === 0) {
                        $initPosition = $i + 2;
                        $endPosition = $initPosition + 1;
                    } else {
                        $initPosition = $i + 3;
                        $endPosition = $initPosition + 1;
                    }
                }
            }
            $dateStart = $datesNew[$initPosition];
            $dateEnd = $datesNew[$endPosition];
            $year = explode('-', $dateEnd)[0];
            $numberWeek = Carbon::parse($dateStart)->weekOfYear;
            $plannings = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->where(function ($query) use ($dateStart, $dateEnd) {
                    $query->where('start_datetime', '>=', "$dateStart 00:00:00");
                    $query->where('end_datetime', '<=', "$dateEnd 23:59:59");
                })
                ->oldest('id')
                ->get();
            $labelFrom = "From $dateStart to $dateEnd";
            $labelWeek = "Year $year - Week: $numberWeek";
            array_push($array, [
                "$labelFrom" => [
                    "$labelWeek" => [],
                ]
            ]);
            $positionArray = count($array) - 1;
            foreach ($plannings as $planning) {
                $startDatePlanning = Carbon::parse($planning->start_datetime)->subHours(5);
                $timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                    ->where('employee_id', $employee->id)
                    ->whereDate('date', $startDatePlanning->format('Y-m-d'))
                    ->get();
                if (count($timeSheetLines) == 0 || count($timeSheetLines) < $daysDifference) {

                    for ($abc = 0; $abc < $daysDifference; $abc++) {
                        $startDateFor = $startDatePlanning->format('Y-m-d');
                        $day = Carbon::parse($startDateFor)->addDays($abc)->format('Y-m-d');
                        $timeSheetLine = TimeSheetsLine::where('task_id', $taskId)
                            ->where('employee_id', $employee->id)
                            ->whereDate('date', $day)
                            ->first();
                        if (!$timeSheetLine) {
                            /*$attributes = [
                                "description" => '',
                                "date" => $day,
                                "email_employee" => $employee->work_email,
                                "unit_amount" => 0,
                                "task_id" => $taskId,
                                "employee_id" => $employee->id,
                                "custom_state" => "draft",
                            ];
                            $timeSheetsLines = new TimeSheetsLine();
                            $data = $timeSheetsLines->setDataCreate($attributes, $task);
                            $timeSheetsLines->create($data);*/
                        }
                    }
                }
            }
            $initDate = explode(' ', $labelFrom)[1];
            $finalDate = explode(' ', $labelFrom)[3];
            $timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                ->where('employee_id', $employee->id)
                ->whereBetween('date', [
                    $initDate,
                    $finalDate
                ])
                ->oldest('date');
            if ($state) {
                if ($state != "notReported" && $state != 'today') {
                    $timeSheetLines = $this->filterState($timeSheetLines, $state);
                } else {
                    if ($state == 'today') {
                        $timeSheetLines = $timeSheetLines->where('employee_id', $employee->id)
                            ->where('unit_amount', 0)
                            ->where('date', Carbon::now('America/Bogota')->format('Y-m-d'))
                            ->where('custom_state', TimeSheetsLine::DRAFT)
                            ->oldest('date');
                    } else {
                        $timeSheetLines = $timeSheetLines->where('employee_id', $employee->id)
                            ->where('unit_amount', 0)
                            ->where('date', '<', Carbon::now('America/Bogota')->format('Y-m-d'))
                            ->where('custom_state', TimeSheetsLine::DRAFT)
                            ->oldest('date');
                    }
                }
            }
            $timeSheetLines = $timeSheetLines->get();
            $timeSheets = [];
            if (count($timeSheetLines) > 0) {
                foreach ($timeSheetLines as $timeSheetLine) {
                    if ($timeSheetLine->custom_state == "waiting") {
                        $timeSheetLine->custom_state = "submitted";
                    }
                    $planning = Planning::where('task_id', $timeSheetLine->task_id)
                        ->where('is_published', true)
                        ->where("employee_id", $employee->id)
                        ->first();
                    $timeSheetLine->name = ($timeSheetLine->name == '/') ? '' : $timeSheetLine->name;
                    $object = [
                        "$timeSheetLine->date" => [
                            "hours_planning" => (float) $planning->allocated_hours / $daysDifference,
                            "timesheet_hours" => $timeSheetLine,
                        ]
                    ];
                    array_push($timeSheets, $object);
                }
                if (count($timeSheets) > 0) {
                    $array[$positionArray][$labelFrom][$labelWeek] = [
                        "timeSheets" => $timeSheets,
                    ];
                }
            } else {
                if (count($array[$positionArray][$labelFrom][$labelWeek]) == 0) {
                    unset($array[$positionArray][$labelFrom]);
                }
            }
        }





        //-- end


        foreach ($array as $item) {
            array_filter($item);
        }
        if (count($array) > 0) {
            $allocatedHours = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->oldest('id')
                ->sum('allocated_hours');
            $sumTs = TimeSheetsLine::where('task_id', $task->id)->where('employee_id', $employee->id)->sum('unit_amount');
            $task->effective_hours = $sumTs;
            $task->planned_hours = (int) $allocatedHours;
            $array[0]["task"] = $task;
            $array[0]["planning"] = count($plannings) > 0 ? [$plannings[0]] : null;
        }
        $finalArray = [];
        if (count($array) > 0) {
            foreach ($array as $item) {
                if (!empty($item) || count($item) > 0) {
                    array_push($finalArray, $item);
                }
            }
        }
        return $finalArray;
    }

    /**
     * @param $timeSheetLines
     * @param null $state
     * @return mixed
     */
    public function filterState($timeSheetLines, $state = null)
    {
        if ($state == TimeSheetsLine::DRAFT) {
            return $timeSheetLines->where('custom_state', TimeSheetsLine::DRAFT);
        }
        if ($state == TimeSheetsLine::REJECT) {
            return $timeSheetLines->where('custom_state', TimeSheetsLine::REJECT);
        }
        if ($state == TimeSheetsLine::APPROVED) {
            return $timeSheetLines->where('custom_state', TimeSheetsLine::APPROVED);
        }
        if ($state == TimeSheetsLine::WAITING || $state == "submitted") {
            return $timeSheetLines->where('custom_state', TimeSheetsLine::WAITING);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'description' => 'required|string',
            'date' => 'required|string',
            'unit_amount' => 'required',
            'earncode' => 'required',
            'task_id' => 'required',
            'custom_state' => 'required'
        ];
        $this->validate($request, $rules);
        $attributes = $request->all();

        $validate = $this->verify24Hours($attributes["task_id"], $attributes["unit_amount"], $attributes["date"]);
        if (!$validate) {
            return $this->ErrorResponse('You cannot register more than 24 hours in a day', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $employee = Employee::where('work_email', '=', auth()->user()->email)
            ->where('active', true)
            ->first();
        $attributes["email_employee"] =  $employee->work_email;
        $attributes["employee_id"] = $employee->id;


        $task = Task::findOrFail($request->task_id);
        $this->timeSheetsLine = new TimeSheetsLine();
        $data = $this->timeSheetsLine->setDataCreate($attributes, $task);
        $timeSheetsLine = $this->timeSheetsLine->create($data);
        //$task->setDataUpdate($timeSheetsLine);
        $task->save();

        return $this->SuccessResponse($timeSheetsLine, Response::HTTP_CREATED);
    }

    public function verify24Hours($task_id, $newAmount, $date, $timesheet_id = null)
    {
        $sum = TimeSheetsLine::where("task_id", "=", $task_id)->where("date", "=", $date)->sum('unit_amount');
        if ($timesheet_id != null) {
            $current = TimeSheetsLine::where("id", "=", $timesheet_id)->sum('unit_amount');
            if ($current == $newAmount) {
                return true;
            }
            $sum = $sum - $current;
        }
        $sum = $sum + $newAmount;
        if ($sum >= 25) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function meTimeSheetsLines(Request $request)
    {
        $rules = [
            "email_employee" => 'required|email|exists:hr_employee,work_email',
        ];
        $this->validate($request, $rules);
        $attributes = $request->all();
        $employeeId = Employee::where("work_email", $attributes["email_employee"])->value("id");
        $timeSheetsLines = TimeSheetsLine::where("employee_id", $employeeId)->latest("id")->get();
        return $this->SuccessResponse($timeSheetsLines, Response::HTTP_OK);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'description' => 'required|string',
            'date' => 'required|string',
            'unit_amount' => 'required',
            'earncode' => 'required',
            'task_id' => 'required',
            'custom_state' => 'required'
        ];
        $this->validate($request, $rules);
        $timeSheetsLine = TimeSheetsLine::findOrFail($id);


        $validate = $this->verify24Hours($request->task_id, $request->unit_amount, $request->date,  $id);
        if (!$validate) {
            return $this->ErrorResponse('You cannot register more than 24 hours in a day', Response::HTTP_UNPROCESSABLE_ENTITY);
        }


        $timeSheetsLine->setDataUpdate($request->all());
        if ($timeSheetsLine->isClean()) {
            return $this->ErrorResponse('At least one value must change', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $timeSheetsLine->save();
        $task = Task::find($timeSheetsLine->task_id);
        $task->setDataUpdate($timeSheetsLine);
        $task->save();
        $im = auth()->user();
        DB::select('CALL prd_register_score(?,?,?,?)', array((int) $im->id, 7, 0, '')); //7
        return $this->SuccessResponse($timeSheetsLine);
    }

    public function destroy($id)
    {
        $timeSheetsLine = TimeSheetsLine::findOrFail($id);
        if (!$timeSheetsLine->validated) {
            $timeSheetsLine->delete();
            return $this->SuccessResponse($timeSheetsLine, Response::HTTP_OK);
        } else {
            return $this->ErrorResponse("The line is already validated, it cannot be deleted", Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * @param Collection $lines
     * @param Employee $employee
     * @return array
     */
    private function showAllFilter(Collection $lines, $employee)
    {
        $tasks = [];
        foreach ($lines as $line) {
            array_push($tasks, $line->task_id);
        }
        $taskFilter = [];
        foreach (array_unique($tasks) as $taskId) {
            $task = Task::find($taskId);
            if ($task) {
                array_push($taskFilter, $task->id);
            }
        }
        $data = [];
        if (count($taskFilter) > 0) {
            foreach ($taskFilter as $taskId) {
                $plannings = Planning::where('task_id', $taskId)
                    ->where('is_published', true)
                    ->where("employee_id", $employee->id)
                    ->oldest('id')
                    ->get();
                if (count($plannings) > 1) {
                    $array = $this->showMultiPlanning($plannings, $taskId, $employee);
                    if ($array) {
                        array_push($data, $array);
                    }
                }
                if (count($plannings) == 1) {
                    $array = $this->showSinglePlanning($plannings, $taskId, $employee);
                    if ($array) {
                        array_push($data, $array);
                    }
                }
            }
        }
        return $data;
    }


    public function types_codes_timesheets()
    {
        $types = [
            ["name" => "Regular", "id" => "R"],
            ["name" => "Standby", "id" => "NBR"],
            ["name" => "Travel", "id" => "TRA"],
            ["name" => "Holiday", "id" => "HOL"],
            ["name" => "Floating holiday",  "id" => "FHL"],
            ["name" => "PTO",  "id" => "PTF"]
        ];
        return $this->SuccessResponse($types);
    }

    private static function  cmp($a, $b) {
        if ($a == $b) {
            return 0;
        }
        return (key($a) < key($b)) ? -1 : 1;
    }


}


