<?php

namespace App\Http\Controllers;

use App\Services\CityService;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;


class CountryController extends Controller
{

    use ApiResponser;

    /**
     * @var $service
     */
    public $service;

    /**
     * Constructor for CountryController
     *
     * @param CityService $service
     */
    public function __construct(CityService $service)
    {
        $this->service = $service;
    }

    /**
     * Return resource List.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = $this->service->listCountries();
        return $this->SuccessResponse($response);
    }

    /**
     * Store resource item.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->service->createCountry($request->all());
        return $this->SuccessResponse($response, Response::HTTP_CREATED);
    }

    /**
     * Show resource item.
     *
     * @param int $country
     * @return void
     */
    public function show($country)
    {
        $response = $this->service->showCountry($country);
        return $this->SuccessResponse($response);
    }

    /**
     * Update resource item
     *
     * @param Request $request
     * @param int $country
     * @return void
     */
    public function update(Request $request, $country)
    {
        $response = $this->service->updateCountry($country, $request->all());
        return $this->SuccessResponse($response);
    }

    /**
     * SoftDelte resource item
     *
     * @param int $country
     * @return void
     */
    public function destroy($country)
    {
        $response = $this->service->deleteCountry($country);
        return $this->SuccessResponse($response);
    }
}
