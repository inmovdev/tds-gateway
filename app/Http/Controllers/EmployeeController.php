<?php


namespace App\Http\Controllers;


use App\Models\Employee;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EmployeeController extends Controller
{
    use ApiResponser;

    public function showPaycom(Request $request)
    {
        $rules = [
            'employee_paycom_code' => 'required'
        ];
        $this->validate($request, $rules);
        $attributes = $request->all();
        $employeePaycomId = $attributes["employee_paycom_code"];
        $employee = Employee::where('id', $employeePaycomId)->first();
        return $this->SuccessResponse($employee, Response::HTTP_OK);
    }

    public function show($user)
    {
        return Employee::where('work_email', '=',  $user->email)->where('active', true)->first();
    }
}
