<?php

namespace App\Http\Controllers;

use App\Services\PositionService;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;


class PositionController extends Controller
{

    use ApiResponser;

    /**
     * @var $service
     */
    public $service;

    /**
     * Constructor for PositionController
     *
     * @param PositionService $positionService
     */
    public function __construct(PositionService $positionService)
    {
       $this->service = $positionService;
    }

    /**
     * Return Position List.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = $this->service->listPositions();
        return $this->SuccessResponse($response);
    }

    /**
     * Store Position item.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = $this->service->createPosition($request->all());
        return $this->SuccessResponse($response, Response::HTTP_CREATED);
    }

    /**
     * Show Position item.
     *
     * @param int $position
     * @return void
     */
    public function show($position)
    {
        $response = $this->service->showPosition($position);
        return $this->SuccessResponse($response);
    }

    /**
     * Update position item
     *
     * @param Request $request
     * @param int $position
     * @return void
     */
    public function update(Request $request, $position)
    {
        $response = $this->service->updatePosition($position, $request->all());
        return $this->SuccessResponse($response);
    }

    /**
     * SoftDelte position item
     *
     * @param int $position
     * @return void
     */
    public function destroy($position)
    {
        $response = $this->service->deletePosition($position);
        return $this->SuccessResponse($response);
    }
}
