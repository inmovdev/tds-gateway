<?php

namespace App\Http\Controllers;

use App\Models\Job;
use App\Models\JobsWorkdayNote;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;


class JobsWorkdayNoteController extends Controller
{

    use ApiResponser;


    public function store(Request $request)
    {
        $rules = [
            "jobs_workday_id" => "exists:jobs_workday,id",
            "jobs_workday_notes_type_id" => "exists:jobs_workday_notes_types,id",
            "text" => "nullable",
            "file" => "nullable",
            "title" => "nullable",
        ];
        $this->validate($request, $rules);

        $fields = $request->all();

        $item = new JobsWorkdayNote();

        if($request->hasFile('file')) {
            $jobWorkDay = DB::table('jobs_workday')
                ->where('id', $request->jobs_workday_id)
                ->first();

            $job = Job::find($jobWorkDay->job_id);
            $path = $new_name = time() . '.' . $request->file->extension();
            $path = $request->file->storeAs("jobs_workday_notes/$job->title", $new_name, 'public');
            $item->file = $path;
        }

        $item->jobs_workday_id =  $fields["jobs_workday_id"];
        $item->jobs_workday_notes_type_id =   $fields["jobs_workday_notes_type_id"];
        $item->text = $fields["text"];
        $item->title = $fields["title"];
        $item->save();

        return response()->json([
            "data" => $item,
            "code" => 200,
        ]);
    }


    public function update(Request $request, $note)
    {
        $rules = [
            "jobs_workday_id" => "exists:jobs_workday,id",
            "jobs_workday_notes_type_id" => "exists:jobs_workday_notes_types,id",
            "text" => "nullable",
            "file" => "nullable",
            "title" => "nullable",
        ];
        $this->validate($request, $rules);

        $fields = $request->all();

        $item  = JobsWorkdayNote::findOrFail($note);

        if ($request->file->extension()) {
            $jobWorkDay = DB::table('jobs_workday')
                ->where('id', $request->jobs_workday_id)
                ->first();

            $job = Job::find($jobWorkDay->job_id);
            $path = $new_name = time() . '.' . $request->file->extension();
            $path = $request->file->storeAs("jobs_workday_notes/$job->title", $new_name, 'public');
            $item->file =  $path;
        }

        $item->jobs_workday_id =  $fields["jobs_workday_id"];
        $item->jobs_workday_notes_type_id =   $fields["jobs_workday_notes_type_id"];
        $item->text =   $fields["text"];
        $item->title =   $fields["title"];

        $item->save();

        return response()->json([
            "data" => $item,
            "code" => 200,
        ]);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  $note
     * @return \Illuminate\Http\Response
     */
    public function destroy($note)
    {
        //dd($note);
        $item = JobsWorkdayNote::findOrFail($note);
        $item->delete();


        $response["deleted"] = $item;
        if ($item) {
            $response["status"] = 200;
        } else {
            $response["status"] = 401;
        }

        return response()->json($response);
    }
}
