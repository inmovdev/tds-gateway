<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\State;
use App\Models\Task;
use App\Models\Planning;
use App\Models\TimeSheetsLine;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB as FacadesDB;
use DB;
use function Symfony\Component\VarDumper\Dumper\esc;

class TimeSheetsLineController extends Controller
{
    use ApiResponser;

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $timeSheetsLines = TimeSheetsLine::latest('id')->get();
        return $this->SuccessResponse($timeSheetsLines);
    }

    public function indexTask($taskId)
    {
        $return = array();

        $employeeController = new EmployeeController();
        $planningController = new PlanningController();
        //

        $task = Task::findOrFail($taskId);

        $employee = $employeeController->show(auth()->user());

        if ($employee) {
            $plnings = array();
            $planning = Planning::where('task_id', '=', $taskId)
                ->where('is_published', '=', true)
                ->where("employee_id", "=", $employee->id)
                ->get();

            $sum_ts = 0;
            if (isset($planning) && !empty($planning)) {

                foreach ($planning as $plan) {
                    $sum_ts = 0;

                    $start = explode(' ', $plan->start_datetime);

                    $end = explode(' ', $plan->end_datetime);
                    $just_one_day = false;

                    $dif_start = Carbon::parse($plan->start_datetime)->subHours(5);
                    $dif_end = Carbon::parse($plan->end_datetime)->subHours(5);
                    $startDate = $dif_start->format("Y-m-d");
                    $endDate = $dif_end->format("Y-m-d");
                    $days_diference = Carbon::parse($startDate)->diffInDays(Carbon::parse($endDate));
                    if ($start[0] == $end[0]) {
                        $just_one_day = true;
                    }

                    //dd($dif_start, $dif_end);
                    if ($dif_start <= $dif_end) {
                        $hours = $plan->allocated_hours;
                        $days = $days_diference + 1; //$plan->working_days_count;

                        if ($hours > 0 && $days > 0) {

                            $hours = $hours / $days;
                        } else {
                            //no days, no hours

                            $hours = 0;
                        }

                        $return['From ' . $start[0] . " to " . $end[0]]["Year " . $dif_start->format('Y') . " - Week: " . $dif_start->weekOfYear][$dif_start->format('Y-m-d')]["hours_planning"] = $hours;

                        $timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                            ->where('employee_id', $employee->id)
                            ->whereDate('date', $dif_start)
                            ->get();
                        if (count($timeSheetLines) == 0) {
                            $attributes = array();
                            $attributes["description"] = '/';
                            $attributes["date"] = $dif_start->format('Y-m-d');
                            $attributes["email_employee"] = $employee->work_email;
                            $attributes["unit_amount"] = 0;
                            $attributes["task_id"] = $taskId;
                            $attributes["employee_id"] = $employee->id;
                            $attributes["custom_state"] = "draft";
                            $task = Task::findOrFail($taskId);

                            $timeSheetsLine = new TimeSheetsLine();
                            $data = $timeSheetsLine->setDataCreate($attributes, $task);
                            $timeSheetsLine = $timeSheetsLine->create($data);

                            $timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                                ->where('employee_id', $employee->id)
                                ->whereDate('date', $dif_start)
                                ->get();
                        }

                        foreach ($timeSheetLines as $line) {
                            if ($line->custom_state == "waiting") {
                                $line->custom_state = "submitted";
                            }
                            $return['From ' . $start[0] . " to " . $end[0]]["Year " . $dif_start->format('Y') . " - Week: " . $dif_start->weekOfYear][$dif_start->format('Y-m-d')]["timesheet_hours"] = $line;
                            $sum_ts += $line->unit_amount;
                        }
                        $dif_start = $dif_start;
                    }
                }
                $task->effective_hours = $sum_ts;
                $task->planned_hours = (!empty($planning[0])) ? $planning[0]->allocated_hours : 0;
                $return["task"] = $task;
                $return["planning"] = $planning;
                return $this->SuccessResponse($return, Response::HTTP_OK);
            } else {
                return $this->ErrorResponse([], Response::HTTP_OK);
            }
        } else {
            return $this->ErrorResponse([], Response::HTTP_OK);
        }
    }

    public function indexTaskV2(Request $request, $taskId)
    {
        $state = $request->state;
        $task = Task::findOrFail($taskId);
        $employee = Employee::where('work_email', '=', auth()->user()->email)
            ->where('active', true)
            ->first();
        if ($employee) {
            $plannings = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->oldest('id')
                ->get();
            if (count($plannings) > 1) {
                $array = $this->showMultiPlanning($plannings, $taskId, $employee, $state);
                return response()->json($array, Response::HTTP_OK);
            }
            if (count($plannings) == 1) {
                $array = $this->showSinglePlanning($plannings, $taskId, $employee, $state);
                return response()->json($array, Response::HTTP_OK);
            }
            if (count($plannings) == 0) {
                return response()->json([], Response::HTTP_OK);
            }
        } else {
            return response()->json([], Response::HTTP_OK);
        }
    }

    public function indexTaskV3(Request $request, $taskId)
    {
        $state = $request->state;
        $task = Task::findOrFail($taskId);
        $employee = Employee::where('work_email', '=', auth()->user()->email)
            ->where('active', true)
            ->first();
        if ($employee) {
            $plannings = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->oldest('id')
                ->get();
            if (count($plannings) > 1) {
                return $this->showMultiPlanning($plannings, $taskId, $employee, $state);
            }
            if (count($plannings) == 1) {
                return $this->showSinglePlanning($plannings, $taskId, $employee, $state);
            }
            if (count($plannings) == 0) {
                return response()->json([], Response::HTTP_OK);
            }
        } else {
            return response()->json([], Response::HTTP_OK);
        }
    }

    private function showMultiPlanning($plannings, $taskId, $employee, $state = null)
    {
        $array = [];
        $dates = [];
        $task = Task::findOrFail($taskId);
        foreach ($plannings as $planning) {
            $startDatePlanning = Carbon::parse($planning->start_datetime)->subHours(5);
            $endDatePlanning = Carbon::parse($planning->end_datetime)->subHours(5);
            $dates = array_merge($dates, [
                $startDatePlanning->startOfWeek()->format('Y-m-d'),
                $endDatePlanning->endOfWeek()->format('Y-m-d'),
            ]);
        }
        $datesNew = [];
        foreach (array_unique($dates) as $date) {
            array_push($datesNew, $date);
        }
        $maxCount = count($datesNew) / 2;
        for ($i = 0; $i < $maxCount; $i++) {
            $initPosition = $i;
            $endPosition = $i + 1;
            if ($i > 0) {
                if ($i == 1) {
                    $initPosition = $i + 1;
                    $endPosition = $initPosition + 1;
                } else {
                    if ($i % 2 === 0) {
                        $initPosition = $i + 2;
                        $endPosition = $initPosition + 1;
                    } else {
                        $initPosition = $i + 3;
                        $endPosition = $initPosition + 1;
                    }
                }
            }
            $dateStart = $datesNew[$initPosition];
            $dateEnd = $datesNew[$endPosition];
            $year = explode('-', $dateEnd)[0];
            $numberWeek = Carbon::parse($dateStart)->weekOfYear;
            $plannings = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->where(function ($query) use ($dateStart, $dateEnd) {
                    $query->where('start_datetime', '>=', "$dateStart 00:00:00");
                    $query->where('end_datetime', '<=', "$dateEnd 23:59:59");
                })
                ->oldest('id')
                ->get();
            $labelFrom = "From $dateStart to $dateEnd";
            $labelWeek = "Year $year - Week: $numberWeek";
            array_push($array, [
                "$labelFrom" => [
                    "$labelWeek" => [],
                ]
            ]);
            $positionArray = count($array) - 1;
            foreach ($plannings as $planning) {
                $startDatePlanning = Carbon::parse($planning->start_datetime)->subHours(5);
                $timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                    ->where('employee_id', $employee->id)
                    ->whereDate('date', $startDatePlanning->format('Y-m-d'))
                    ->get();
                if (count($timeSheetLines) == 0) {
                    $attributes = [
                        "description" => '/',
                        "date" => $startDatePlanning->format('Y-m-d'),
                        "email_employee" => $employee->work_email,
                        "unit_amount" => 0,
                        "task_id" => $taskId,
                        "employee_id" => $employee->id,
                        "custom_state" => "draft",
                    ];
                    $timeSheetsLines = new TimeSheetsLine();
                    $data = $timeSheetsLines->setDataCreate($attributes, $task);
                    $timeSheetsLines->create($data);
                }
            }
            $initDate = explode(' ', $labelFrom)[1];
            $finalDate = explode(' ', $labelFrom)[3];
            $timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                ->where('employee_id', $employee->id)
                ->whereBetween('date', [
                    $initDate,
                    $finalDate
                ])
                ->oldest('date');
            if ($state) {
                if ($state != "notReported" && $state != 'today') {
                    $timeSheetLines = $this->filterState($timeSheetLines, $state);
                } else {
                    if ($state == 'today') {
                        $timeSheetLines = $timeSheetLines->where('employee_id', $employee->id)
                            ->where('unit_amount', 0)
                            ->where('date', Carbon::now('America/Bogota')->format('Y-m-d'))
                            ->where('custom_state', TimeSheetsLine::DRAFT)
                            ->oldest('date');
                    } else {
                        $timeSheetLines = $timeSheetLines->where('employee_id', $employee->id)
                            ->where('unit_amount', 0)
                            ->where('date', '<', Carbon::now('America/Bogota')->format('Y-m-d'))
                            ->where('custom_state', TimeSheetsLine::DRAFT)
                            ->oldest('date');
                    }
                }
            }
            $timeSheetLines = $timeSheetLines->get();
            $timeSheets = [];
            if (count($timeSheetLines) > 0) {
                foreach ($timeSheetLines as $timeSheetLine) {
                    if ($timeSheetLine->custom_state == "waiting") {
                        $timeSheetLine->custom_state = "submitted";
                    }
                    $allocatedHours = Planning::where('task_id', $timeSheetLine->task_id)
                        ->where('is_published', true)
                        ->where("employee_id", $employee->id)
                        ->whereBetween('start_datetime', [
                            "$timeSheetLine->date 00:00:00",
                            "$timeSheetLine->date 23:59:59"
                        ])
                        ->sum('allocated_hours');
                    $object = [
                        "$timeSheetLine->date" => [
                            "hours_planning" => (float)$allocatedHours,
                            "timesheet_hours" => $timeSheetLine,
                        ]
                    ];
                    array_push($timeSheets, $object);
                }
                if (count($timeSheets) > 0) {
                    $array[$positionArray][$labelFrom][$labelWeek] = [
                        "timeSheets" => $timeSheets
                    ];
                }
            } else {
                if (count($array[$positionArray][$labelFrom][$labelWeek]) == 0) {
                    unset($array[$positionArray][$labelFrom]);
                }
            }
        }
        foreach ($array as $item) {
            array_filter($item);
        }
        if (count($array) > 0) {
            $allocatedHours = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->oldest('id')
                ->sum('allocated_hours');
            $startDates = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->oldest('id')
                ->pluck('start_datetime')
                ->toArray();
            $dates = TimeSheetsLine::where('task_id', $task->id)->where('employee_id', $employee->id)->oldest('date')->pluck('date')->toArray();
            $sumTs = TimeSheetsLine::where('task_id', $task->id)->where('employee_id', $employee->id)->sum('unit_amount');
            $task->effective_hours = $sumTs;
            $task->planned_hours = (int)$allocatedHours;
            $array[0]["task"] = $task;
            $array[0]["planning"] = count($plannings) > 0 ? [$plannings[0]] : null;
            //$array[0]["startDates"] = $startDates;
            //$array[0]["dates"] = $dates;
        }
        $finalArray = [];
        if (count($array) > 0) {
            foreach ($array as $item) {
                if (!empty($item) || count($item) > 0) {
                    array_push($finalArray, $item);
                }
            }
        }
        return $finalArray;
    }

    private function showSinglePlanning($plannings, $taskId, $employee, $state = null)
    {
        $array = [];
        $dates = [];
        $task = Task::findOrFail($taskId);
        $startDatePlanning = Carbon::parse($plannings[0]->start_datetime)->subHours(5);
        $endDatePlanning = Carbon::parse($plannings[0]->end_datetime)->subHours(5);
        $daysDifference = $startDatePlanning->diffInDays($endDatePlanning) == 0 ? 1 : $startDatePlanning->diffInDays($endDatePlanning) + 1;
        foreach ($plannings as $planning) {
            $startDatePlanning = Carbon::parse($planning->start_datetime)->subHours(5);
            $endDatePlanning = Carbon::parse($planning->end_datetime)->subHours(5);
            $start = $startDatePlanning->format('Y-m-d');
            $end = $endDatePlanning->format('Y-m-d');
            for ($i = 0; $i < $daysDifference; $i++) {
                $dates = array_merge($dates, [
                    Carbon::parse($start)->addDays($i)->startOfWeek()->format('Y-m-d'),
                    Carbon::parse($end)->addDays($i)->startOfWeek()->format('Y-m-d'),
                ]);
            }
        }
        $datesNew = [];
        sort($dates);
        foreach (array_unique($dates) as $date) {
            array_push($datesNew, $date);
        }
        if (!count($datesNew) % 2 == 0) {
            $newWeek = Carbon::parse(end($datesNew))->endOfWeek()->format('Y-m-d');
            array_push($datesNew, $newWeek);
        }
        $maxCount = count($datesNew) / 2;
        for ($i = 0; $i < $maxCount; $i++) {
            $initPosition = $i;
            $endPosition = $i + 1;
            if ($i > 0) {
                if ($i == 1) {
                    $initPosition = $i + 1;
                    $endPosition = $initPosition + 1;
                } else {
                    if ($i % 2 === 0) {
                        $initPosition = $i + 2;
                        $endPosition = $initPosition + 1;
                    } else {
                        $initPosition = $i + 3;
                        $endPosition = $initPosition + 1;
                    }
                }
            }
            $dateStart = $datesNew[$initPosition];
            $dateEnd = $datesNew[$endPosition];
            $year = explode('-', $dateEnd)[0];
            $numberWeek = Carbon::parse($dateStart)->weekOfYear;
            $plannings = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->where(function ($query) use ($dateStart, $dateEnd) {
                    $query->where('start_datetime', '>=', "$dateStart 00:00:00");
                    $query->where('end_datetime', '<=', "$dateEnd 23:59:59");
                })
                ->oldest('id')
                ->get();
            $labelFrom = "From $dateStart to $dateEnd";
            $labelWeek = "Year $year - Week: $numberWeek";
            array_push($array, [
                "$labelFrom" => [
                    "$labelWeek" => [],
                ]
            ]);
            $positionArray = count($array) - 1;
            foreach ($plannings as $planning) {
                $startDatePlanning = Carbon::parse($planning->start_datetime)->subHours(5);
                $timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                    ->where('employee_id', $employee->id)
                    ->whereDate('date', $startDatePlanning->format('Y-m-d'))
                    ->get();
                if (count($timeSheetLines) == 0 || count($timeSheetLines) < $daysDifference) {
                    for ($i = 0; $i < $daysDifference; $i++) {
                        $startDateFor = $startDatePlanning->format('Y-m-d');
                        $day = Carbon::parse($startDateFor)->addDays($i)->format('Y-m-d');
                        $timeSheetLine = TimeSheetsLine::where('task_id', $taskId)
                            ->where('employee_id', $employee->id)
                            ->whereDate('date', $day)
                            ->first();
                        if (!$timeSheetLine) {
                            $attributes = [
                                "description" => '/',
                                "date" => $day,
                                "email_employee" => $employee->work_email,
                                "unit_amount" => 0,
                                "task_id" => $taskId,
                                "employee_id" => $employee->id,
                                "custom_state" => "draft",
                            ];
                            $timeSheetsLines = new TimeSheetsLine();
                            $data = $timeSheetsLines->setDataCreate($attributes, $task);
                            $timeSheetsLines->create($data);
                        }
                    }
                }
            }
            $initDate = explode(' ', $labelFrom)[1];
            $finalDate = explode(' ', $labelFrom)[3];
            $timeSheetLines = TimeSheetsLine::where('task_id', $taskId)
                ->where('employee_id', $employee->id)
                ->whereBetween('date', [
                    $initDate,
                    $finalDate
                ])
                ->oldest('date');
            if ($state) {
                if ($state != "notReported" && $state != 'today') {
                    $timeSheetLines = $this->filterState($timeSheetLines, $state);
                } else {
                    if ($state == 'today') {
                        $timeSheetLines = $timeSheetLines->where('employee_id', $employee->id)
                            ->where('unit_amount', 0)
                            ->where('date', Carbon::now('America/Bogota')->format('Y-m-d'))
                            ->where('custom_state', TimeSheetsLine::DRAFT)
                            ->oldest('date');
                    } else {
                        $timeSheetLines = $timeSheetLines->where('employee_id', $employee->id)
                            ->where('unit_amount', 0)
                            ->where('date', '<', Carbon::now('America/Bogota')->format('Y-m-d'))
                            ->where('custom_state', TimeSheetsLine::DRAFT)
                            ->oldest('date');
                    }
                }
            }
            $timeSheetLines = $timeSheetLines->get();
            $timeSheets = [];
            if (count($timeSheetLines) > 0) {
                foreach ($timeSheetLines as $timeSheetLine) {
                    if ($timeSheetLine->custom_state == "waiting") {
                        $timeSheetLine->custom_state = "submitted";
                    }
                    $planning = Planning::where('task_id', $timeSheetLine->task_id)
                        ->where('is_published', true)
                        ->where("employee_id", $employee->id)
                        ->first();
                    $object = [
                        "$timeSheetLine->date" => [
                            "hours_planning" => (float)$planning->allocated_hours / $daysDifference,
                            "timesheet_hours" => $timeSheetLine,
                        ]
                    ];
                    array_push($timeSheets, $object);
                }
                if (count($timeSheets) > 0) {
                    $array[$positionArray][$labelFrom][$labelWeek] = [
                        "timeSheets" => $timeSheets,
                    ];
                }
            } else {
                if (count($array[$positionArray][$labelFrom][$labelWeek]) == 0) {
                    unset($array[$positionArray][$labelFrom]);
                }
            }
        }
        foreach ($array as $item) {
            array_filter($item);
        }
        if (count($array) > 0) {
            $allocatedHours = Planning::where('task_id', '=', $taskId)
                ->where('is_published', true)
                ->where("employee_id", $employee->id)
                ->oldest('id')
                ->sum('allocated_hours');
            $sumTs = TimeSheetsLine::where('task_id', $task->id)->where('employee_id', $employee->id)->sum('unit_amount');
            $task->effective_hours = $sumTs;
            $task->planned_hours = (int)$allocatedHours;
            $array[0]["task"] = $task;
            $array[0]["planning"] = count($plannings) > 0 ? [$plannings[0]] : null;
        }
        $finalArray = [];
        if (count($array) > 0) {
            foreach ($array as $item) {
                if (!empty($item) || count($item) > 0) {
                    array_push($finalArray, $item);
                }
            }
        }
        return $finalArray;
    }

    /**
     * @param $timeSheetLines
     * @param null $state
     * @return mixed
     */
    public function filterState($timeSheetLines, $state = null)
    {
        if ($state == TimeSheetsLine::DRAFT) {
            return $timeSheetLines->where('custom_state', TimeSheetsLine::DRAFT);
        }
        if ($state == TimeSheetsLine::REJECT) {
            return $timeSheetLines->where('custom_state', TimeSheetsLine::REJECT);
        }
        if ($state == TimeSheetsLine::APPROVED) {
            return $timeSheetLines->where('custom_state', TimeSheetsLine::APPROVED);
        }
        if ($state == TimeSheetsLine::WAITING || $state == "submitted") {
            return $timeSheetLines->where('custom_state', TimeSheetsLine::WAITING);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            "email_employee" => 'required|email|exists:hr_employee,work_email',
            'description' => 'required|string',
            'date' => 'required|string',
            'unit_amount' => 'required',
            'task_id' => 'required|exists:project_task,id'
        ];
        $this->validate($request, $rules);
        $attributes = $request->all();
        $task = Task::findOrFail($request->task_id);
        $this->timeSheetsLine = new TimeSheetsLine();
        $data = $this->timeSheetsLine->setDataCreate($attributes, $task);
        $timeSheetsLine = $this->timeSheetsLine->create($data);
        $task->setDataUpdate($timeSheetsLine);
        $task->save();
        return $this->SuccessResponse($timeSheetsLine, Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function meTimeSheetsLines(Request $request)
    {
        $rules = [
            "email_employee" => 'required|email|exists:hr_employee,work_email',
        ];
        $this->validate($request, $rules);
        $attributes = $request->all();
        $employeeId = Employee::where("work_email", $attributes["email_employee"])->value("id");
        $timeSheetsLines = TimeSheetsLine::where("employee_id", $employeeId)->latest("id")->get();
        return $this->SuccessResponse($timeSheetsLines, Response::HTTP_OK);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'description' => 'required|string',
            'date' => 'required|string',
            'unit_amount' => 'required',
        ];
        $this->validate($request, $rules);
        $timeSheetsLine = TimeSheetsLine::findOrFail($id);
        $timeSheetsLine->setDataUpdate($request->all());
        if ($timeSheetsLine->isClean()) {
            return $this->ErrorResponse('Al menos un valor debe cambiar', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $timeSheetsLine->save();
        $task = Task::find($timeSheetsLine->task_id);
        $task->setDataUpdate($timeSheetsLine);
        $task->save();
        $im = auth()->user();
        DB::select('CALL prd_register_score(?,?,?,?)', array((int)$im->id, 7, 0, '')); //7
        return $this->SuccessResponse($timeSheetsLine);
    }

    public function destroy($id)
    {
        $timeSheetsLine = TimeSheetsLine::findOrFail($id);
        if (!$timeSheetsLine->validated) {
            $timeSheetsLine->delete();
            return $this->SuccessResponse($timeSheetsLine, Response::HTTP_OK);
        } else {
            return $this->ErrorResponse("The line is already validated, it cannot be deleted", Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * @param Collection $lines
     * @param Employee $employee
     * @return array
     */
    private function showAllFilter(Collection $lines, $employee)
    {
        $tasks = [];
        foreach ($lines as $line) {
            array_push($tasks, $line->task_id);
        }
        $taskFilter = [];
        foreach (array_unique($tasks) as $taskId) {
            $task = Task::find($taskId);
            if ($task) {
                array_push($taskFilter, $task->id);
            }
        }
        $data = [];
        if (count($taskFilter) > 0) {
            foreach ($taskFilter as $taskId) {
                $plannings = Planning::where('task_id', $taskId)
                    ->where('is_published', true)
                    ->where("employee_id", $employee->id)
                    ->oldest('id')
                    ->get();
                if (count($plannings) > 1) {
                    $array = $this->showMultiPlanning($plannings, $taskId, $employee);
                    if ($array) {
                        array_push($data, $array);
                    }
                }
                if (count($plannings) == 1) {
                    $array = $this->showSinglePlanning($plannings, $taskId, $employee);
                    if ($array) {
                        array_push($data, $array);
                    }
                }
            }
        }
        return $data;
    }
}
