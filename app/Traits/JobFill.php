<?php

namespace App\Traits;

use Illuminate\Http\Request;
use App\Models\Job;
use App\Models\Task;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use GuzzleHttp\Client as Guzzle;

trait JobFill
{

    public function get_coordinates($adress)
    {
        $direccion = urlencode($adress);
        $url = "https://maps.google.com/maps/api/geocode/json?address=" . $direccion . "&key=" . env('MAPS_GEOCODE_API');
        $client = new Guzzle();
        $response = $client->request('GET', $url);
        return $response = json_decode($response->getBody());
    }


    public function fill_job($user_id)
    {

        $projectController = new \App\Http\Controllers\ProjectController();

        $opportunities = DB::select("SELECT o.*, o.id as opp_id , o.id_odoo as odoo_project_id, c.*, tu.* FROM opportunities as o
        INNER JOIN opportunity_tag_users as tu on o.id = tu.opportunity_id
        INNER JOIN customers as c on c.salesforce_account_id = o.account_id
        where tu.user_id = '$user_id'");

//        foreach ($opportunities as $opportunity) {
//
//            $opportunitiyTransformer = new \App\Transformers\OpportunityTransformer();
//            $datas = $opportunitiyTransformer->getTasks($opportunity->odoo_project_id);
//            if (!empty($datas)) {
//                foreach ($datas as $data) {
//
//                    $job_query = Job::where('opportunity_id', '=', $opportunity->opp_id)->where('project_odoo_id', '=', $opportunity->odoo_project_id)->where('task_odoo_id', '=', $data["id"]);
//                    $cant = $job_query->count();
//
//
//                    $customer = DB::table('customers')->where('salesforce_account_id', '=', $opportunity->account_id);
//                    $customer_cant = $customer->count();
//                    if ($customer_cant > 0) {
//                        $customer = $customer->get();
//                        $customer_id = $customer[0]->id;
//                        $adress = $customer[0]->billing_address . ',' . $customer[0]->city;
//                        //$response_maps = $this->get_coordinates($adress);
//                        //if($response_maps){
//                        $latitude = $customer[0]->latitude;//$response_maps->results[0]->geometry->location->lat;
//                        $longitude = $customer[0]->latitude;//$response_maps->results[0]->geometry->location->lng;
//                        //}
//                    } else {
//                        $customer_id = '';
//                        $address = 'Without information';
//                        $latitude = '';
//                        $longitude = '';
//                    }
//
//                    $now = Carbon::now()->format('Y-m-d');
//
//                    $project = $projectController->show($opportunity->odoo_project_id);
//
//                    $array = array(
//                        "contact_id" => $customer_id,
//                        "title" => $opportunity->opportunity_name,
//                        "purpose" => 'Without information',
//                        "date_create" => $now,
//                        "parameter" => 'Without information',
//                        "users_id" => $user_id,
//                        "address" => $adress,
//                        "contract" => 'TDS-' . $data["id"],
//                        #"latitude" => $latitude,
//                        #"longitude" => $longitude,
//                        "places_id" => 0,
//                        "extra_info" => 'Without information',
//                        "checkin" => false,
//                        "visit_date" => $data["planning"]["start_datetime"],
//                        "status" => 'AC',
//                        "project_odoo_id" => $opportunity->odoo_project_id,
//                        "task_odoo_id" => $data["id"],
//                        "status_opportunities" => $opportunity->status,
//                        "opportunity_id" => $opportunity->opp_id
//                    );
//                    if ($cant < 1) {
//                        $workday = array("");
//                        $job = Job::create($array);
//                        DB::table('jobs_workday')->insert(["job_id" => $job->id, "user_id" => $user_id, "status" => 'S', "total_seconds_duration" => '0', "date" => $data["planning"]["start_datetime"]]);
//                    } else {
//                        $array = array(
//                            "contact_id" => $customer_id,
//                            "title" => $opportunity->opportunity_name,
//                            "purpose" => 'Without information',
//                            #"date_create" => $now,
//                            "parameter" => 'Without information',
//                            "users_id" => $user_id,
//                            "address" => $adress,
//                            "contract" => 'TDS-' . $data["id"],
//                            #"latitude" => $latitude,
//                            #"longitude" => $longitude,
//                            "places_id" => 0,
//                            "extra_info" => 'Without information',
//                            #"checkin" => false,
//                            "visit_date" => $data["planning"]["start_datetime"],
//                            #"status" => 'AC',
//                            "project_odoo_id" => $opportunity->odoo_project_id,
//                            "task_odoo_id" => $data["id"],
//                            "status_opportunities" => $opportunity->status,
//                            "opportunity_id" => $opportunity->opp_id);
//
//                        $job = $job_query->get();
//                        Job::where('id', '=', $job[0]->id)
//                            ->update($array);
//
//                    }
//
//                }
//            }
//
//        }

    }


}
