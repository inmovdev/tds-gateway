<?php

namespace App\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

trait ConsumesExternalServices
{

    /**
     * Send a request to a external service
     * @param string $method
     * @param string $requestUrl
     * @param array $formParams optional
     * @param array $headers
     * @return string Response server
     */
    public function performRequest($method,  $requestUrl, $formParams=[], $headers=[])
    {
        $client = new Client([
            'base_uri' => $this->baseUri,
        ]);

        if(isset($this->secret)){
            $headers['Authorization'] = $this->secret;
        }

        //dd($headers);
        $response = $client->request($method, $requestUrl, ['form_params' => $formParams, 'headers' => $headers]);
        return $response->getBody()->getContents();
    }
}
