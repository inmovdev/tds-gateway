<?php
namespace App\Traits;

use Berkayk\OneSignal\OneSignalClient as OneSignalClient;

trait OnseSignal {
   
     /**
     * Send a OneSignal push notification
     *
     * @param array $uuids Array of Onesignal uuids array('aaa111', 'bbb222', 'ccc333', [..])
     * @param string $section Section to be redirected in APP
     * @param string $item_id Item id to be redirected in APP
     * @param string $title Title of the push notification
     * @param string $content Content of the push notification
     * @return bool
     */
     public function send_push($uuids, $section='', $item_id='', $title, $content){
        try{
            $parameters = array(
                "include_player_ids" => $uuids,
                "app_id" => env('ONE_SIGNAL_APP_ID'),
                "contents" => array("en"=> $content, "es"=> $content),
                "headings" => array("en" => $title, "es"=> $title),
                "subtitle" => array("en" => "", "es"=> ""),
                "android_led_color" => "FF9900FF",
                "test_type" => "1",
                "data" => array("section"=>$section, "item_id"=> $item_id)
                );

                $message =  new OneSignalClient(env("ONE_SIGNAL_APP_ID"), env("ONE_SIGNAL_REST_API_KEY"), env("ONE_SIGNAL_AUTH_KEY"));
                $message->sendNotificationCustom($parameters);
                return true;
        }catch(Exception $ex){
                //return response()->json($ex);
                return false;
        }
     }

}
