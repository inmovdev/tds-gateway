<?php
namespace App\Traits;

use SendGrid\Mail\Mail as SendGridMail;
use Illuminate\Http\Request;
use SendGrid as SendGridClient;

trait Sendgrid {

     /**
     * Send a Sendgrid Email
     *
     * @param array $data_replace Array of parameters to be sent to the template
     * @param string $from_text From Text
     * @param string $subject Email Subject
     * @param array $recipients  Array of recipients array("email" => "example@example.com", "full_name" => "Lorem Ipsum"), [..]
     * @return bool
     */
    public function send_email($data_replace=[], $from_text, $subject, $recipients, $template_id ){

        $email = new SendGridMail();
        $email->setFrom(env("MAIL_FROM_ADDRESS"), $from_text);
        $email->setSubject($subject);
        foreach($recipients as $item){
            $email->addTo($item['email'], $item['full_name']);
        }
        $email->setTemplateId($template_id);
        $email->addDynamicTemplateDatas($data_replace);

        try {
        $sendgrid = new SendGridClient(env("MAIL_API"));
        $response = $sendgrid->send($email);
        //print $response->statusCode() . "\n";
        //print_r($response->headers());
        //print $response->body() . "\n";
            return true;
        } catch (Exception $e) {
        //    return  'Caught exception: '. $e->getMessage() ."\n";
            return false;
        }

    }



}
