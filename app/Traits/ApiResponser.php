<?php

namespace App\Traits;

use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

trait ApiResponser
{
    /**
     * Return standard success response
     * @param string|array $data information to send
     * @param int $code Http Response Code
     * @return \Illuminate\Http\Response
     */
    public function SuccessResponse($data, $code = Response::HTTP_OK)
    {

        return response($data, $code)->header('Content-Type', 'application/json');
    }

    /**
     * Return standard valid response
     * @param string|array $data information to send
     * @param int $code Http Response Code
     * @return \Illuminate\Http\Response
     */
    public function validResponse($data, $code = Response::HTTP_OK)
    {

        return response()->json(['data' => $data], $code);
    }


    /**
     * Return standard error response
     * @param string|array $message Text to error
     * @param int $code Http Response Code
     * @return \Illuminate\Http\JsonResponse
     */
    public function ErrorResponse($message, $code)
    {

        return response()->json(['error' => $message, 'code' => $code], $code);
    }


    /**
     * Return standard error response
     * @param string|array $message Text to error
     * @param int $code Http Response Code
     * @return \Illuminate\Http\Response
     */
    public function ErrorMessage($message, $code)
    {

        return response($message, $code)->header('Content-Type', 'application/json');
    }

    protected function transformData($data, $transformer)
    {
        $transformation = fractal($data, new $transformer);
        return $transformation->toArray();
    }

    protected function showAll(Collection $collection, $code = 200)
    {
        if ($collection->isEmpty()) {
            return $this->successResponse(['data' => $collection, 'code' => $code], 200);
        }

        $transformer = $collection->first()->transformer;
        $collection = $this->transformData($collection, $transformer);
        return $this->successResponse($collection, $code);
    }

    protected function showOne(Model $instance, $code = 200)
    {
        $transformer = $instance->transformer;
        $instance = $this->transformData($instance, $transformer);
        return $this->successResponse($instance, $code);
    }
}
