<?php
namespace App\Traits;

use Illuminate\Http\Request;


trait Helper {

    public function implode_collection($collection, $field){
        $respose = '';
        foreach($collection as $item){
            $respose.= trim($item->{$field}).',';
        }
        return rtrim($respose,',');
    }

    public function check_free_email($email){
        $email = strtolower($email);
        $free_mails = array("gmail", "hotmail", "gmx", "aol", "outlook", "protonmail", "yahoo", "mail", "inbox", "zoho", "shortmail", "hushmail", "ingenieros");

        $alias = explode("@", $email);
        $alias = explode(".", $alias[1]);
        $alias = $alias[0];

        if(in_array($alias, $free_mails)){
          return true;
        }else{
            return false;
        }
    }


    public function time_ago($timestamp)  {
        $time_ago = strtotime($timestamp);
        $current_time = time();
        $time_difference = $current_time - $time_ago;
        $seconds = $time_difference;
        $minutes      = round($seconds / 60 );           // value 60 is seconds
        $hours           = round($seconds / 3600);           //value 3600 is 60 minutes * 60 sec
        $days          = round($seconds / 86400);          //86400 = 24 * 60 * 60;
        $weeks          = round($seconds / 604800);          // 7*24*60*60;
        $months          = round($seconds / 2629440);     //((365+365+365+365+366)/5/12)*24*60*60
        $years          = round($seconds / 31553280);     //(365+365+365+365+366)/5 * 24 * 60 * 60
        
        if($seconds <= 60){
            return  trans('general.justnow');
        }else if($minutes <=60){
            if($minutes==1){
                return trans('general.aminute');
            }else{
                return    trans('general.timeago')." $minutes ".trans('general.timeminutes');
            }
        }else if($hours <=24){
            if($hours==1){
                return trans('general.ahour');
            }else{
                return trans('general.timeago')." $hours ".trans('general.timehours');
            }
        } else if($days <= 7){
            if($days==1){
                return trans('general.yesterday');
            }else{
                return trans('general.timeago')." $days ".trans('general.timedays');
            }
        }else if($weeks <= 4.3) //4.3 == 52/12
            {
            if($weeks==1){
                return trans('general.week');
            }else{
                return trans('general.timeago')." $weeks ".trans('general.timeweek');
            }
        }else if($months <=12){
            if($months==1){
                return trans('general.month');
            }else{
                return  trans('general.timeago')." $months ".trans('general.timemonth');
            }
        }else{
            if($years==1){
                return trans('general.year');
            }else{
                return trans('general.timeago')." $years ".trans('general.timeyear');
            }
        }
    }


}
