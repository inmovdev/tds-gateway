<?php
namespace App\Traits;

use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client as TwilioClient;

trait Twilio {
   
     /**
     * Send a OneSignal push notification
     *
     * @param array $text Content of the SMS
     * @param array $recipients  Array of recipients array("phone" => "+573202452200", "full_name" => "Lorem Ipsum"), [..]
     * @return bool
     */
     public function send_sms($text, $recipients){
        return   $this->_notifyThroughSms($text, $recipients);
     }

     private function _notifyThroughSms($e, $numbers)
    {
        // $items = $this->_notificationRecipients();

        foreach ($numbers  as $recipient) {
            return $this->_sendSms(
                $recipient["full_name"],
                $recipient["phone"],
                $e
            );
        }
    }

    private function _notificationRecipients()
    {
        $adminsFile = base_path() .
            DIRECTORY_SEPARATOR .
            'config' . DIRECTORY_SEPARATOR .
            'administrators.json';
        try {
            $adminsFileContents = \File::get($adminsFile);

            return json_decode($adminsFileContents);
        } catch (FileNotFoundException $e) {
            Log::error(
                'Could not find ' .
                    $adminsFile .
                    ' to notify admins through SMS'
            );
            return [];
        }
    }


    private function _sendSms($name, $to, $message)
    {
        $accountSid = env('TWILIO_ACCOUNT_SID');
        $authToken = env('TWILIO_AUTH_TOKEN');
        $twilioNumber = env('TWILIO_NUMBER');

        $client = new TwilioClient($accountSid, $authToken);

        try {
            $message = $client->messages->create(
                $to,
                [
                    "body" => $message,
                    "from" => $twilioNumber
                    //   On US phone numbers, you could send an image as well!
                    //  'mediaUrl' => $imageUrl
                ]
            );

            sleep(4);
            // Delaaay;
            $messageInfo = $client->messages($message->sid)
                ->fetch();
            $statusTW = $messageInfo->status;
            $twilioResponse = $statusTW;

            if (($twilioResponse != "undelivered") || ($twilioResponse != "failed")) {
                return true;
            } else {
                return false;
                /*
                 switch ($twilioResponse) {
                    case "undelivered":
                        return response()->json(
                            [
                                'errors' => [
                                    'status' => 500,
                                    'messages' => [trans('auth.smsphoneoff')]
                                ]
                            ],
                            500
                        );
                        break;
                    case "unknown":
                    case "failed":
                        return response()->json(
                            [
                                'errors' => [
                                    'status' => 500,
                                    'messages' => [trans('auth.smsphonenoexist')]
                                ]
                            ],
                            500
                        );
                        break;
                 }
                */

            }
            //   Log::info('Mensaje enviado a ' . $twilioNumber);
        } catch (TwilioException $e) {
            /* Log::error(
                'Could not send SMS notification.' .
                ' Twilio replied with: ' . $e
            );*/

            return false;
        }
    }





}
