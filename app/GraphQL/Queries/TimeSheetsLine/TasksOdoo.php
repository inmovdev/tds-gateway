<?php

namespace App\GraphQL\Queries\TimeSheetsLine;

use App\Services\TimeSheetsLineService;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class TasksOdoo
{

    /**
     *
     * @var $service
     */
    protected $service;

    /**
     * Constructor TimeSheetsLinesMe
     *
     * @param TimeSheetsLineService $service
     * @return void
     */
    public function __construct(TimeSheetsLineService $service)
    {
        $this->service = $service;
    }

    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        // TODO implement the resolver
        $response = json_decode($this->service->listTasks($args["project_id"]));
        return $response->data;
    }
}
