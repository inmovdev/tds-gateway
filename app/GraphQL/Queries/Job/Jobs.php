<?php

namespace App\GraphQL\Queries\Job;

use App\Http\Controllers\JobController;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;

class Jobs
{

    /**
     * @var $service
     */
    protected $controller;

    /**
     * Jobs constructor.
     * @param JobController $controller
     */
    public function __construct(JobController $controller)
    {
        $this->controller = $controller;
    }

    /**
     * Return a value for the field.
     *
     * @param null $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param mixed[] $args The arguments that were passed into the field.
     * @param \Nuwave\Lighthouse\Support\Contracts\GraphQLContext $context Arbitrary data that is shared between all fields of a single query.
     * @param \GraphQL\Type\Definition\ResolveInfo $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        // TODO implement the resolver
        if (isset($args["id"]) && !empty($args["id"])) {
            $response = $this->controller->show($args["id"]);
            if($response){
                $rs = json_decode($response->content());
            }
            return ($response && !isset($rs->error)) ?  $rs->data : [];
        } else {
            $response = $this->controller->index($args);
         // dd($response);
         // exit();
            if($response){
                $rs = json_decode($response->content());
            }
            return ($response && !isset($rs->error)) ? $rs->data : [];
        }
    }
}
