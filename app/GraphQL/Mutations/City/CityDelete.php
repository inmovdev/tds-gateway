<?php

namespace App\GraphQL\Mutations\City;

use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Services\CityService;

class CityDelete
{
    /**
     * @var $service
     */
    protected $service;

    /**
     * Constructor for CityCreate
     *
     * @return void
     */
    public function __construct(CityService $service)
    {
       $this->service = $service;
    }

    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $response = json_decode($this->service->createCity($args));
        return $response->data;
    }
}
