<?php

namespace App\GraphQL\Mutations\User;

use App\Http\Controllers\UserController;
use App\Models\User;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Services\PositionService;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;

class UserUpdate
{
    use ApiResponser;

    /**
     * @var $service
     */
    protected $service;


    /**
     * @var $controller
     */
    protected $controller;

    /**
     * Contructor for UserCreate
     *
     * @param PositionService $service
     * @param UserController $controller
     */
    public function __construct(PositionService $service, UserController $controller)
    {
        $this->service = $service;
        $this->controller = $controller;
    }

    /**
     * Return a value for the field.
     *
     * @param  null  $rootValue Usually contains the result returned from the parent field. In this case, it is always `null`.
     * @param  mixed[]  $args The arguments that were passed into the field.
     * @param  \Nuwave\Lighthouse\Support\Contracts\GraphQLContext  $context Arbitrary data that is shared between all fields of a single query.
     * @param  \GraphQL\Type\Definition\ResolveInfo  $resolveInfo Information about the query itself, such as the execution state, the field name, path to the field from the root, and more.
     * @return mixed
     */
    public function __invoke($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        // TODO implement the resolver
        // $response = json_decode($this->service->showPosition($args["position_id"]));
        $response = $this->controller->update($args)->getData();
        return $response->data;
    }
}
