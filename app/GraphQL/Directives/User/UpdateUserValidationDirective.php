<?php

namespace App\GraphQL\Directives\User;

use Illuminate\Validation\Rule;
use Nuwave\Lighthouse\Schema\Directives\ValidationDirective;

class UpdateUserValidationDirective extends ValidationDirective
{
    /**
     * Name of the directive.
     *
     * @return string
     */
    public function name(): string
    {
        return 'updateUserValidation';
    }

    /**
     * @return mixed[]
     */
    public function rules(): array
    {
        return [
            'document' => [Rule::unique('users', 'document')->ignore($this->args['id'], 'id')],
            'name' => ['max:255'],
            'last_name' => ['max:255'],
            'sex' => [Rule::in('M,F,O')],
            'mobile' => ['max:30'],
            'email' => [Rule::unique('users', 'email')->ignore($this->args['id'], 'id')],
            'update_data' => [Rule::in('0,1')],
            'position_id' => ['required'],
        ];
    }
}
