<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Http\Request;

class SendEmailHelpDesk extends Job
{
    /**
     * Create a new job instance.
     * @param Request $request
     * @param User $user
     * @return void
     */
    public function __construct(Request $request, User $user)
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}
