<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Crypt;

class JobsWorkdayNote  extends Model
{

    use SoftDeletes;



    protected $table = "jobs_workday_notes";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'jobs_workday_id', 'jobs_workday_notes_type_id', 'text', 'file'
    ];

}
