<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScoresGuide extends Model
{
    protected $table = "scores_guide";

    public $timestamps = false;

    const CHANGE_PROFILE_PHOTO = "Change profile photo"; //Only one time
    const AUTO_PHONE_LOG = "Auto phone log";
    const NEW_LOG_ENTRY = "New log entry";
    const VIEW_POST = "View post";
    const DOWNLOAD_A_DOCUMENTN = "Download a document";
    const LIKE_POST = '"Like" in post';
    const SUBMIT_HOURS = "Submit hours";
    const PM_REJECT_TIMESHEET_SENT = "PM rejected Timesheet sent";
    const PM_APPROVES_TIMESHEET_SENT = "PM approves Timesheet sent";
    const PARTICIPATION_IN_PROJECTS = "Participation in projects";
    const CHECK_IN_ON_A_JOB = "Check in on a job";
    const COMPLETE_A_TASK = "Complete a task";
    const FINISH_A_WORK_DAY = "Finish a work day";
    const COMPLETE_JOB = "Complete Job";
    const SHARE_ID_BADGE = "Share ID badge"; //Only one time
    const RECIPIENT_INTERACTION_ON_BADGE_ID = "Recipient Interaction on Badge ID";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'value', 'source'];
}
