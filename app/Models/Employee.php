<?php

namespace App\Models;
use DB;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $connection = 'odoo';
    protected $table = "hr_employee";

}
