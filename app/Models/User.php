<?php

namespace App\Models;

use RuntimeException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{

    use SoftDeletes, Authenticatable, Authorizable, HasApiTokens;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'branch_id', 'country_id', 'profile_id', 'position_id', 'document', 'name', 'last_name', 'gender', 'mobile', 'email', 'password', 'avatar',
        'token', 'uuid', 'status', 'update_data', 'employee_paycom_code', 'state_code', 'city', 'department', 'position_title', 'marital_status',
        'work_phone', 'employee_status', 'dol_status', 'street', 'position_family', 'age'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Setting create data User
     *
     * @param array $attributes
     * @return $array
     */
    public function setDataCreate($attributes)
    {
        $array["branch_id"] = array_key_exists("branch_id", $attributes) ? $attributes["branch_id"] : 1;
        $array["country_id"] = array_key_exists("country_id", $attributes) ? $attributes["country_id"] : 1;
        $array["profile_id"] = array_key_exists("profile_id", $attributes) ? $attributes["profile_id"] : 1;
        $array["position_id"] = array_key_exists("position_id", $attributes) ? $attributes["position_id"] : 1;
        $array["document"] = array_key_exists("document", $attributes) ? $attributes["document"] : null;
        $array["name"] = $this->encrypData(trim($attributes["name"]));
        $array["last_name"] = $this->encrypData(trim($attributes["last_name"]));
        $array["gender"] = array_key_exists("gender", $attributes) ? $attributes["gender"] : "M";
        $array["mobile"] = $attributes["mobile"];
        $array["email"] = strtolower(trim($attributes["email"]));
        $array["password"] = bcrypt($attributes["password"]);
        $array["uuid"] = $this->encrypData($attributes["uuid"]);
        $array["status"] = $attributes["status"];

        if (array_key_exists("employee_paycom_code", $attributes)) {
            $array["employee_paycom_code"] = $attributes["employee_paycom_code"];
        }
        if (array_key_exists("state_code", $attributes)) {
            $array["state_code"] = $attributes["state_code"];
        }
        if (array_key_exists("city", $attributes)) {
            $array["city"] = $attributes["city"];
        }
        if (array_key_exists("department", $attributes)) {
            $array["department"] = $attributes["department"];
        }
        if (array_key_exists("position_title", $attributes)) {
            $array["position_title"] = $attributes["position_title"];
        }
        if (array_key_exists("marital_status", $attributes)) {
            $array["marital_status"] = $attributes["marital_status"];
        }
        if (array_key_exists("work_phone", $attributes)) {
            $array["work_phone"] = $attributes["work_phone"];
        }
        if (array_key_exists("employee_status", $attributes)) {
            $array["employee_status"] = $attributes["employee_status"];
        }
        if (array_key_exists("dol_status", $attributes)) {
            $array["dol_status"] = $attributes["dol_status"];
        }
        if (array_key_exists("street", $attributes)) {
            $array["street"] = $attributes["street"];
        }
        if (array_key_exists("position_family", $attributes)) {
            $array["position_family"] = $attributes["position_family"];
        }
        if (array_key_exists("age", $attributes)) {
            $array["age"] = $attributes["age"];
        }

        return $array;
    }

    /**
     * Setting update data User
     *
     * @param array $attributes
     * @return $this
     */
    public function setDataUpdate($attributes)
    {
        $this->branch_id = array_key_exists("branch_id", $attributes) ? $attributes["branch_id"] : 1;
        $this->country_id = array_key_exists("country_id", $attributes) ? $attributes["country_id"] : 1;
        $this->profile_id = array_key_exists("profile_id", $attributes) ? $attributes["profile_id"] : 1;
        $this->position_id = array_key_exists("position_id", $attributes) ? $attributes["position_id"] : 1;
        $this->document = array_key_exists("document", $attributes) ? $attributes["document"] : null;
        $this->name = $this->encrypData(trim($attributes["name"]));
        $this->last_name = $this->encrypData(trim($attributes["last_name"]));
        $this->gender = array_key_exists("gender", $attributes) ? $attributes["gender"] : $this->gender;
        $this->mobile = $attributes["mobile"];
        $this->email = strtolower($attributes["email"]);
        $this->status = $attributes["status"];
        if (isset($attributes["uuid"]) && !empty($attributes["uuid"])) {
            $this->uuid = $this->encrypData(trim($attributes["uuid"]));
        }
        $this->update_data = true;
        return $this;
    }

    /**
     * Setting update data User
     *
     * @param array $attributes
     * @return $this
     */
    public function setDataUuid($attributes)
    {
        if (isset($attributes["uuid"]) && !empty($attributes["uuid"])) {
            $this->uuid = $this->encrypData(trim($attributes["uuid"]));
        }
        $this->update_data = true;
        return $this;
    }

    /**
     * Getting data User
     *
     * @return $this
     */
    public function getDataDescrypt()
    {
        $this->branch_id = $this->branch_id;
        $this->country_id = $this->country_id;
        $this->profile_id = $this->profile_id;
        $this->position_id = $this->position_id;
        $this->document = $this->document;
        $this->name = $this->decryptData($this->name);
        $this->last_name = $this->decryptData($this->last_name);
        $this->gender = $this->gender;
        $this->mobile = $this->mobile;
        $this->email = $this->email;
        $this->uuid = $this->decryptData($this->uuid);
        $this->status = $this->status;
        $this->update_data = $this->update_data ? "1" : "0";
        return $this;
    }

    /**
     * Encrypt the received value
     *
     * @param string $value
     * @return string
     */
    private function encrypData($value)
    {
        if ($value)
            return Crypt::encryptString($value);
    }

    /**
     * Describe the received value
     *
     * @param string $value
     * @return string
     */
    private function decryptData($value)
    {
        if ($value)
            return Crypt::decryptString($value);
    }

    public function getEmailAttribute($value)
    {
        return strtolower($value);
    }

    /* public function setEmailAttribute($value)
    {
        return strtolower($value);
    }*/



    public function findForPassport($username)
    {
        $username = strtolower(trim($username));

        return $this->whereRaw("LOWER(email) = '" . $username . "'")->first();
    }
}
