<?php

namespace App\Models;

use App\Transformers\TaskTransformer;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $connection = 'odoo';
    protected $table = "project_task";

    public $timestamps = false;

    public $transformer = TaskTransformer::class;

    protected $fillable = [
        'stage_id', 'remaining_hours', 'effective_hours', 'total_hours_spent', 'planned_hours', 'progress'
    ];


    /**
     * @param $timeSheetLine
     * @return $this
     */
    public function setDataUpdate($timeSheetLine)
    {
        $unitAmount = $timeSheetLine->unit_amount;
        $this->remaining_hours = $this->remaining_hours - $unitAmount;
        $this->effective_hours = $this->effective_hours + $unitAmount;
        $this->total_hours_spent = $this->total_hours_spent + $unitAmount;
        if ($this->planned_hours || $this->planned_hours > 0) {
            $onePoint = 100 / $this->planned_hours;
            $this->progress = $this->total_hours_spent * $onePoint;
        }
        return $this;
    }

    /**
     * @param int $stateId
     * @return $this
     */
    public function changeState($stateId)
    {
        $this->stage_id = $stateId;
        return $this;
    }

    public function project()
    {
        $this->belongsTo(Project::class);
    }
}
