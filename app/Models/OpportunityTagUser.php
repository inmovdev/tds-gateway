<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OpportunityTagUser extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'opportunity_id', 'user_id', 'description', 'start_date', 'tag_date', 'tagger_user_id', 'tag_user_id'
    ];


}
