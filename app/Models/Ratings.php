<?php

namespace App\Models;

use App\Transformers\LineTransformer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Ratings extends Model
{

    protected $table = "ratings";

    public $timestamps = false;

    public $transformer = LineTransformer::class;

    protected $fillable = [
        'id', 'item_id', 'user_id', 'item_type', 'type', 'comment', 'score'
    ];

    public function setDataLike($item_type, $itemId)
    {
        $array['item_type'] = $item_type;
        $array['item_id'] = $itemId;
        $array['user_id'] = auth()->user()->id;
        $array['type'] = "L";
        $array['date'] = Carbon::now('America/Bogota');
        return $array;
    }

    public function setDataComment($attributes, $item_type, $itemId)
    {
        $array['item_type'] = $item_type;
        $array["comment"] = $attributes["comment"];
        $array['item_id'] = $itemId;
        $array['user_id'] = auth()->user()->id;
        $array['type'] = "C";
        $array['date'] = Carbon::now('America/Bogota');
        return $array;
    }

    public function setDataDeleteSoft()
    {
        $this->deleted_at = Carbon::now('America/Bogota');
        return $this;
    }

}
