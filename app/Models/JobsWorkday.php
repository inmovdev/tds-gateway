<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobsWorkday extends Model
{
    protected $table = "jobs_workday";

    protected $fillable = [
        "job_id", "user_id", "status", "date", "opportunity_id",
    ];

    public function setDataCreate($job, $opportunity, $visitDate)
    {
        $array['job_id'] = $job->id;
        $array['user_id'] = auth()->user()->id;
        $array['status'] = 'S';
        $array['date'] = $visitDate;
        $array['opportunity_id'] = $opportunity->id;
        return $array;
    }
}
