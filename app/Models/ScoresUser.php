<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ScoresUser extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'scores_guide_id', 'score', 'date_created'];

    /**
     * Seeting create data
     *
     * @param ScoresGuide $scoresGuide
     * @return void
     */
    public function setDataCreate($scoresGuide)
    {
        $array["user_id"] = auth()->user()->id;
        $array["score_guide_id"] = $scoresGuide->id;
        $array["score"] = $scoresGuide->value;
        $array["date_created"] = Carbon::now('America/Bogota');
        return $array;
    }
}
