<?php


namespace App\Models;


use App\Transformers\OpportunityTransformer;
use Illuminate\Database\Eloquent\Model;

class Opportunity extends Model
{
    public $transformer = OpportunityTransformer::class;
}
