<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Planning extends Model
{
    protected $connection = 'odoo';
    protected $table = "planning_slot";
}
