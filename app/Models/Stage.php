<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    protected $connection = 'odoo';
    protected $table = "project_task_type";

    public $timestamps = false;

    const TO_DO = 'TO DO';
    const PROJECT_TO_DO = "PROJECT TO DO";
    const COMPLETED = "COMPLETED";
    const STUCK = "STUCK";
    const NOT_USED = "NOT USED";
    const WORK_IN_PROGRESS = "WORK IN PROGRESS";

    protected $fillable = [
        'name', 'case_default'
    ];
}
