<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RatingsScoresResume extends Model
{
    protected $table = "ratings_scores_resume";

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'average_rating', 'total_score', 'updated_at'];
}
