<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersLevel extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'icon', 'min', 'max', 'level', 'background', 'color'];
}
