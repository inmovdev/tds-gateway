<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Agreement extends Model
{
    protected $connection = 'odoo';
    protected $table = "agreement";
}
