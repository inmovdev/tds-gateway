<?php

namespace App\Models;

use App\Transformers\LineTransformer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserLevel extends Model
{
    protected $table = "users_levels";
    public $timestamps = false;

    protected $fillable = [
        "id", "name", "icon", "min", "max", "level", "background", "color"
    ];
}
