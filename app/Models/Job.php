<?php

namespace App\Models;

use App\Transformers\JobTransformer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Job extends Model
{

    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = [
        "contact_id",
        "title",
        "purpose",
        "date_create",
        "parameter",
        "users_id",
        "address",
        "contract",
        "latitude",
        "longitude",
        "places_id",
        "extra_info",
        "checkin",
        "visit_date",
        "status",
        "project_odoo_id",
        "task_odoo_id",
        "status_opportunities",
        "opportunity_id",
        "planning_odoo_id",
    ];
    use SoftDeletes;

    public $transformer = JobTransformer::class;

    public function jobsTarget()
    {
        $data = "with listar as (select a.*,
(case when a.availability = 'Assigned' then 30 else (
	case when a.availability = 'Targeted' then 20 else 10 end
) end ) as orden from (select a.*,
(case when a.check_status like '%2%' then 'Assigned' else (
	case when a.check_status like '%1%' then 'Targeted' else 'Avalaible' end) end
) as availability,
(length(a.check_status) - length(replace(a.check_status,'1',''))) as targered_count
from (
	select u.id,u.name, u.last_name ,u.gender,u.email,u.work_phone,
u.state_code,u.city,u.street,u.department,u.employee_status,
u.position_family,u.position_title,u.age,u.marital_status,
STRING_AGG(op.opportunity_name,', ') as projects,
STRING_AGG(
	(to_char(op.estimated_start_date,'Mon/dd/yyyy')
			|| ' - ' || to_char(op.estimated_end_date,'Mon/dd/yyyy')),', ') dates,
STRING_AGG((
	case when
		op.id is NULL then '0' else (
									case when op.stage = 'Closed Lost' then '0' else (
										case when op.stage = 'Closed Won' then (case when estimated_end_date < current_date then '0' else '2' end) else '1' end
									) end
								)
			   end),','
) as check_status
from vs_users_decrypt u
left join opportunity_tag_users tag on tag.user_id = u.id
left join opportunities op on op.id = tag.opportunity_id  group by
u.id,u.name, u.last_name,u.gender,u.email,u.work_phone,
u.state_code,u.city,u.street,u.department,u.employee_status,
u.position_family,u.position_title,u.age,u.marital_status) a) a )
 select * from listar order by orden";
        return DB::select(DB::raw($data));
    }

    public function setDataCreate($planning, $visitDate, $opportunity, $customer, $now, $i)
    {
        $startDay = Carbon::parse($planning->start_datetime);
        $visitDate = $i == 0 ? $startDay : $startDay->addDays($i);

        $taskId = $planning->task_id;
        $array["contact_id"] = $customer ? $customer->id : null;
        $array["title"] = $opportunity->opportunity_name;
        $array["purpose"] = 'Without information';
        $array["date_create"] = $now;
        $array["parameter"] = 'Without information';
        $array["users_id"] = auth()->user()->id;
        $array["address"] = $customer ? "$customer->billing_address, $customer->city" : "Without information";
        $array["contract"] = "TDS-$taskId";
        $array["places_id"] = 0;
        $array["extra_info"] = 'Without information';
        $array["checkin"] = false;
        $array["visit_date"] = $visitDate;
        $array["status"] = 'AC';
        $array["project_odoo_id"] = $planning->project_id;
        $array["task_odoo_id"] = $planning->task_id;
        $array["status_opportunities"] = $opportunity->status;
        $array["opportunity_id"] = $opportunity->id;
        $array["planning_odoo_id"] = $planning->id;
        return $array;
    }

    public function setDataUpdate($planning, $i)
    {
        $startDay = Carbon::parse($planning->start_datetime);
        $visitDate = $i == 0 ? $startDay : $startDay->addDays($i);
        $this->project_odoo_id = $planning->project_id;
        $this->task_odoo_id = $planning->task_id;
        $this->visit_date = $visitDate;
        $this->status = 'AC';
        return $this;
    }

    public function setDataUpdateMulti($planning)
    {
        $startDay = Carbon::parse($planning->start_datetime)->subHours(5);
        $this->project_odoo_id = $planning->project_id;
        $this->task_odoo_id = $planning->task_id;
        $this->visit_date = $startDay;
        return $this;
    }

    public function setDataUpdateChecking($attributes)
    {
        $this->status = "CO";
        $this->checkin = true;
        $this->date_checkin = Carbon::now('America/Bogota');
        $this->longitude = $attributes["longitude"];
        $this->latitude = $attributes["latitude"];
        return $this;
    }
}
