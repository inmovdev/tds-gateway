<?php

namespace App\Models;

use App\Transformers\LineTransformer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $table = "scores_guide";

    public $timestamps = false;

    public $transformer = LineTransformer::class;

    protected $fillable = [
        "id", "name", "description", "value", "source"
    ];

}
