<?php

namespace App\Models;

use App\Transformers\LineTransformer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PhoneLog extends Model
{
    protected $table = "phone_logs";
    public $timestamps = false;

    protected $fillable = [
        "name", "date", "job_id", "note", "phone", "user_id"
    ];
}
