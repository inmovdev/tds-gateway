<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $connection = 'odoo';
    protected $table = "account_analytic_account";
}
