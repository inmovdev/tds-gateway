<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StateOdoo extends Model
{
    protected $connection = 'odoo';
    protected $table = "res_country_state";

}
