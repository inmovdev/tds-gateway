<?php

namespace App\Models;

use App\Transformers\LineTransformer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = "news";

    protected $fillable = [
        'id', 'user_id', 'news_category_id', 'news_gallery_id', 'branch_id', 'title', 'text_short', 'text', 'photo', 'publication_date', 'close_date', 'post_to', 'status',  'must_notify', 'target_group_type', 'target_group_value', 'notified'
    ];
}
