<?php

namespace App\Models;

use App\Transformers\LineTransformer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TimeSheetsLine extends Model
{
    protected $connection = 'odoo';
    protected $table = "account_analytic_line";

    CONST DRAFT = "draft";
    CONST WAITING = "waiting";
    CONST APPROVED = "approved";
    CONST REJECT = "rejected";

    public $timestamps = false;

    public $transformer = LineTransformer::class;

    protected $fillable = [
        "name", "date", "amount", "unit_amount", "product_uom_id", "account_id", "partner_id", "user_id", "company_id",
        "currency_id", "group_id", "create_uid", "create_date", "write_uid", "write_date", "task_id", "project_id", "employee_id",
        "department_id", "validated", "product_id", "general_account_id", "move_id", "code", "ref", "so_line",
        "timesheet_invoice_type", "timesheet_invoice_id", "custom_state", "earncode",
    ];

    /**
     * @param $attributes
     * @param $task
     * @return mixed
     */
    public function setDataCreate($attributes, $task)
    {
        $now = Carbon::now();
        $employee = $this->getEmployee($attributes);
        $userId = 2;
        $accountId = 1;
        $array["name"] = $attributes["description"];
        $array["date"] = $attributes["date"];
        $array["amount"] = $this->calculateAmount($attributes);
        $array["unit_amount"] = (float)$attributes["unit_amount"];
        $array["product_uom_id"] = 6;
        $array["account_id"] = $accountId;
        $array["company_id"] = 1;
        $array["currency_id"] = 8;
        $array["user_id"] = $userId;
        $array["create_uid"] = $userId;
        $array["create_date"] = $now;
        $array["write_uid"] = $userId;
        $array["write_date"] = $now;
        $array["task_id"] = $attributes["task_id"];
        $array["earncode"] = (!empty($attributes["earncode"]))?$attributes["earncode"]:"regular";
        $array["project_id"] = $task->project_id;
        $array["employee_id"] = $employee->id;
        $array["department_id"] = Department::where("name", "Professional Services")->value("id");
        $array["validated"] = false;
        $array["custom_state"] = (!empty($attributes["custom_state"]))?$attributes["custom_state"]:"draft";
        //$array["timesheet_invoice_type"] = "non_billable";
        return $array;
    }


    /**
     * @param $attributes
     * @return $this
     */
    public function setDataUpdate($attributes)
    {
        $this->name = $attributes["description"];
        $this->date = $attributes["date"];
        if($attributes["earncode"]){
            $this->earncode = $attributes["earncode"];
        }
        if($attributes["custom_state"]){
            $this->custom_state = $attributes["custom_state"];
        }
        $this->amount = $this->calculateAmount($attributes);
        $this->unit_amount = (float)$attributes["unit_amount"];
        return $this;
    }

    /**
     * @param $attributes
     * @return mixed
     */
    private function getEmployee($attributes)
    {
        return Employee::where("work_email", $attributes["email_employee"])->first();
    }

    /**
     * @param $attributes
     * @return false|float|int
     */
    private function calculateAmount($attributes)
    {
        $unitAmount = (float)$attributes["unit_amount"];
        if ($unitAmount >= 1) {
            return ($unitAmount * 60) * -100;
        } elseif ($unitAmount > 0 && $unitAmount < 1) {
            return round($unitAmount * -100, 2);
        } else {
            return 0;
        }
    }

    public function employee()
    {
        $this->belongsTo(Employee::class, "employee_id");
    }
}
