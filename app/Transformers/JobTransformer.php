<?php

namespace App\Transformers;

use App\Http\Controllers\JobController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TimeSheetsLineController;
use App\Models\Agreement;
use App\Models\Job;
use App\Models\Partner;
use App\Models\Planning;
use App\Models\Project;
use App\Models\ProjectsManager;
use App\Models\StateOdoo;
use App\Models\Task;
use App\Services\TimeSheetsLineService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class JobTransformer extends TransformerAbstract
{
    /**
     * @param $job
     * @return array
     */
    private function getContact($job)
    {
        $table = DB::table('customers');
        $contact = $table->where('id', $job->contact_id)->first();
        $partner = Partner::find($contact->id_odoo);
        return [
            'id' => $contact ? $contact->id : "Without information",
            'name' => $contact ? $contact->name : "Without information",
            'phone' => $contact ? !ctype_space($contact->phone) ? !empty($contact->phone) ?: "Without information" : "Without information" : "Without information",
            'assistant' => null,
            'assistant_phone' => null,
            'other_phone' => null,
            'home_phone' => $partner ? !ctype_space($partner->phone) ?  !empty($partner->phone) ? $partner->phone : null : null : null,
            'mailing_address' => $partner ? $partner->email : null,
            'mobile' => $partner ? $partner->mobile : null,
            'created_at' => $contact ? $contact->created_at : null,
        ];
    }

    /**
     * @param $job
     * @return array
     */
    private function getJobsWorkdays($job)
    {
        $table = DB::table('jobs_workday');
        $workDay = $table->where('job_id', $job->id)->latest('id')->first();
        if ($workDay) {
            $tracker = DB::table('jobs_workday_time_tracker')
                ->where('jobs_workday_id', $workDay->id)
                ->latest('id')
                ->first();
            if (!$tracker) {
                DB::table('jobs_workday_time_tracker')->insert([
                    'jobs_workday_id' => $workDay->id,
                    'start' => null,
                    'end' => null,
                    'duration_seconds' => 0,
                    'created_at' => Carbon::now('America/Bogota'),
                    'upadted_at' => Carbon::now('America/Bogota'),

                    'status' => "A",
                ]);
            }
            $tracker = DB::table('jobs_workday_time_tracker')
                ->where('jobs_workday_id', $workDay->id)
                ->latest('id')
                ->first();
            return [
                'id' => $workDay->id,
                'user_id' => $workDay->user_id,
                'status' => $workDay->status,
                'total_seconds_duration' => $workDay->total_seconds_duration,
                'date' => $workDay->date,
                'created_at' => $workDay->created_at,
                'updated_at' => $workDay->updated_at,
                'jobs_workday_time_trackers' => [$tracker]
            ];
        } else {
            return null;
        }
    }

    /**
     * @param $job
     * @return mixed
     */
    private function getProject($job)
    {
        if ($job->project_odoo_id) {
            $project = Project::find($job->project_odoo_id);
            return $project;
        }
        return null;
    }

    private function getProjectManager($project)
    {
        if ($project) {
            $projectManager = ProjectsManager::find($project->user_id);
            if(!isset($projectManager->partner_id)){
                return null;
            }else{
                return Partner::find($projectManager->partner_id);
            }
        }
        return null;
    }

    /**
     * @param $job
     * @return mixed
     */
    private function getTask($job)
    {
        if ($job->task_odoo_id) {
            $task = Task::findOrFail($job->task_odoo_id);
            return $task;
        }
        return null;
    }

    private function getJobsWorkdayLatest($worDay)
    {
        $table = DB::table('jobs_workday_time_tracker');
        $tracker = $table->where('jobs_workday_id', $worDay->id)
            ->latest('id')
            ->first();
        return $tracker;
    }

    private function getPlanning($job)
    {
        $planning = null;
        if ($job->planning_odoo_id) {
            $planning = Planning::where('id', $job->planning_odoo_id)->first();
        } else {
            $planning = Planning::where('project_id', $job->project_odoo_id)
                ->where('task_id', $job->task_odoo_id)
                ->first();
        }
        return $planning;
    }

    private function getAgreement($project)
    {
        if ($project->agreement_id)
            return Agreement::find($project->agreement_id);
        return null;
    }

    private function getExtraInfo($task, $job)
    {
        if ($task && $task->description != "<p><br></p>") {
            return $task->description;
        }
        if ($job && $job->extra_info) {
            return $job->extra_info;
        }
        return "Without information";
    }

    private function getLastestChecking($job)
    {
        $table = DB::table('jobs_checkings');
        $checking = $table->where('job_id', $job->id)->whereDate('created_at', Carbon::now('America/Bogota'))->first();
        if ($checking) {
            return $checking;
        }
    }

    /**
     * Turn this item object into a generic array.
     *
     * @param Job $job
     * @return array
     */
    public function transform(Job $job)
    {
        $contact = $this->getContact($job);
        $workDays = $this->getJobsWorkdays($job);
        $project = $this->getProject($job);
        $task = $this->getTask($job);
        $planning = $this->getPlanning($job);
        $projectManager = $this->getProjectManager($project);
        $agreement = $this->getAgreement($project);
        $extraInfo = $this->getExtraInfo($task, $job);
        $checking = $this->getLastestChecking($job);
        $address = "Without information job";
        if ($project && isset($project->delivery_street) &&  $project->delivery_street) {
            $state = $project->delivery_state_id ? StateOdoo::where('id', $project->delivery_state_id)->first() : null;
            $codeZip = (int)$project->delivery_zip;
            $address = "$project->delivery_street, $project->delivery_city, $state->name, $codeZip";
        }
        return [
            'id' => $job->id,
            'purchase_order' => $project->cm_po ? $project->cm_po : "Without information",
            'work_order' => $project->cm_wo ? $project->cm_wo : "Without information",
            'contract' => $agreement ? "#$agreement->code" : "Without information",
            'ubication' => $job->latitude ? true : false,
            'latitude' => $job->latitude,
            'longitude' => $job->longitude,
            'address' => $address,
            'extra_info' => $extraInfo ? $extraInfo : "Without information",
            'parameter' => $job->parameter,
            'places_id' => $job->places_id,
            'purpose' => $task ? $task->name : $job->purpose,
            'status' => $job->status,
            'title' => $project ? $project->name : $job->title,
            'users_id' => $job->users_id,
            'visit_date' => Carbon::parse($job->visit_date),
            'end_date' => Carbon::parse($planning->end_datetime)->timezone($task ? $task->tz : 'America/New_York'),
            'end_data' => Carbon::parse($planning->end_datetime)->timezone($task ? $task->tz : 'America/New_York'),
            'project_odoo_id' => $job->project_odoo_id,
            'task_odoo_id' => $job->task_odoo_id,
            'checkin' => (bool)$job->checkin,
            'date_checkin' => $job->date_checkin,
            'date_create' => $job->date_create,
            'created_at' => $job->created_at,
            'deleted_at' => $job->deleted_at,
            'contact' => $contact,
            'jobsworkdays' => $workDays ? [$workDays] : [],
            'projectOdoo' => $project,
            'taskOdoo' => $task,
            'projectManager' => $projectManager,
        ];
    }
}
