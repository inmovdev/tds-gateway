<?php

namespace App\Transformers;

use App\Models\Stage;
use App\Models\State;
use App\Models\Task;
use League\Fractal\TransformerAbstract;

class TaskTransformer extends TransformerAbstract
{

    private function getStatus($stageId)
    {
        $stage = Stage::where('id', $stageId)->first();
        switch ($stage->name) {
            case "PROJECT TO DO":
            case "TO DO":
                return "I";
            case "WORK IN PROGRESS":
                return "A";
            case "COMPLETED":
                return "F";
            default:
                return $stage->name;
        }
    }

    /**
     * Turn this item object into a generic array.
     *
     * @param Task $task
     * @return array
     */
    public function transform(Task $task)
    {
        $status = $this->getStatus($task->stage_id);
        return [
            'id' => $task->id,
            'rating_last_value' => $task->rating_last_value,
            'access_token' => $task->access_token,
            'email_cc' => $task->email_cc,
            'message_main_attachment_id' => $task->message_main_attachment_id,
            'active' => $task->active,
            'name' => $task->name,
            'description' => $task->description,
            'priority' => $task->priority,
            'sequence' => $task->sequence,
            'stage_id' => $task->stage_id,
            'status' => $status,
            'kanban_state' => $task->kanban_state,
            'create_date' => $task->create_date,
            'write_date' => $task->write_date,
            'date_end' => $task->date_end,
            'date_assign' => $task->date_assign,
            'date_deadline' => $task->date_deadline,
            'date_last_stage_update' => $task->date_last_stage_update,
            'project_id' => $task->project_id,
            'planned_hours' => $task->planned_hours,
            'user_id' => $task->user_id,
            'partner_id' => $task->partner_id,
            'company_id' => $task->company_id,
            'color' => $task->color,
            'displayed_image_id' => $task->displayed_image_id,
            'parent_id' => $task->parent_id,
            'email_from' => $task->email_from,
            'working_hours_open' => $task->working_hours_open,
            'working_hours_close' => $task->working_hours_close,
            'working_days_open' => $task->working_days_open,
            'working_days_close' => $task->working_days_close,
            'create_uid' => $task->create_uid,
            'write_uid' => $task->write_uid,
            'planned_date_begin' => $task->planned_date_begin,
            'planned_date_end' => $task->planned_date_end,
            'remaining_hours' => $task->remaining_hours,
            'effective_hours' => $task->effective_hours,
            'total_hours_spent' => $task->total_hours_spent,
            'progress' => $task->progress,
            'subtask_effective_hours' => $task->subtask_effective_hours,
            'sale_line_id' => $task->sale_line_id,
            'sale_order_id' => $task->sale_order_id,
            'billable_type' => $task->billable_type,
            'timesheet_timer_start' => $task->timesheet_timer_start,
            'timesheet_timer_pause' => $task->timesheet_timer_pause,
            'timesheet_timer_first_start' => $task->timesheet_timer_first_start,
            'timesheet_timer_last_stop' => $task->timesheet_timer_last_stop,
        ];
    }
}
