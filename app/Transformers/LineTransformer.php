<?php

namespace App\Transformers;

use App\Http\Controllers\TaskController;
use App\Models\TimeSheetsLine;
use League\Fractal\TransformerAbstract;

class LineTransformer extends TransformerAbstract
{
    /**
     * @param $line
     * @return mixed
     */
    private function getTask($line)
    {
        if ($line->task_id) {
            $controller = new TaskController();
            return $controller->show($line->task_id);
        }
        return null;
    }

    /**
     * Turn this item object into a generic array.
     *
     * @param TimeSheetsLine $line
     * @return array
     */
    public function transform(TimeSheetsLine $line)
    {
        $task = $this->getTask($line);
        return [
            'id' => $line->id,
            'name' => $line->name,
            'date' => $line->date,
            'amount' => $line->amount,
            'unit_amount' => $line->unit_amount,
            'product_uom_id' => $line->product_uom_id,
            'account_id' => $line->account_id,
            'partner_id' => $line->partner_id,
            'user_id' => $line->user_id,
            'company_id' => $line->company_id,
            'currency_id' => $line->currency_id,
            'group_id' => $line->group_id,
            'create_uid' => $line->create_uid,
            'create_date' => $line->create_date,
            'write_uid' => $line->write_uid,
            'write_date' => $line->write_date,
            'task_id' => $line->task_id,
            'project_id' => $line->project_id,
            'employee_id' => $line->employee_id,
            'department_id' => $line->department_id,
            'validated' => (boolean)$line->validated,
            'product_id' => $line->product_id,
            'general_account_id' => $line->general_account_id,
            'move_id' => $line->move_id,
            'code' => $line->code,
            'ref' => $line->ref,
            'so_line' => $line->so_line,
            'timesheet_invoice_type' => $line->timesheet_invoice_type,
            'timesheet_invoice_id' => $line->timesheet_invoice_id,
            'custom_state' => $line->cumstom_state == "waiting" ? "Submitted" : $line->custom_state,
            'custom_rejected_by' => $line->custom_rejected_by,
            'custom_approve_date' => $line->custom_approve_date,
            'custom_rejected_date' => $line->custom_rejected_date,
            'custom_rejected_reason' => $line->custom_rejected_reason,
            'custom_is_rejected' => $line->custom_is_rejected,
            'task' => $task,
        ];
    }
}
