<?php

namespace App\Transformers;

use App\Models\Customer;
use App\Models\Employee;
use App\Models\Job;
use App\Models\Opportunity;
use App\Models\Partner;
use App\Models\Planning;
use App\Models\Project;
use App\Models\ProjectsManager;
use App\Models\Stage;
use App\Models\StateOdoo;
use App\Models\Task;
use Illuminate\Support\Facades\DB;
use League\Fractal\TransformerAbstract;

class OpportunityTransformer extends TransformerAbstract
{

    /**
     * @param $opportunity
     * @return array
     */
    private function getProject($opportunity)
    {
        $project = Project::where('id', $opportunity->id_odoo)->first();
        $tasks = $this->getTasks($project->id);
        $project = array_merge($tasks);
        return $project;
    }

    /**
     * @param $projectId
     * @return array
     */
    public function getTasks($projectId)
    {
        $employee = $this->getEmployee();
        if ($employee) {
            $tasksArray = Planning::where('employee_id', $employee->id)
                ->where('is_published', true)
                ->where('project_id', $projectId)
                ->pluck('task_id')
                ->toArray();
            $array = [];
            if (count($tasksArray) > 0) {
                $tasks = Task::whereIn('id', $tasksArray)->where('active', true)->get();
                if (count($tasks) > 0) {
                    foreach ($tasks as $task) {
                        $planning = Planning::where('employee_id', $employee->id)
                            ->where('is_published', true)
                            ->where('project_id', $projectId)
                            ->where('task_id', $task->id)
                            ->first();
                        $stage = Stage::find($task->stage_id);
                        array_push($array, [
                            'id' => $task->id,
                            'name' => $task->name,
                            'description' => $task->description == "<p><br></p>" ? "Without information" : $task->description,
                            'stage' => [
                                'id' => $stage->id,
                                'name' => $stage->name,
                            ],
                            'planning' => [
                                'id' => $planning->id,
                                'start_datetime' => $planning->start_datetime,
                                'end_datetime' => $planning->end_datetime,
                                'working_days_count' => $planning->working_days_count,
                            ],
                        ]);
                    }
                }
                return $array;
            }
        }
    }

    public function getCustomer($opportunity)
    {
        return Customer::where('account_id', $opportunity->account_id)->first();
    }

    private function getStatus($opportunity)
    {
        if ($opportunity->status == "A" && !$opportunity->id_odoo)
            return "Assigned";
        if ($opportunity->status == "T" && !$opportunity->id_odoo)
            return "Targeted";
        if ($opportunity->status == "A" && $opportunity->id_odoo)
            return "M";
    }

    private function getEmployee()
    {
        return Employee::where('work_email', auth()->user()->email)->where('active', true)->first();
    }

    /**
     * @param $job
     * @return array
     */
    private function getContact($job)
    {
        $table = DB::table('customers');
        $contact = $table->where('id', $job->contact_id)->first();
        $partner = Partner::find($contact->id_odoo);
        return [
            'id' => $contact ? $contact->id : "Without information",
            'name' => $contact ? $contact->name : "Without information",
            'phone' => $contact ? !ctype_space($contact->phone) ? !empty($contact->phone) ?: "Without information" : "Without information" : "Without information",
            'assistant' => "Without information",
            'assistant_phone' => "Without information",
            'home_phone' => $partner ? !ctype_space($partner->phone) ? !empty($partner->phone) ? $partner->phone : "Without information" : "Without information" : "Without information",
            'other_phone' => "Without information",
            'mailing_address' => $partner ? $partner->email : "Without information",
            'mobile' => $partner ? $partner->mobile : "Without information",
            'created_at' => $contact ? $contact->created_at : "Without information",
            'hp' => !ctype_space($partner->phone)
        ];
    }

    /**
     * @param $job
     * @return array
     */
    private function getJobsWorkdays($job)
    {
        $table = DB::table('jobs_workday');
        $workDay = $table->where('job_id', $job->id)->latest('id')->first();
        if ($workDay) {
            $tracker = DB::table('jobs_workday_time_tracker')
                ->where('jobs_workday_id', $workDay->id)
                ->latest('id')
                ->first();
            return [
                'id' => $workDay->id,
                'user_id' => $workDay->user_id,
                'status' => $workDay->status,
                'total_seconds_duration' => $workDay->total_seconds_duration,
                'date' => $workDay->date,
                'created_at' => $workDay->created_at,
                'updated_at' => $workDay->updated_at,
                'jobs_workday_time_trackers' => [$tracker]
            ];
        } else {
            return null;
        }
    }

    private function getProjectManager($project)
    {
        if ($project) {
            $projectManager = ProjectsManager::find($project->user_id);
            if(!isset($projectManager->partner_id)){
                return null;
            }else{
                return Partner::find($projectManager->partner_id);
            }
        }
        return null;
    }

    /**
     * @param $customer
     * @param $array
     * @return array
     */
    private function getJobs($customer, $array)
    {
        $employee = $this->getEmployee();
        $jobs = Job::where('users_id', auth()->user()->id)
            //->where('created_at', '>=', Carbon::now())
            ->get();
        $jobsArray = [];
        foreach ($jobs as $job) {
            $project = Project::find($job->project_odoo_id);
            $task = Task::find($job->task_odoo_id);
            $planning = Planning::where('employee_id', $employee->id)
                ->where('project_id', $job->project_odoo_id)
                ->where('task_id', $job->task_odoo_id)
                ->first();
            $contact = $this->getContact($job);
            $projectManager = $this->getProjectManager($project);
            $workDays = $this->getJobsWorkdays($job);
            $address = "Without information";
            if ($project && isset($project->delivery_street) &&  $project->delivery_street) {
                $state = $project->delivery_state_id ? StateOdoo::where('id', $project->delivery_state_id)->first() : null;
                $codeZip = (int)$project->delivery_zip;
                $address = "$project->delivery_street, $project->delivery_city, $state->name, $codeZip";
            }
            $jobO = [
                'type' => "Job",
                'id' => $job->id,
                'title' => $project ? $project->name : $job->title,
                'purpose' => $task ? $task->name : $job->purpose,
                'date_create' => $job->date_create,
                'parameter' => "Without information",
                'address' => $address,
                'contact' => $project ? $project->contract_id ? $project->contract_id : $job->contract : $job->contract,
                'latitude' => $job->latitude,
                'longitude' => $job->longitude,
                'places_id' => $job->places_id,
                'extra_info' => $task ? $task->description == "<p><br></p>" ? "Without information" : $task->description : $job->extra_info,
                'status' => $job->status,
                'visit_date' => $planning ? $planning->start_datetime : $job->visit_date,
                'project_odoo_id' => $job->project_odoo_id,
                'task_odoo_id' => $job->task_odoo_id,
                'checkin' => (boolean)$job->checkin,
                'date_checkin' => $job->date_checkin,
                'date_create' => $job->date_create,
                'created_at' => $job->created_at->format('Y-m-d H:m:s'),
                'deleted_at' => $job->deleted_at,
                'contact' => $contact,
                'jobsworkdays' => $workDays ? [$workDays] : [],
                'projectOdoo' => $project,
                'taskOdoo' => $task,
                'projectManager' => $projectManager,
            ];
            array_push($jobsArray, $jobO);
        }
        $jobsArray = array_merge($jobsArray, $array);
        return $jobsArray;
    }

    /**
     * Turn this item object into a generic array.
     *
     * @param Opportunity $opportunity
     * @return array
     */
    public function transform(Opportunity $opportunity)
    {
        $project = $opportunity->id_odoo ? $this->getProject($opportunity) : null;
        dd($project);
        $customer = $this->getCustomer($opportunity);
        //$status = $this->getStatus($opportunity);
        $address = "Without information";
        if ($project && isset($project->delivery_street) &&  $project->delivery_street) {
            $state = $project->delivery_state_id ? StateOdoo::where('id', $project->delivery_state_id)->first() : null;
            $codeZip = (int)$project->delivery_zip;
            $address = "$project->delivery_street, $project->delivery_city, $state->name, $codeZip";
        }
        $array = [
            'type' => "Opportunity",
            'id' => $opportunity->id,
            'name' => $opportunity->opportunity_name,
            'start_date' => $opportunity->estimated_start_date,
            'end_date' => $opportunity->estimated_end_date,
            'city' => $customer ? $customer->city : "Without information",
            'state' => $customer ? $customer->state : "Without information",
            'address' => $address,
            'created_at' => $opportunity->created_at,
            'status' => $opportunity->probability == 100 && $opportunity->stage == "Closed Won" ? "A" : "T",
            'project' => $project,
        ];
        $results = $this->getJobs($customer, $array);
        return $results;
    }
}
