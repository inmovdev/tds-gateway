<?php

namespace App\Services;

use App\Traits\ConsumesExternalServices;

class PositionService
{

    use ConsumesExternalServices;

    /**
     * The Base Uri to consume Position
     * @var string $baseUri
     */
    public $baseUri;

    /**
     * The secret to consume Position
     * @var string $baseUri
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.position.base_uri');
        $this->secret = config('services.position.secret');
    }

    /**
     * Return Position List
     * @return string
     */
    public function listPositions()
    {
        return $this->performRequest('GET', '/positions');
    }

    /**
     * Create Position item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function createPosition($request)
    {
        return $this->performRequest('POST', '/positions', $request);
    }

    /**
     * Get Position item
     * @param int $id
     * @return string
     */
    public function showPosition($id)
    {
        return $this->performRequest('GET', "/positions/{$id}");
    }


    /**
     * Update Position item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function updatePosition($id, $request)
    {
        return $this->performRequest('PUT', "/positions/{$id}", $request);
    }

    /**
     * DELETE Position item
     * @param int $id
     * @return string
     */
    public function deletePosition($id)
    {
        return $this->performRequest("DELETE", "/positions/{$id}");
    }
}
