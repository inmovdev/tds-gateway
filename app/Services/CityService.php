<?php

namespace App\Services;

use App\Traits\ConsumesExternalServices;

class CityService
{

    use ConsumesExternalServices;

    /**
     * The Base Uri to consume City
     * @var string $baseUri
     */
    public $baseUri;

    /**
     * The secret to consume City
     * @var string $baseUri
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.city.base_uri');
        $this->secret = config('services.city.secret');
    }

    /**
     * Return country List
     * @return string
     */
    public function listCountries()
    {
        return $this->performRequest('GET', '/countries');
    }

    /**
     * Return state List
     * @return string
     */
    public function listStates()
    {
        return $this->performRequest('GET', '/states');
    }

    /**
     * Return city List
     * @return string
     */
    public function listCities($request)
    {
        return $this->performRequest('GET', '/cities', $request);
    }

    /**
     * Create country item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function createCountry($request)
    {
        return $this->performRequest('POST', '/countries', $request);
    }

    /**
     * Create state item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function createState($request)
    {
        return $this->performRequest('POST', '/states', $request);
    }

    /**
     * Create city item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function createCity($request)
    {
        return $this->performRequest('POST', '/cities', $request);
    }

    /**
     * Get country item
     * @param int $id
     * @return string
     */
    public function showCountry($id)
    {
        return $this->performRequest('GET', "/countries/{$id}");
    }

    /**
     * Get state item
     * @param int $id
     * @return string
     */
    public function showState($id)
    {
        return $this->performRequest('GET', "/states/{$id}");
    }

    /**
     * Get city item
     * @param int $id
     * @return string
     */
    public function showCity($id)
    {
        return $this->performRequest('GET', "/cities/{$id}");
    }


    /**
     * Update country item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function updateCountry($id, $request)
    {
        return $this->performRequest('PUT', "/countries/{$id}", $request);
    }

    /**
     * Update states item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function updateState($id, $request)
    {
        return $this->performRequest('PUT', "/states/{$id}", $request);
    }

    /**
     * Update states item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function updateCity($id, $request)
    {
        return $this->performRequest('PUT', "/cities/{$id}", $request);
    }

    /**
     * Delete country item
     * @param int $id
     * @return string
     */
    public function deleteCountry($id)
    {
        return $this->performRequest("DELETE", "/countries/{$id}");
    }

    /**
     * Delete state item
     * @param int $id
     * @return string
     */
    public function deleteState($id)
    {
        return $this->performRequest("DELETE", "/states/{$id}");
    }

    /**
     * Delete city item
     * @param int $id
     * @return string
     */
    public function deleteCity($id)
    {
        return $this->performRequest("DELETE", "/cities/{$id}");
    }

    /**
     * Get states item
     *
     * @param int $id
     * @return void
     */
    public function listStatesCountry($id)
    {
        return $this->performRequest("GET", "/states/country/{$id}");
    }

    /**
     * Get states item
     *
     * @param int $id
     * @return void
     */
    public function listCitiesState($id)
    {
        return $this->performRequest("GET", "/cities/state/{$id}");
    }
}
