<?php

namespace App\Services;

use App\Traits\ConsumesExternalServices;

class AppinfoService
{
    use ConsumesExternalServices;

    /**
     * The Base Uri to consume App info
     * @var string $baseUri
     */
    public $baseUri;

    /**
     * The secret to consume App info
     * @var string $baseUri
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.appinfo.base_uri');
        $this->secret = config('services.appinfo.secret');
    }

    /**
     * Return App info List
     * @return string
     */
    public function listAppinfos()
    {
        return $this->performRequest('GET', '/appinfo');
    }

    /**
     * Create App info item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function createAppinfos($request)
    {
        return $this->performRequest('POST', '/appinfo', $request);
    }

    /**
     * Get App info item
     * @param int $id
     * @return string
     */
    public function showAppinfos($id)
    {
        return $this->performRequest('GET', "/appinfo/{$id}");
    }


    /**
     * Update App info item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function updateAppinfos($id, $request)
    {
        return $this->performRequest('PUT', "/appinfo/{$id}", $request);
    }

    /**
     * DELETE App info item
     * @param int $id
     * @return string
     */
    public function deleteAppinfos($id)
    {
        return $this->performRequest("DELETE", "/appinfo/{$id}");
    }


}
