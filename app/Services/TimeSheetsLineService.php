<?php

namespace App\Services;

use App\Traits\ConsumesExternalServices;

class TimeSheetsLineService
{

    use ConsumesExternalServices;

    /**
     * The base uri to consume TimeSheetsLine
     * @var string $baseUri
     */
    public $baseUri;

    /**
     * The secret to consume TimeSheetsLine
     * @var string $baseUri
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.timeSheetsLine.base_uri');
        $this->secret = config('services.timeSheetsLine.secret');
    }

    public function listProjects()
    {
        return $this->performRequest('GET', '/projects');
    }

    public function showProject($projectId)
    {
        return $this->performRequest('GET', "/projects/{$projectId}");
    }

    public function showTask($taskId)
    {
        return $this->performRequest('GET', "/tasks/{$taskId}");
    }

    public function showEmployee($user)
    {
        return $this->performRequest('GET', "/employeePaycom?employee_paycom_code=$user->employee_paycom_code");
    }

    public function showLatestPlanning($employeeId)
    {
        return $this->performRequest('GET', "/plannings/{$employeeId}");
    }

    public function listTasks($projectId)
    {
        return $this->performRequest('GET', "/taskProject/$projectId");
    }

    /**
     * Return TimeSheetsLine List
     * @return string
     */
    public function listTimeSheetsLines($request)
    {
        return $this->performRequest('POST', '/timeSheetsLinesMe', $request);
    }

    /**
     * Create TimeSheetsLine item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function createTimeSheetsLine($request)
    {
        return $this->performRequest('POST', '/timeSheetsLines', $request);
    }

    /**
     * Update TimeSheetsLine item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function updateTimeSheetsLine($id, $request)
    {
        return $this->performRequest('PUT', "/timeSheetsLines/{$id}", $request);
    }

    /**
     * Delete TimeSheetsLine item
     * @param int $id
     * @return string
     */
    public function deleteTimeSheetsLine($id)
    {
        return $this->performRequest("DELETE", "/timeSheetsLines/{$id}");
    }
}
