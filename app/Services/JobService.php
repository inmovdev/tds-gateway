<?php

namespace App\Services;

use App\Traits\ConsumesExternalServices;

class JobService
{
    use ConsumesExternalServices;

    /**
     * The Base Uri to consume JobService
     * @var string $baseUri
     */
    public $baseUri;

    /**
     * The secret to consume JobService
     * @var string $baseUri
     */
    public $secret;

    /**
     * JobService Construct
     */
    public function __construct()
    {
        $this->baseUri = config('services.job.base_uri');
        $this->secret = config('services.job.secret');
    }

    /**
     * Undocumented function
     *
     * @param int $jobWorkdayId
     * @return void
     */
    public function startJobWorkday($jobWorkdayId)
    {
        return $this->performRequest('POST', "/startWorkDay/$jobWorkdayId");
    }

    /**
     * Undocumented function
     *
     * @param int $jobsWorkdayTimeTrackerId
     * @return void
     */
    public function endJobWorkday($jobsWorkdayTimeTrackerId)
    {
        return $this->performRequest('POST', "/endWorkDay/$jobsWorkdayTimeTrackerId");
    }
}
