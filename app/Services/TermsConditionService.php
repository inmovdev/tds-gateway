<?php

namespace App\Services;

use App\Traits\ConsumesExternalServices;

class TermsConditionService
{

    use ConsumesExternalServices;

    /**
     * The base uri to consume TermsCondition
     * @var string $baseUri
     */
    public $baseUri;

    /**
     * The secret to consume TermsCondition
     * @var string $baseUri
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.tyc.base_uri');
        $this->secret = config('services.tyc.secret');
    }

    /**
     * Return TermsCondition List
     * @return string
     */
    public function listTermsConditions()
    {
        return $this->performRequest('GET', '/termsConditions');
    }

    /**
     * Create TermsCondition item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function createTermsCondition($request)
    {
        return $this->performRequest('POST', '/termsConditions', $request);
    }

    /**
     * Get TermsCondition item
     * @param int $id
     * @return string
     */
    public function showTermsCondition($id)
    {
        return $this->performRequest('GET', "/termsConditions/{$id}");
    }


    /**
     * Update TermsCondition item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function updateTermsCondition($id, $request)
    {
        return $this->performRequest('PUT', "/termsConditions/{$id}", $request);
    }

    /**
     * Delete TermsCondition item
     * @param int $id
     * @return string
     */
    public function deleteTermsCondition($id)
    {
        return $this->performRequest("DELETE", "/termsConditions/{$id}");
    }

    /**
     * Return UserTermsCondition List
     * @return string
     */
    public function listUserTermsConditions()
    {
        return $this->performRequest("GET", "/userTermsConditions");
    }

    /**
     * Create UserTermsCondition item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function createUserTermsCondition($request)
    {
        return $this->performRequest('POST', '/userTermsConditions', $request);
    }

    /**
     * Get UserTermsCondition item
     * @param int $id
     * @return string
     */
    public function showUserTermsCondition($id)
    {
        return $this->performRequest('GET', "/userTermsConditions/{$id}");
    }


    /**
     * Update TermsCondition item
     * @param \Illuminate\Http\Response $request
     * @return string
     */
    public function updateUserTermsCondition($id, $request)
    {
        return $this->performRequest('PUT', "/userTermsConditions/{$id}", $request);
    }

    /**
     * Delete TermsCondition item
     * @param int $id
     * @return string
     */
    public function deleteUserTermsCondition($id)
    {
        return $this->performRequest("DELETE", "/userTermsConditions/{$id}");
    }

    /**
     * Show latest resource where user
     *
     * @param int $userId
     * @return string
     */
    public function showUserTermsConditionUser($userId)
    {
        return $this->performRequest("GET", "userTermsConditions/user/{$userId}");
    }
}
