<?php

namespace App\Services;

use App\Traits\ConsumesExternalServices;

class RatingService
{
    use ConsumesExternalServices;

    /**
     * The Base Uri to consume Rating
     * @var mixed
     */
    public $baseUri;

    /**
     * The secret to consume Rating
     * @var mixed
     */
    public $secret;

    /**
     * RatingService constructor.
     */
    public function __construct()
    {
        $this->baseUri = config('services.rating.base_uri');
        $this->secret = config('services.rating.secret');
    }

    /**
     * Return Rating List
     * @return string
     */
    public function listRatings()
    {
        return $this->performRequest('GET', '/ratings');
    }

    /**
     * Create Rating item
     * @param array $request
     * @return string
     */
    public function createRating($request)
    {
        return $this->performRequest('POST', '/ratings', $request);
    }

    /**
     * Get Rating item
     * @param int $id
     * @return string
     */
    public function showRating($id)
    {
        return $this->performRequest('GET', "/ratings/{$id}");
    }


    /**
     * Update Rating item
     * @param int $id
     * @param array $request
     * @return string
     */
    public function updateRating($id, $request)
    {
        return $this->performRequest('PUT', "/ratings/{$id}", $request);
    }

    /**
     * Delete Rating item
     * @param int $id
     * @return string
     */
    public function deleteRating($id)
    {
        return $this->performRequest("DELETE", "/ratings/{$id}");
    }
}
