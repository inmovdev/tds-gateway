<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsCheckingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs_checkings', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->boolean('checking')->default(false);
            $table->decimal('latitude', 12,7);
            $table->decimal('longitude', 12,7);
            $table->unsignedBigInteger('job_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
