<?php

use Illuminate\Support\Facades\Hash;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


$factory->define(\App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'document' => $faker->numberBetween(1000000, 2000000),
        'name' => $faker->name,
        'last_name' => $faker->lastName,
        'sex' =>  $faker->randomElement(['M' ,'F', 'O']),
        'mobile' => '3202452005',//$faker->phoneNumber,
        'email' => 'carlosbeltran@ingenieros.com',//$faker->email,
        'uuid' => $faker->numberBetween(1000000, 2000000),
        'token' => $faker->sentence(6, true),
        'status' => 'A',
        'password' => Hash::make(12345678)
    ];
});
