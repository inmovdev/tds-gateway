<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;

class DatabaseSeeder extends Seeder
{

    /**
     * Encrypt the received value
     *
     * @param string $value
     * @return void
     */
    private function encrypData($value)
    {
        return Crypt::encryptString($value);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call('UsersTableSeeder');
        //factory(User::class, 3)->create();

        $table = \Illuminate\Support\Facades\DB::table('oauth_clients');
        $table->truncate();
        $table->insert([
            "user_id" => null,
            "name" => "Lumen Personal Access Client",
            "secret" => "qUDEglVFfN3reikhXu6mP1hj8dDGX2YGeWHHFivc",
            "redirect" => "http://localhost",
            "personal_access_client" => true,
            "password_client" => false,
            "revoked" => false,
            "created_at" => \Carbon\Carbon::now(),
            "updated_at" => \Carbon\Carbon::now(),
        ]);

        $table->insert([
            "user_id" => null,
            "name" => "Lumen Password Grant Client",
            "secret" => "W58GlWy10O324266agYyc0U5SKCvtpmQLaO5IWv6",
            "redirect" => "http://localhost",
            "personal_access_client" => false,
            "password_client" => true,
            "revoked" => false,
            "created_at" => \Carbon\Carbon::now(),
            "updated_at" => \Carbon\Carbon::now(),
        ]);

//        $user = new User();
//        $user->name = $this->encrypData("User");
//        $user->last_name = $this->encrypData("Example");
//        $user->gender = "M";
//        $user->branch_id = 1;
//        $user->country_id = 1;
//        $user->profile_id = 1;
//        $user->position_id = 1;
//        $user->mobile = "3202452005";
//        $user->email = "user@user.com";
//        $user->password = bcrypt("123456");
//        $user->uuid = $this->encrypData("1234567890");
//        $user->status = "A";
//        $user->save();
    }
}
