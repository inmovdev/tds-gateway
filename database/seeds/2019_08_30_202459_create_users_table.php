<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('branch_id')->unsigned()->default('1');
            $table->bigInteger('country_id')->unsigned()->default('1');
            $table->bigInteger('profile_id')->unsigned()->default('1');;
            $table->bigInteger('position_id')->unsigned()->default('1');;
            $table->bigInteger('document')->nullable()->unsigned();
            $table->string('name');
            $table->string('last_name');
            $table->enum('gender', ['M', 'F', 'O']);
            $table->string('mobile')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('avatar')->nullable();
            $table->longText('token')->nullable();
            $table->longText('uuid')->nullable();
            $table->enum('status', ['A', 'S'])->default('A');
            $table->enum('update_data', ['1', '0'])->default('0');
            $table->string('position_title');
            $table->string('work_phone');
            $table->string('employee_status');
            $table->string('dol_status');
            $table->string('state_code');
            $table->string('city');
            $table->string('department');
            $table->string('marital_status');
            $table->string('employee_paycom_code');
            $table->string('street');
            $table->string('position_family',90);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
