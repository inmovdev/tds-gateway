<?php
return [
    'appinfo'=>[
        'base_uri' => env('APP_INFO_BASE_URL'),
        'secret'=> env('APP_INFO_SECRET'),
    ],
    'position' => [
        'base_uri' => env('POSITION_BASE_URL'),
        'secret' => env('POSITION_SECRET'),
    ],
    'city' => [
        'base_uri' => env('CITIES_BASE_URL'),
        'secret' => env('CITIES_SECRET')
    ],
    'tyc' => [
        'base_uri' => env('TYC_BASE_URL'),
        'secret' => env('TYC_SECRET')
    ],
    'rating' => [
        'base_uri' => env('RATING_BASE_URL'),
        'secret' => env('RATING_SECRET')
    ],
    'timeSheetsLine' => [
        'base_uri' => env('TIME_SHEETS_LINE_BASE_URL'),
        'secret' => env('TIME_SHEETS_LINE_SECRET'),
    ],
    'job' => [
        'base_uri' => env('JOB_BASE_URL'),
        'secret' => env('JOB_SECRET'),
    ]
];
