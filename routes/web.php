<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['middleware' => 'client.credentials'], function () use ($router) {
    /**
     * App Info
     */
    $router->get('/appinfo', 'AppInfoController@index');
    $router->post('/appinfo', 'AppInfoController@store');
    $router->get('/appinfo/{appinfo}', 'AppInfoController@show');
    $router->put('/appinfo/{appinfo}', 'AppInfoController@update');
    $router->patch('/appinfo/{appinfo}', 'AppInfoController@update');
    $router->delete('/appinfo/{appinfo}', 'AppInfoController@destroy');
    /**
     * User
     */
    $router->get('/users', 'UserController@index');
    $router->post('/users', 'UserController@store');
    $router->get('/users/{user}', 'UserController@show');
    $router->delete('/users/{user}', 'UserController@destroy');
    /**
     * Position
     */
    $router->get('/positions', 'PositionController@index');
    $router->post('/positions', 'PositionController@store');
    $router->get('/positions/{position}', 'PositionController@show');
    $router->put('/positions/{position}', 'PositionController@update');
    $router->patch('/positions/{position}', 'PositionController@update');
    $router->delete('/positions/{position}', 'PositionController@destroy');
    /**
     * Country
     */
    $router->get('/countries', 'CountryController@index');
    $router->post('/countries', 'CountryController@store');
    $router->get('/countries/{country}', 'CountryController@show');
    $router->put('/countries/{country}', 'CountryController@update');
    $router->patch('/countries/{country}', 'CountryController@update');
    $router->delete('/countries/{country}', 'CountryController@destroy');
    /**
     * State
     */
    $router->get('/states', 'StateController@index');
    $router->post('/states', 'StateController@store');
    $router->get('/states/{state}', 'StateController@show');
    $router->put('/states/{state}', 'StateController@update');
    $router->patch('/states/{state}', 'StateController@update');
    $router->delete('/states/{state}', 'StateController@destroy');
    $router->get('/states/country/{country}', 'StateController@indexCountry');
    /**
     * City
     */
    $router->get('/cities', 'CityController@index');
    $router->post('/cities', 'CityController@store');
    $router->get('/cities/{city}', 'CityController@show');
    $router->put('/cities/{city}', 'CityController@update');
    $router->patch('/cities/{city}', 'CityController@update');
    $router->delete('/cities/{city}', 'CityController@destroy');
    $router->get('/cities/state/{state}', 'CityController@indexState');
    /**
     * TermsCondition
     */
    $router->get("/termsConditions", "TermsConditionsController@index");
    $router->post("termsConditions", "TermsConditionsController@store");
    $router->get("/termsConditions/{termsCondition}", "TermsConditionsController@show");
    $router->put("/termsConditions/{termsCondition}", "TermsConditionsController@update");
    $router->patch("/termsConditions/{termsCondition}", "TermsConditionsController@update");
    $router->delete("/termsConditions/{termsCondition}", "TermsConditionsController@destroy");
});

$router->post('v1/oauth/login', 'CustomAuthController@auth');
$router->post('v1/oauth/logout', 'CustomAuthController@logout');
$router->post('v1/oauth/forgot/password', 'UserController@recovery_password'); // send recovery request
$router->get('v1/oauth/cancel/recovery/{code}', 'UserController@recovery_password_cancel'); // cancel request
$router->post('v1/oauth/reset/password', 'UserController@validate_token');//validate


/**
 * Protected by user token
 *
 */
$router->get('/v1/verifyHasura', 'UserController@verifyAuthHasura');

$router->group(['middleware' => 'auth:api'], function () use ($router) {
    $router->get('/oauth/me', 'UserController@me');
    $router->post('v1/user/{id}/profile_image', 'UserController@profile_image');
    $router->post('v1/users/mass', 'UserController@mass');
    $router->patch('v1/user/uuid/{id}', 'UserController@uuid_update');

    $router->post('v1/note', 'JobsWorkdayNoteController@store');
    $router->put('v1/note/{note}', 'JobsWorkdayNoteController@update');
    $router->patch('v1/note/{note}', 'JobsWorkdayNoteController@update');
    $router->delete('v1/note/{note}', 'JobsWorkdayNoteController@destroy');

    $router->get('v1/myJobs', 'JobController@index');
    $router->get('v1/jobs/nearly', 'JobController@get_nearly_jobs');
    $router->get('projects', 'ProjectController@index');
    $router->get('tasks/{projectId}', 'TaskController@index');
    $router->get('lines/{taskId}', 'TimeSheetsLineController@indexTask');
    $router->get('v2/lines/{taskId}', 'TimeSheetsLineController@indexTaskV4');
    $router->get('v3/lines/{taskId}', 'TimeSheetsLineController@indexTaskV3');
    $router->post('v1/jobs/nearly', 'JobController@get_nearly_jobs');
    $router->get('v1/jobsTarget', 'JobController@jobsTarget');
    $router->get('opportunities', 'OpportunityController@index');
    $router->post('v1/checkingJob/{jobId}', 'JobController@jobChecking');


    $router->get('v1/directory', 'UserController@index');
    $router->get('v1/news/{news}', 'NewsController@show');
    $router->get('v1/news/like/{news}', 'NewsController@like');

    $router->get('v1/phone/logs', 'PhoneLogController@index');
    $router->post('v1/phone/logs', 'PhoneLogController@store');

    $router->post('v1/helpdesk/send', 'HelpDeskController@send_email');

    /**
     * User Score
     */
    $router->get('v1/my_score', 'UserLevelController@index');

    /**
     * User Score
     */
    $router->get('v1/ranking', 'UserLevelController@ranking');

    $router->get('timeSheetsFilter', 'TimeSheetsLineController@filterState');
    $router->get('v2/timeSheetsFilter', 'JobControllerV2@jobsTaskLines');
    $router->post('timesheet', 'TimeSheetsLineController@store');
    $router->put('timesheet/{id}', 'TimeSheetsLineController@update');

});
$router->get('v2/timeSheetsHourTypes', 'TimeSheetsLineController@types_codes_timesheets');

$router->patch('/users/{user}', 'UserController@update_user');
$router->put('/users/{user}', 'UserController@update_user');

$router->get("resetPassInmov", "UserController@resetPassword");

$router->get("job_fill/{iduser}", "JobController@job_fill_test");

$router->get('v1/get/user/info/{user_id}', 'UserController@get_user_info');
$router->get('createUsers', 'UserController@createUsers');
